import { Component } from '@angular/core';
import { AuthService } from '../core/services/business/auth.service';

@Component({
  template: `
<img style="margin: auto; display: block; width: 75px;" src="./assets/images/icon-loading-colored.svg">
<label style="text-align: center; font-family: 'Open Sans', sans-serif; font-size: 20px; display: block;"> Please wait... </label>` })
export class LoginComponent {

  constructor(private auth: AuthService) {
    auth.login();
  }
}
