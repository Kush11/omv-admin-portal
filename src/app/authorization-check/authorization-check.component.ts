import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  template: `
<img style="margin: auto; display: block; width: 75px;" src="./assets/images/icon-loading-colored.svg">
<label style="text-align: center; font-family: 'Open Sans', sans-serif; font-size: 20px; display: block;"> Please wait... </label>` })
export class AuthorizationCheckComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    const key = 'return_url';
    const route = sessionStorage.getItem(key);
    this.router.navigate([route]);
  }
}
