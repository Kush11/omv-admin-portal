import { LiveStreamCamera } from 'src/app/core/models/entity/camera';
import { Permission } from './../../core/models/entity/permissions';
import { ConfigurationSetting } from './../../core/models/entity/configuration';
import { Company } from 'src/app/core/models/entity/company';
import { State, Selector, Action, StateContext } from '@ngxs/store';
import {
  GetCompanies, CreateCompany, UpdateCompany, DeleteCompany, GetCompany, GetConfigurationSettings, DeleteConfigurationSetting, CreateConfigurationSetting, UpdateConfigurationSetting,
  SetCurrentCompanyId, CreatePermissions, GetPermissions, UpdatePermissions, GetSecurityProviders,
  CreateSecurityProvider, UpdateSecurityProvider, MigrateDatabase, ClearConfigurationSettings, GetConfigurationKeys, GetLiveStreamCameras,
  GetLookups,
  CreateLiveStreamCamera, UpdateLiveStreamCamera, ClearSecurityProviders, GetLiveStreamAssets, GetLiveStreamGroups, DeleteLiveStreamCamera, UpdateCompanyLogo, ClearCurrentCompany, ClearLiveStreamCameras, GetErrorMessages, GetAzureWriteSASToken, GetAzureReadSASToken, CreateLookup, GetLookupTypes, DeleteLookup
} from './companies.action';
import { CustomersService } from 'src/app/core/services/business/customers/customers.service';
import { tap } from 'rxjs/internal/operators/tap';
import { ShowSpinner, HideSpinner, ShowErrorMessage, ShowSuccessMessage } from 'src/app/state/app.actions';
import { Document } from '../../core/models/entity/document';
import { SecurityProvider } from 'src/app/core/models/entity/security-provider';
import { LookupService } from 'src/app/core/services/business/lookup/lookup.service';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { AuthType } from 'src/app/core/enum/auth-type';
import { ErrorMessage } from 'src/app/core/models/entity/error-message';

export class CompaniesStateModel {
  companies: Company[];
  allCustomers: Company[];
  currentCompanyId: number;
  currentCompany: Company;
  totalCompanies: number;
  azureWriteSASToken: string;
  azureReadSASToken: string;

  configurationSettings: ConfigurationSetting[];
  configurationKeys: Lookup[];
  permissions: Permission;
  errors: ErrorMessage[];

  webSecurityProvider: SecurityProvider;
  desktopSecurityProvider: SecurityProvider;

  uploadedCompanyLogoDocument: Document;
  cameras: LiveStreamCamera[];
  livestreamAssets: Lookup[];
  livestreamGroups: Lookup[];

  lookups: Lookup[];
  lookupTypes: string[];
}

@State<CompaniesStateModel>({
  name: 'companies',
  defaults: {
    companies: [],
    allCustomers: [],
    currentCompany: null,
    currentCompanyId: null,
    totalCompanies: 0,
    azureReadSASToken: null,
    azureWriteSASToken: null,

    configurationSettings: [],
    configurationKeys: [],
    permissions: null,
    errors: [],

    webSecurityProvider: null,
    desktopSecurityProvider: null,

    uploadedCompanyLogoDocument: null,

    cameras: null,
    livestreamAssets: [],
    livestreamGroups: [],

    lookups: [],
    lookupTypes: []

  }
})
export class CompaniesState {

  //#region S E L E C T O R S

  //#region Companies

  @Selector()
  static getCompanies(state: CompaniesStateModel) {
    return state.companies;
  }

  @Selector()
  static getTotalCompanies(state: CompaniesStateModel) {
    return state.totalCompanies;
  }

  @Selector()
  static getCurrentCompany(state: CompaniesStateModel) {
    return state.currentCompany;
  }

  @Selector()
  static getCurrentCompanyId(state: CompaniesStateModel) {
    return state.currentCompanyId;
  }

  @Selector()
  static getAzureWriteSASToken(state: CompaniesStateModel) {
    return state.azureWriteSASToken;
  }

  @Selector()
  static getAzureReadSASToken(state: CompaniesStateModel) {
    return state.azureReadSASToken;
  }

  //#endregion

  //#region Configuration Settings

  @Selector()
  static getConfigurationSettings(state: CompaniesStateModel) {
    return state.configurationSettings;
  }

  @Selector()
  static getConfigurationKeys(state: CompaniesStateModel) {
    return state.configurationKeys;
  }

  //#endregion

  //#region Permissions

  @Selector()
  static getPermissions(state: CompaniesStateModel) {
    return state.permissions;
  }

  //#endregion

  //#region Errors

  @Selector()
  static getErrorMessages(state: CompaniesStateModel) {
    return state.errors;
  }

  //#endregion

  //#region LiveStream Cameras

  @Selector()
  static getCameras(state: CompaniesStateModel) {
    return state.cameras;
  }

  @Selector()
  static getLiveStreamAssets(state: CompaniesStateModel) {
    return state.livestreamAssets;
  }

  @Selector()
  static getLiveStreamGroups(state: CompaniesStateModel) {
    return state.livestreamGroups;
  }

  //#endregion

  //#region Security Providers

  @Selector()
  static getWebSecurityProvider(state: CompaniesStateModel) {
    return state.webSecurityProvider;
  }

  @Selector()
  static getDesktopSecurityProvider(state: CompaniesStateModel) {
    return state.desktopSecurityProvider;
  }

  //#endregion

  //#region Lookups

  @Selector()
  static getLookups(state: CompaniesStateModel) {
    return state.lookups;
  }

  @Selector()
  static getLookupTypes(state: CompaniesStateModel) {
    return state.lookupTypes;
  }

  //#endregion  

  constructor(private customersService: CustomersService, private lookupService: LookupService) { }

  //#region A C T I O N S

  //#region Companies

  @Action(GetCompanies)
  getCompanies(ctx: StateContext<CompaniesStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getCustomers()
      .pipe(
        tap(companies => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            companies,
            totalCompanies: companies.length
          });
          ctx.dispatch(new HideSpinner());
        },
          err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(GetCompany)
  getCompany(ctx: StateContext<CompaniesStateModel>, { id }: GetCompany) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getCustomer(id)
      .pipe(
        tap(company => {
          const state = ctx.getState();
          company.imageUrl += state.azureReadSASToken;
          ctx.setState({
            ...state,
            currentCompany: company
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(ClearCurrentCompany)
  clearCurrentCompany(ctx: StateContext<CompaniesStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentCompany: null
    });
  }

  @Action(CreateCompany)
  createCompany(ctx: StateContext<CompaniesStateModel>, { company }: CreateCompany) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.createCustomer(company).pipe(
      tap(() => {
        ctx.dispatch(new ShowSuccessMessage('Company was successfully created.'));
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new GetCompanies());
      }, (err) => ctx.dispatch(new ShowErrorMessage(err.error)))
    );
  }

  @Action(UpdateCompany)
  updateCompany(ctx: StateContext<CompaniesStateModel>, { id, company, customMessage }: UpdateCompany) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.updateCustomer(id, company)
      .pipe(
        tap(() => {
          if (!customMessage) {
            ctx.dispatch(new ShowSuccessMessage(`${company.name} was successfully updated.`));
          } else {
            ctx.dispatch(new ShowSuccessMessage(customMessage));
          }
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(UpdateCompanyLogo)
  updateCompanyLogo(ctx: StateContext<CompaniesStateModel>, { logoUrl }: UpdateCompanyLogo) {
    ctx.dispatch(new ShowSpinner());
    const state = ctx.getState();
    let currentCompany: Company = JSON.parse(JSON.stringify(state.currentCompany));
    currentCompany.imageUrl = logoUrl;

    return this.customersService.updateCustomer(currentCompany.id, currentCompany)
      .pipe(
        tap(() => {
          ctx.dispatch(new HideSpinner());
          ctx.dispatch(new ShowSuccessMessage('Logo was successfully uploaded'));
          ctx.dispatch(new GetCompany(currentCompany.id));
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(GetAzureWriteSASToken)
  getAzureWriteSASToken(ctx: StateContext<CompaniesStateModel>, { companyId }: GetAzureWriteSASToken) {
    return this.customersService.getAzureWriteSASToken(companyId)
      .pipe(
        tap(response => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            azureWriteSASToken: response
          });
        }, error => ctx.dispatch(new ShowErrorMessage(error.error)))
      );
  }

  @Action(GetAzureReadSASToken)
  getAzureReadSASToken(ctx: StateContext<CompaniesStateModel>, { companyId }: GetAzureReadSASToken) {
    return this.customersService.getAzureReadSASToken(companyId)
      .pipe(
        tap(response => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            azureReadSASToken: response
          });
        }, error => ctx.dispatch(new ShowErrorMessage(error.error)))
      );
  }


  @Action(DeleteCompany)
  deleteCompany(ctx: StateContext<CompaniesStateModel>, { company }: DeleteCompany) {
    ctx.dispatch(new ShowSpinner());
    const { id, name } = company;
    return this.customersService.deleteCustomer(id)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`${name} was successfully deactivated.`));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(SetCurrentCompanyId)
  setCurrentCompanyId(ctx: StateContext<CompaniesStateModel>, { companyId }: SetCurrentCompanyId) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      currentCompanyId: companyId
    });
  }

  //#endregion

  //#region Configuration Settings

  @Action(GetConfigurationSettings)
  getConfigurationSettings(ctx: StateContext<CompaniesStateModel>, { companyId }: GetConfigurationSettings) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getConfigurationSettings(companyId)
      .pipe(
        tap(configurationSettings => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            configurationSettings: configurationSettings
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(CreateConfigurationSetting)
  createConfigurationSetting(ctx: StateContext<CompaniesStateModel>, { companyId, configuration }: CreateConfigurationSetting) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.createConfigurationSetting(companyId, configuration)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Configuration Setting was successfully created.'));
          ctx.dispatch(new GetConfigurationSettings(companyId));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(UpdateConfigurationSetting)
  updateConfigurationSetting(ctx: StateContext<CompaniesStateModel>, { companyId, configurationSetting: configuration }: UpdateConfigurationSetting) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.updateConfigurationSetting(companyId, configuration)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`${configuration.key} was successfully updated.`));
          ctx.dispatch(new GetConfigurationSettings(companyId));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(DeleteConfigurationSetting)
  deleteConfigurationSetting(ctx: StateContext<CompaniesStateModel>, { companyId, key }: DeleteConfigurationSetting) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.deleteConfigurationSetting(companyId, key)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`${key} was successfully deleted.`));
          ctx.dispatch(new GetConfigurationSettings(companyId));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(ClearConfigurationSettings)
  clearCustomerConfigurations(ctx: StateContext<CompaniesStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      configurationSettings: []
    });
  }

  @Action(GetConfigurationKeys)
  getConfigurationKeys(ctx: StateContext<CompaniesStateModel>) {
    return this.lookupService.getConfigurationKeys().pipe(
      tap(configurationKeys => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          configurationKeys
        });
      },
      )
    );
  }

  //#endregion

  //#region Permissions

  @Action(GetPermissions)
  getPermissions(ctx: StateContext<CompaniesStateModel>, { companyId }: GetPermissions) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getPermissions(companyId)
      .pipe(
        tap(permissions => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            permissions
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(CreatePermissions)
  createPermissions(ctx: StateContext<CompaniesStateModel>, { companyId, permissions }: CreatePermissions) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.createPermissions(companyId, permissions)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Permissions were successfully created.'));
          ctx.dispatch(new GetPermissions(companyId));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(UpdatePermissions)
  updatePermission(ctx: StateContext<CompaniesStateModel>, { companyId, permission }: UpdatePermissions) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.updatePermissions(companyId, permission)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Permissions were successfully updated.'));
          ctx.dispatch(new HideSpinner());
          // ctx.dispatch(new GetCompany(companyId));
          // ctx.dispatch(new GetCompanies());
          // ctx.dispatch(new GetPermissions(companyId));
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  //#endregion

  //#region LiveStream

  @Action(GetLiveStreamCameras)
  getLiveStreamCameras(ctx: StateContext<CompaniesStateModel>, { companyId }: GetLiveStreamCameras) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getLiveStreamCameras(companyId).pipe(
      tap(cameras => {
        const state = ctx.getState();
        ctx.setState({
          ...state,
          cameras
        });
        ctx.dispatch(new HideSpinner());
      }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
    );
  }

  @Action(ClearLiveStreamCameras)
  clearLiveStreamCameras(ctx: StateContext<CompaniesStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      cameras: []
    });
  }

  @Action(CreateLiveStreamCamera)
  createCamera(ctx: StateContext<CompaniesStateModel>, { companyId, camera }: CreateLiveStreamCamera) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.createLiveStreamCamera(companyId, camera).pipe(
      tap(() => {
        ctx.dispatch(new ShowSuccessMessage('Camera was successfully created.'));
        ctx.dispatch(new HideSpinner());
        ctx.dispatch(new GetLiveStreamCameras(companyId));
      }, (err) => ctx.dispatch(new ShowErrorMessage(err.error)))
    );
  }

  @Action(UpdateLiveStreamCamera)
  updateLiveStreamCamera(ctx: StateContext<CompaniesStateModel>, { companyId, camera }: UpdateLiveStreamCamera) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.updateLiveStreamCamera(companyId, camera)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`${camera.name} was successfully updated.`));
          ctx.dispatch(new GetLiveStreamCameras(companyId));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(DeleteLiveStreamCamera)
  deleteLiveStreamCamera(ctx: StateContext<CompaniesStateModel>, { companyId, cameraId }: DeleteLiveStreamCamera) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.deleteLiveStreamCamera(companyId, cameraId)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`Camera was successfully deleted.`));
          ctx.dispatch(new GetLiveStreamCameras(companyId));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(GetLiveStreamAssets)
  getLiveStreamAssets(ctx: StateContext<CompaniesStateModel>, { id }) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getLiveStreamAssets(id)
      .pipe(
        tap(assets => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            livestreamAssets: assets
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(GetLiveStreamGroups)
  getLiveStreamGroups(ctx: StateContext<CompaniesStateModel>, { id }) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getLiveStreamGroups(id)
      .pipe(
        tap(groups => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            livestreamGroups: groups
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  //#endregion

  //#region Security Providers

  @Action(GetSecurityProviders)
  getSecurityProviders(ctx: StateContext<CompaniesStateModel>, { companyId }: GetSecurityProviders) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getCompanySettings(companyId)
      .pipe(
        tap(providers => {
          let webSecurityProvider: SecurityProvider; let desktopSecurityProvider: SecurityProvider;
          if (providers) {
            webSecurityProvider = providers.find(p => p.authType === AuthType.Web);
            desktopSecurityProvider = providers.find(p => p.authType === AuthType.Desktop);
          }
          const state = ctx.getState();
          ctx.setState({
            ...state,
            webSecurityProvider: webSecurityProvider,
            desktopSecurityProvider: desktopSecurityProvider
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(ClearSecurityProviders)
  clearSecurityProviders(ctx: StateContext<CompaniesStateModel>) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      webSecurityProvider: null,
      desktopSecurityProvider: null
    });
  }

  @Action(CreateSecurityProvider)
  createSecurityProvider(ctx: StateContext<CompaniesStateModel>, { companyId, securityProvider }: CreateSecurityProvider) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.createCompanySettings(companyId, securityProvider)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage('Settings were successfully created.'));
          ctx.dispatch(new GetSecurityProviders(companyId));
          ctx.dispatch(new HideSpinner());
        }, (err) => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(UpdateSecurityProvider)
  updateSecurityProvider(ctx: StateContext<CompaniesStateModel>, { companyId, securityProvider }: UpdateSecurityProvider) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.updateCompanySettings(companyId, securityProvider)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`Settings were successfully updated.`));
          ctx.dispatch(new GetSecurityProviders(companyId));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  //#endregion

  //#region Lookups

  @Action(GetLookups)
  getLookups(ctx: StateContext<CompaniesStateModel>, { companyId }: GetLookups) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getLookups(companyId)
      .pipe(
        tap(lookups => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            lookups: lookups
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(GetLookupTypes)
  getLookupTypes(ctx: StateContext<CompaniesStateModel>, { companyId }: GetLookupTypes) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getTypes(companyId)
      .pipe(
        tap(types => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            lookupTypes: types
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(CreateLookup)
  createLookup(ctx: StateContext<CompaniesStateModel>, { companyId, lookup }: CreateLookup) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.createLookup(companyId, lookup)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage("A new lookup was successfully created."));
          ctx.dispatch(new HideSpinner());
          ctx.dispatch(new GetLookups(companyId));
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }


  @Action(DeleteLookup)
  deleteLookup(ctx: StateContext<CompaniesStateModel>, { companyId, lookup }: DeleteLookup) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.deleteLookup(companyId, lookup)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage("Lookup value was successfully deleted."));
          ctx.dispatch(new HideSpinner());
          ctx.dispatch(new GetLookups(companyId));
          
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  //#endregion

  @Action(MigrateDatabase)
  migrateDatabase(ctx: StateContext<CompaniesStateModel>, { companyId }: MigrateDatabase) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.migrateDatabase(companyId)
      .pipe(
        tap(() => {
          ctx.dispatch(new ShowSuccessMessage(`Database was migrated successfully.`));
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(GetErrorMessages)
  getErrorMessages(ctx: StateContext<CompaniesStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.customersService.getErrorMessages()
      .pipe(
        tap(errors => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            errors
          });
          ctx.dispatch(new HideSpinner());
        },
          err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  //#endregion
}
