import { ConfigurationSetting } from 'src/app/core/models/entity/configuration';
import { Document } from '../../core/models/entity/document';
import { Permission } from 'src/app/core/models/entity/permissions';
import { SecurityProvider } from 'src/app/core/models/entity/security-provider';
import { LiveStreamCamera } from 'src/app/core/models/entity/camera';
import { Company } from 'src/app/core/models/entity/company';
import { Lookup } from 'src/app/core/models/entity/lookup';

//#region Companies

export class GetCompanies {
  static readonly type = '[Companies] GetCompanies';
}

export class GetCompany {
  static readonly type = '[Companies] GetCompany';

  constructor(public id: number) { }
}

export class ClearCurrentCompany {
  static readonly type = '[Companies] ClearCurrentCompany';
}

export class CreateCompany {
  static readonly type = '[Companies] CreateCompany';

  constructor(public company: Company) { }
}

export class UpdateCompany {
  static readonly type = '[Companies] UpdateCompany';

  constructor(public id: number, public company: Company, public customMessage?: string) { }
}

export class UpdateCompanyLogo {
  static readonly type = '[Companies] UpdateCompanyLogo';

  constructor(public logoUrl: string) { }
}

export class GetAzureWriteSASToken {
  static readonly type = '[Companies] GetAzureWriteSASToken';

  constructor(public companyId: number) { }
}

export class GetAzureReadSASToken {
  static readonly type = '[Companies] GetAzureReadSASToken';

  constructor(public companyId: number) { }
}

export class DeleteCompany {
  static readonly type = '[Companies] DeleteCompany';

  constructor(public company: Company) { }
}

export class SetCurrentCompanyId {
  static readonly type = '[Companies] SetCurrentCompanyId';

  constructor(public companyId: number) { }
}

//#endregion

//#region Configuration Settings

export class GetConfigurationSettings {
  static readonly type = '[Companies] GetConfigurationSettings';

  constructor(public companyId: number) { }
}

export class CreateConfigurationSetting {
  static readonly type = '[Companies] CreateConfigurationSetting';

  constructor(public companyId: number, public configuration: ConfigurationSetting) { }
}

export class UpdateConfigurationSetting {
  static readonly type = '[Configuration] UpdateConfigurationSetting';

  constructor(public companyId: number, public configurationSetting: ConfigurationSetting) { }
}

export class DeleteConfigurationSetting {
  static readonly type = '[Configuration] DeleteConfigurationSetting';

  constructor(public companyId: number, public key: string) { }
}

export class GetConfigurationKeys {
  static readonly type = '[Companies] GetConfigurationKeys';
}

export class ClearConfigurationSettings {
  static readonly type = '[Companies] ClearConfigurationSettings';
}

//#endregion

//#region Permissions

export class GetPermissions {
  static readonly type = '[Companies] GetPermissions';

  constructor(public companyId: number) { }
}

export class CreatePermissions {
  static readonly type = '[Companies] CreatePermissions';

  constructor(public companyId: number, public permissions: Permission[]) { }
}

export class UpdatePermissions {
  static readonly type = '[Companies] UpdatePermissions';

  constructor(public companyId: number, public permission: Permission[]) { }
}

//#endregion

//#region LiveStream

export class GetLiveStreamCameras {
  static readonly type = '[Companies] GetLiveStreamCameras';

  constructor(public companyId: number) { }
}

export class ClearLiveStreamCameras {
  static readonly type = '[Companies] ClearLiveStreamCameras';
}

export class CreateLiveStreamCamera {
  static readonly type = '[Companies] LiveStream';

  constructor(public companyId: number, public camera: LiveStreamCamera) { }
}

export class UpdateLiveStreamCamera {
  static readonly type = '[Companies] UpdateLiveStreamCamera';

  constructor(public companyId: number, public camera: LiveStreamCamera) { }
}

export class GetLiveStreamAssets {
  static readonly type = '[Media] GetLiveStreamAssets';

  constructor(public id: number) { }
}

export class GetLiveStreamGroups {
  static readonly type = '[Media] GetLiveStreamGroups';

  constructor(public id: number) { }
}

export class DeleteLiveStreamCamera {
  static readonly type = '[Companies]  DeleteLiveStreamCamera';

  constructor(public companyId: number, public cameraId: number) { }
}

//#endregion

//#region Security Providers

export class GetSecurityProviders {
  static readonly type = '[Companies] GetSecurityProviders';

  constructor(public companyId: number) { }
}

export class ClearSecurityProviders {
  static readonly type = '[Companies] ClearSecurityProviders';
}

export class CreateSecurityProvider {
  static readonly type = '[Companies] CreateSecurityProvider';

  constructor(public companyId: number, public securityProvider: SecurityProvider) { }
}

export class UpdateSecurityProvider {
  static readonly type = '[Companies] UpdateSecurityProvider';

  constructor(public companyId: number, public securityProvider: SecurityProvider) { }
}

//#endregion

//#region Lookups

export class GetLookups {
  static readonly type = '[Companies] GetLookups';

  constructor(public companyId: number) { }
}

export class GetLookupTypes {
  static readonly type = '[Companies] GetLookupTypes';

  constructor(public companyId: number) { }
}

export class CreateLookup {
  static readonly type = '[Companies] CreateLookup';

  constructor(public companyId: number, public lookup: Lookup) { }
}

export class UpdateLookup {
  static readonly type = '[Companies] UpdateLookup';

  constructor(public companyId: number, public lookup: Lookup) { }
}

export class DeleteLookup {
  static readonly type = '[Companies]  DeleteLookup';

  constructor(public companyId: number, public lookup: Lookup) { }
}

//#endregion

export class MigrateDatabase {
  static readonly type = '[Companies] MigrateDatabase';

  constructor(public companyId: number) { }
}

export class GetErrorMessages {
  static readonly type = '[Companies] GetErrorMessages';
}
