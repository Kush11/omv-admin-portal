import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { DataStateChangeEventArgs } from '@syncfusion/ej2-treegrid';
import { map } from 'rxjs/internal/operators/map';
import { Directory } from 'src/app/core/models/entity/directory';
import { DirectoryService } from 'src/app/core/services/business/directory/directory.service';
import { CustomersService } from 'src/app/core/services/business/customers/customers.service';

@Injectable({
  providedIn: "root"
})
export class CompanyLiveStreamService extends Subject<Object> {

  directories: Directory[] = [];

  constructor(private customerService: CustomersService) {
    super();
  }

  public getData(companyId: number): Observable<DataStateChangeEventArgs> {
    return this.customerService.getDirectories(companyId)
      .pipe(
        map((response: any) => {
          this.directories = response;
          return (<any>{
            result: response,
            count: parseInt('8', 10)
          });
        })
      );
  }

  public execute(companyId: number): void {
    this.getData(companyId)
      .subscribe(x => {
        super.next(x)
      });
  }
}