import { CreateLiveStreamCamera, GetLiveStreamCameras, UpdateLiveStreamCamera, GetLiveStreamAssets, GetLiveStreamGroups, DeleteLiveStreamCamera, ClearLiveStreamCameras } from './../../state/companies.action';
import { Component, OnInit, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { CompaniesState } from '../../state/companies.state';
import { Observable } from 'rxjs/internal/Observable';
import { LiveStreamCamera } from 'src/app/core/models/entity/camera';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { SubSink } from 'subsink/dist/subsink';
import { GridColumnButton, GridColumn } from 'src/app/core/models/entity/grid.column';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Company } from 'src/app/core/models/entity/company';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { CompanyLiveStreamService } from './company-live-stream.service';
import { DataStateChangeEventArgs } from '@syncfusion/ej2-treegrid';
import { TreeGridComponent } from '@syncfusion/ej2-angular-treegrid';
import { Directory } from 'src/app/core/models/entity/directory';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { CustomersService } from 'src/app/core/services/business/customers/customers.service';
import { RowSelectEventArgs, QueryCellInfoEventArgs } from '@syncfusion/ej2-grids/src/grid/base/interface';

@Component({
  selector: 'app-company-live-stream',
  templateUrl: './company-live-stream.component.html',
  styleUrls: ['./company-live-stream.component.css']
})
export class CompanyLiveStreamComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(CompaniesState.getCameras) cameras$: Observable<LiveStreamCamera>;
  @Select(CompaniesState.getCurrentCompanyId) currentCompanyId$: Observable<number>;
  @Select(CompaniesState.getCurrentCompany) currentCustomer$: Observable<Company>;
  @Select(CompaniesState.getLiveStreamAssets) livestreamAssets$: Observable<Lookup[]>;
  @Select(CompaniesState.getLiveStreamGroups) livestreamGroups$: Observable<Lookup[]>;

  @ViewChild('cameraModal', {static: false}) cameraModal: DialogComponent;
  @ViewChild('confirmModal', {static: false}) confirmModal: ModalComponent;
  @ViewChild('treegrid', {static: false}) treegrid: TreeGridComponent;

  @Output() edit = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();

  checkboxTemplate = `<input class="c-checkbox-default" type="checkbox" title="" (click)="toggleFavoriteEvent(data)">`;
  notFavoriteTemplate = `<input class="c-checkbox-star" type="checkbox" title="" (click)="toggleFavoriteEvent(data)">`;
  favoriteTemplate = `<input class="c-checkbox-star" type="checkbox" title="" checked (click)="toggleFavoriteEvent(data)">`;
  downloadTemplate = `<button class="button-no-outline button-grid-link" (click)="downloadEvent(data)"> Download </button>`;
  editTemplate = `<img src="./assets/images/icon-pencil.svg" width="18" (click)="editEvent(data)">`;


  private subs = new SubSink();
  actions: GridColumnButton[] = [
    { type: 'button', text: 'Delete', visible: true, action: this.showDeleteModal.bind(this) },
    { type: 'button', text: 'Edit', visible: true, action: this.showCameraModal.bind(this) }
  ];
  columns: GridColumn[] = [
    { headerText: 'Name', field: 'name', filterType: 'checkbox', width: 90, useAsLink: true, action: this.showCameraModal.bind(this) },
    { headerText: 'Source', field: 'source', ignoreFiltering: true, width: 200 },
    { headerText: 'Asset', field: 'assetName', ignoreFiltering: true, width: 120 },
    { headerText: 'Group', field: 'groupName', ignoreFiltering: true, width: 120 },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 120 }
  ];
  lookupFields = { text: 'description', value: 'value' };
  cameraForm: FormGroup;
  cameras: LiveStreamCamera;
  selectedCameraId: number;
  selectedCamera: LiveStreamCamera;
  deleteMessage: string;
  isEdit: boolean;
  currentCompanyId: number;
  currentCamera: LiveStreamCamera;
  canManageLiveStream: boolean;

  data: Observable<DataStateChangeEventArgs>;
  state: DataStateChangeEventArgs;
  directories: Directory[];
  isDestinationSelected: boolean;
  directoryPath: string;
  currentDirectoryId: any;
  treeColumnName = 'name';

  constructor(private fb: FormBuilder, protected store: Store, private service: CompanyLiveStreamService, private customerService: CustomersService) {
    super(store);
    this.data = service;
    this.cameraForm = this.fb.group({
      name: ['', [Validators.required]],
      source: ['', [Validators.required]],
      asset: ['', [Validators.required]],
      group: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.subs.add(
      this.currentCustomer$
        .subscribe(company => {
          if (company) {
            this.currentCompanyId = company.id;
            this.service.execute(this.currentCompanyId);
            if (company.connectionString) {
              this.canManageLiveStream = true;
              this.store.dispatch([new GetLiveStreamAssets(this.currentCompanyId), new GetLiveStreamGroups(this.currentCompanyId)]);
              this.store.dispatch(new GetLiveStreamCameras(this.currentCompanyId));
            } else {
              this.store.dispatch(new ClearLiveStreamCameras());
            }
          }
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  dataStateChange(state: DataStateChangeEventArgs) {
    if (state.requestType === 'expand') {
      let folder = state.data as Directory;
      this.treegrid.showSpinner();
      this.customerService.getDirectories(this.currentCompanyId, folder.id)
        .subscribe(folders => {
          state.childData = folders;
          state.childDataBind();
          this.treegrid.hideSpinner();
        }, err => {
          this.showErrorMessage(err.message);
          this.treegrid.hideSpinner();
        });
    } else {
      console.log("dataStateChange else: ", state);
      this.service.execute(this.currentCompanyId);
    }
  }

  addCamera() {
    this.currentDirectoryId = 0;
    this.directoryPath = '';
    this.isDestinationSelected = false;
    this.cameraForm.reset();
    this.isEdit = false;
    this.currentCamera = null;
    this.cameraModal.show();
  }

  showCameraModal(camera: LiveStreamCamera) {
    this.isEdit = true;
    this.directoryPath = '';
    this.cameraForm.reset();
    if (camera) {
      this.currentCamera = camera;
      this.cameraForm.setValue({
        source: camera.source,
        name: camera.name,
        asset: camera.asset,
        group: camera.group,
      });
      this.directoryPath = camera.directoryPath;
      this.isDestinationSelected = this.directoryPath != null && this.directoryPath != '';
      this.cameraModal.show();
    }
  }

  showDeleteModal(args: LiveStreamCamera) {
    this.selectedCameraId = args.id;
    this.selectedCamera = args;
    this.deleteMessage = `Are you sure you want to delete "${args.name}"?`;
    this.confirmModal.show();
  }

  saveCamera() {
    if (this.cameraForm.valid) {
      const camera: LiveStreamCamera = { ...this.currentCamera, ...this.cameraForm.value };
      camera.directoryId = this.currentDirectoryId;
      if (this.isEdit) {
        this.store.dispatch(new UpdateLiveStreamCamera(this.currentCompanyId, camera)).toPromise()
          .then(() => {
            this.cameraForm.reset();
          });
      } else {
        this.store.dispatch(new CreateLiveStreamCamera(this.currentCompanyId, camera)).toPromise()
          .then(() => {
            this.cameraForm.reset();
          });
      }
      this.cameraModal.hide();
    }
  }

  removeCamera() {
    this.store.dispatch(new DeleteLiveStreamCamera(this.currentCompanyId, this.selectedCameraId));
  }

  closeDialog() {
    this.directoryPath = '';
    this.cameraModal.hide();
    this.cameraForm.reset();
  }

  //#region Tree Grid Events

  changeDestination() {
    this.isDestinationSelected = false;
  }

  rowSelected(args: RowSelectEventArgs) {
    var list = args.target.classList;
    let isRowCollapsedOrExpanded = list.contains('expand') || list.contains('collapse') || list.contains('e-treegridcollapse') || list.contains('e-treegridexpand');
    if (isRowCollapsedOrExpanded) return;

    let data = args.data as any;
    if (data) {
      this.cameraForm.markAsDirty();
      this.isDestinationSelected = true;
      this.currentDirectoryId = data.id;
      this.directoryPath = '';
      this.buildFolderPath(this.currentDirectoryId);
    }
  }

  addFolderIcon(args: QueryCellInfoEventArgs) {
    if (args.column.field === this.treeColumnName) {
        let imgElement: HTMLElement = document.createElement('IMG');
        imgElement.setAttribute('src', './assets/images/icon-folder.svg');
        imgElement.classList.add('folder');
        let div: HTMLElement = document.createElement('DIV');
        div.style.display = 'inline-block';
        div.appendChild(imgElement);
        let cellValue: HTMLElement = document.createElement('DIV');
        if (args.cell.querySelector('.e-treecell')) {
          cellValue.innerHTML = args.cell.querySelector('.e-treecell').innerHTML;
          cellValue.setAttribute('style', 'display:inline-block;padding-left:6px');
          args.cell.querySelector('.e-treecell').innerHTML = '';
          args.cell.querySelector('.e-treecell').appendChild(div);
          args.cell.querySelector('.e-treecell').appendChild(cellValue);
        }
    }
  }

  private buildFolderPath(directoryId: number) {
    let directory = this.service.directories.find(x => x.id === directoryId);
    let parent = this.service.directories.find(x => x.id === directory.parentId);
    if (parent) {
      this.directoryPath = this.directoryPath ? `${directory.name} > ${this.directoryPath}` : `${directory.name}`;
      return this.buildFolderPath(parent.id);
    }
    return this.directoryPath = this.directoryPath ? `${directory.name} > ${this.directoryPath}` : `${directory.name}`;
  }

  //#endregion
}
