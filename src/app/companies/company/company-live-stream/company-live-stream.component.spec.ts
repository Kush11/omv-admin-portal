import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyLiveStreamComponent } from './company-live-stream.component';

describe('CompanyLiveStreamComponent', () => {
  let component: CompanyLiveStreamComponent;
  let fixture: ComponentFixture<CompanyLiveStreamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyLiveStreamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyLiveStreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
