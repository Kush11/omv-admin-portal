import { GetAzureWriteSASToken, GetAzureReadSASToken } from './../state/companies.action';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { Company } from '../../core/models/entity/company';
import { ConfigurationSetting } from './../../core/models/entity/configuration';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SubSink } from 'subsink/dist/subsink';
import { Tab } from 'src/app/core/models/entity/tab';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { ConfigurationSettingKey } from '../../core/enum/configuration-setting-key';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ModalConfirmComponent } from 'src/app/shared/modal/modal-confirm.component';
import { CompaniesService } from '../../core/services/business/companies.service';
import { ShowSpinner, ShowWarningMessage } from 'src/app/state/app.actions';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { CompaniesState } from '../state/companies.state';
import { SetCurrentCompanyId, GetCompany, DeleteCompany, UpdateCompany, ClearCurrentCompany } from '../state/companies.action';
import { DetailCardComponent } from 'src/app/shared/detail-card/detail-card.component';
import { CompanyService } from './company.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent extends BaseComponent implements OnInit {

  @Select(CompaniesState.getConfigurationSettings) configurationSettings$: Observable<ConfigurationSetting[]>;
  @Select(CompaniesState.getCurrentCompany) currentCompany$: Observable<Company>;
  @Select(CompaniesState.getAzureWriteSASToken) azureWriteSASToken$: Observable<string>;

  @ViewChild('fieldDialog', {static: false}) fieldDialogList: DialogComponent;
  @ViewChild('confirmModal', {static: false}) confirmModal: ModalConfirmComponent;
  @ViewChild('detailCard', {static: false}) detailCard: DetailCardComponent;
  @ViewChild('file', {static: false}) file: any;
  modalPosition: Object = { X: 'center', Y: 'center' };

  private subs = new SubSink();
  tabs: Tab[];
  companyId: number;
  currentCompany: Company;
  deleteMessage: string;
  isEdit: boolean;
  currentCustomerField: any;
  companyForm: FormGroup;
  customerId: any;
  items: any;
  sasToken = '';
  storageAccount = '';
  mediaContainer = '';
  customerConfiguration: ConfigurationSetting;

  get pointsOfContact() {
    return this.companyForm.get('pointsOfContact') as FormArray;
  }
  get hostHeader() {
    return this.companyForm.get('hostHeader');
  }

  constructor(private router: Router, protected store: Store, private activatedRoute: ActivatedRoute, private fb: FormBuilder, private companyService: CompanyService) {
    super(store);
    this.companyForm = this.fb.group({
      name: ['', [Validators.required]],
      companyType: ['', [Validators.required]],
      contractType: ['', [Validators.required]],
      hostHeader: ['', [Validators.required]],
      pointsOfContact: this.fb.array([])
    });
  }

  ngOnInit() {
    this.subs.add(
      this.activatedRoute.paramMap
        .subscribe(params => {
          this.companyId = Number(params.get('id'));
          this.store.dispatch(new GetAzureWriteSASToken(this.companyId));
          this.store.dispatch(new GetAzureReadSASToken(this.companyId));
          if (this.companyId && this.companyId != 0) {
            this.store.dispatch(new SetCurrentCompanyId(this.companyId));
            this.store.dispatch(new GetCompany(this.companyId));
            this.tabs = [
              { link: `/companies/${this.companyId}/configurations`, name: 'Configurations', isActive: true },
              { link: `/companies/${this.companyId}/security-providers`, name: 'Security Providers' },
              { link: `/companies/${this.companyId}/lookups`, name: 'Lookups' },
              { link: `/companies/${this.companyId}/live-stream`, name: 'Live Stream' },
              // { link: `/companies/${this.companyId}/errors`, name: 'Errors' }
            ];
          }
        }),
      this.azureWriteSASToken$
        .subscribe(writeSASToken => {
          this.sasToken = writeSASToken;
        }),
      this.configurationSettings$
        .subscribe(settings => {
          if (settings) {
            const mediaContainerSetting = settings.find(s => s.key === ConfigurationSettingKey.MediaContainer);
            this.mediaContainer = mediaContainerSetting ? mediaContainerSetting.value : '';
            const storageAccountSetting = settings.find(s => s.key === ConfigurationSettingKey.StorageAccount);
            this.storageAccount = storageAccountSetting ? storageAccountSetting.value : '';
          } else {
            this.sasToken = this.mediaContainer = this.storageAccount = '';
          }
        }),
      this.currentCompany$
        .subscribe(company => this.currentCompany = company),
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearCurrentCompany());
  }

  addPointOfContact() {
    this.pointsOfContact.push(this.fb.control('', [Validators.required, Validators.email]));
  }

  removePointOfContact(index: number) {
    this.companyForm.get('pointsOfContact').markAsDirty();
    this.pointsOfContact.removeAt(index);
  }

  removeCompany() {
    this.store.dispatch(new DeleteCompany(this.currentCompany));
    const pageView = sessionStorage.getItem('view');
    this.router.navigate(['companies'], { queryParams: { view: pageView } });
  }

  updateCompany() {
    const company: Company = { ...this.currentCompany, ...this.companyForm.value };
    this.store.dispatch(new UpdateCompany(company.id, company)).toPromise()
      .then(() => {
        this.store.dispatch(new GetCompany(company.id));
      });
    this.companyForm.reset();
    this.closeDialog();
  }

  onEdit(customer: Company) {
    this.isEdit = true;
    if (customer) {
      this.currentCustomerField = customer;
      this.companyForm.patchValue({
        name: this.currentCompany.name,
        companyType: this.currentCompany.companyType,
        contractType: this.currentCompany.contractType,
        hostHeader: customer.hostHeader,
      });
      this.setPointsOfContact();
    }
    this.fieldDialogList.show();
  }

  onDelete(customer: Company) {
    this.currentCompany = customer;
    this.deleteMessage = `Are you sure you want to deactivate "${customer.name}"?`;
    this.confirmModal.show();
  }

  onImpersonate(customer: Company) {
    if (!customer.hostHeader) {
      return this.store.dispatch(new ShowWarningMessage('Host Header has not been set.'));
    }
    if (this.isUrlValid(customer.hostHeader)) {
      window.open(customer.hostHeader, '_blank');
    } else {
      window.open(`https://${customer.hostHeader}`, '_blank');
    }
  }

  switchTabs(tabLink: string) {
    this.router.navigate([tabLink]);
  }

  closeDialog() {
    this.companyForm.reset();
    this.pointsOfContact.controls.splice(0);
    this.fieldDialogList.hide();
  }

  //#region Upload Logo

  onCheckCanUpload() {
    if (this.sasToken && this.mediaContainer && this.storageAccount) {
      this.detailCard.showUploadModal();
    } else {
      let message = !this.sasToken && !this.mediaContainer && !this.storageAccount ? 'SAS Token, Media Container and Storage Account' :
        !this.sasToken && !this.mediaContainer && this.storageAccount ? 'SAS Token and Media Container' :
          !this.sasToken && this.mediaContainer && !this.storageAccount ? 'SAS Token and Storage Account' :
            this.sasToken && !this.mediaContainer && !this.storageAccount ? 'Media Container and Storage Account' :
              !this.sasToken && this.mediaContainer && this.storageAccount ? 'SAS Token' :
                this.sasToken && !this.mediaContainer && this.storageAccount ? 'Media Container' :
                  this.sasToken && this.mediaContainer && !this.storageAccount ? 'Storage Account' :
                    '';
      message = `Please provide ${message} for this company to upload a logo.`;
      this.store.dispatch(new ShowWarningMessage(message));
    }
  }

  onLogoChanged(file: File) {
    this.companyService.upload(file, this.mediaContainer, this.sasToken, this.storageAccount);
  }

  //#endregion

  private setPointsOfContact() {
    this.currentCustomerField.pointsOfContact.map(p => {
      const control = new FormControl(p, [Validators.required, Validators.email]);
      const pointsGroup = this.companyForm as FormGroup;
      (pointsGroup.controls.pointsOfContact as FormArray).push(control);
    });
  }

  private isUrlValid(url: string): boolean {
    return /^(http|https|ftp):\/\/.*$/.test(url);
  }
}

