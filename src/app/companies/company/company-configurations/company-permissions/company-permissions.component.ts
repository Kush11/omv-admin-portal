import { Component, OnInit, Input } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SubSink } from 'subsink/dist/subsink';
import { GetPermissions, UpdatePermissions, CreatePermissions } from 'src/app/companies/state/companies.action';
import { CompaniesState } from 'src/app/companies/state/companies.state';
import { Permission } from './../../../../core/models/entity/permissions';
import { Button } from 'src/app/core/models/entity/button';

@Component({
  selector: 'app-company-permissions',
  templateUrl: './company-permissions.component.html',
  styleUrls: ['./company-permissions.component.css']
})
export class CompanyPermissionsComponent implements OnInit {

  @Input() companyId: number;

  @Select(CompaniesState.getPermissions) permissions$: Observable<Permission[]>;

  private subs = new SubSink();
  permissionName: string;
  dashboard = 'Dashboard';
  media = 'Media';
  liveStream = 'Live Stream';
  workPlanning = 'Work Planning';
  collaboration = 'Collaborations';
  permissions: Permission[] = [
    { menuName: this.dashboard, isPermitted: false },
    { menuName: this.media, isPermitted: false },
    { menuName: this.liveStream, isPermitted: false },
    { menuName: this.workPlanning, isPermitted: false },
    { menuName: this.collaboration, isPermitted: false }
  ];
  companyPermissions: Permission[] = [];
  index: number;
  buttons: Button[] = [
    { text: 'SAVE CHANGES', type: 'submit', cssClass: 'button-round modal-save-changes-qa', action: this.saveSettings.bind(this) },
    { text: 'Cancel', type: 'button', cssClass: 'button-no-outline modal-cancel-qa', action: this.cancel.bind(this) },
  ];

  constructor(protected store: Store) {
  }

  ngOnInit() {
    this.store.dispatch(new GetPermissions(this.companyId));
    this.subs.add(
      this.permissions$
        .subscribe(permissions => {
          this.permissions.forEach(permission => permission.isPermitted = false);
          if (permissions) {
            this.companyPermissions = permissions;
            permissions.forEach(permission => {
              this.index = this.permissions.findIndex(c => c.menuName === permission.menuName);
              const menuPermission = this.permissions[this.index];
              if (menuPermission) {
                menuPermission.isPermitted = permission.isPermitted;
              }
            });
          }
        })
    );
  }

  onCheckChange(event: any, value?: any) {
    this.permissions[value].isPermitted = event.checked;
  }

  saveSettings() {
    if (this.index) {
      this.store.dispatch(new UpdatePermissions(this.companyId, this.permissions));
    } else {
      this.store.dispatch(new CreatePermissions(this.companyId, this.permissions));
    }
  }

  cancel() {
    if (this.companyPermissions.length > 0) {
      this.companyPermissions.forEach(permission => {
        this.index = this.permissions.findIndex(c => c.menuName === permission.menuName);
        this.permissions[this.index].isPermitted = permission.isPermitted;
      });
    } else {
      this.permissions.forEach(permission => permission.isPermitted = false);
    }
  }
}
