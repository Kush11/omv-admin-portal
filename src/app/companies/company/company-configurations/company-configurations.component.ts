import { ConfigurationSetting } from 'src/app/core/models/entity/configuration';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Company } from 'src/app/core/models/entity/company';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { SubSink } from 'subsink/dist/subsink';
import { GridColumn, GridColumnButton } from 'src/app/core/models/entity/grid.column';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { CompaniesState } from '../../state/companies.state';
import { GetConfigurationKeys, DeleteConfigurationSetting, UpdateConfigurationSetting, CreateConfigurationSetting, MigrateDatabase, UpdateCompany, GetCompany, GetConfigurationSettings } from '../../state/companies.action';
import { Button } from 'src/app/core/models/entity/button';

@Component({
  selector: 'app-company-configurations',
  templateUrl: './company-configurations.component.html',
  styleUrls: ['./company-configurations.component.css']
})
export class CompanyConfigurationsComponent extends BaseComponent implements OnInit {

  @Select(CompaniesState.getConfigurationSettings) configurationSettings$: Observable<ConfigurationSetting[]>;
  @Select(CompaniesState.getCurrentCompanyId) currentCompanyId$: Observable<number>;
  @Select(CompaniesState.getConfigurationKeys) configurationKeys$: Observable<Lookup[]>;
  @Select(CompaniesState.getCurrentCompany) currentCompany$: Observable<Company>;

  @ViewChild('configurationSettingDialog', {static: false}) configurationSettingDialog: DialogComponent;
  @ViewChild('confirmModal', {static: false}) confirmModal: ModalComponent;

  private subs = new SubSink();
  fieldForm: FormGroup;
  companyId: number;
  showParentField: boolean;
  showListControl: boolean;
  dbButtons: Button[] = [
    { text: 'SAVE CHANGES', type: 'submit', cssClass: 'button-round modal-save-changes-qa', disabled: true, action: this.saveConnectionString.bind(this) },
    { text: 'Cancel', type: 'button', cssClass: 'button-no-outline modal-cancel-changes-qa', action: this.cancelConnectionString.bind(this) },
  ];
  actions: GridColumnButton[] = [
    { type: 'button', text: 'Delete', visible: true, action: this.onDelete.bind(this) },
    { type: 'button', text: 'Edit', visible: true, action: this.onEdit.bind(this) }
  ];
  columns: GridColumn[] = [
    { headerText: 'Key', field: 'key', filterType: 'checkbox', width: 200 },
    { headerText: 'Value', field: 'value', ignoreFiltering: true, width: 200 },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 120 }
  ];
  deleteMessage: string;
  currentCustumerField: ConfigurationSetting;
  isEdit: boolean;
  configurationForm: FormGroup;
  selectedSection: string;
  unSetConfigurationKeys: Lookup[];
  selectedConfigurationSetting: ConfigurationSetting;
  currentCompanyId: number;
  configurationKeys: Lookup[];
  fields = { text: 'description', value: 'value' };
  connectionString = '';
  currentCompany: Company;
  databaseForm: FormGroup;
  configurationSettings: ConfigurationSetting[];

  constructor(protected store: Store, private fb: FormBuilder) {
    super(store);
    this.databaseForm = this.fb.group({
      connectionString: ['']
    });
    this.configurationForm = this.fb.group({
      key: ['', [Validators.required, Validators.pattern(/^[a-zA-Z_]\w*(\.[a-zA-Z_]\w*)*$/)]],
      value: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.store.dispatch(new GetConfigurationKeys());
    this.subs.add(
      this.currentCompanyId$
        .subscribe(companyId => {
          this.currentCompanyId = companyId;
          if (this.currentCompanyId)
            this.store.dispatch(new GetConfigurationSettings(this.currentCompanyId));
        }),
      this.configurationSettings$
        .subscribe(configurationSettings => {
          this.configurationSettings = configurationSettings;
          if (this.configurationSettings && this.configurationKeys) {
            const keys = this.configurationSettings.map(x => x.key);
            this.unSetConfigurationKeys = this.configurationKeys.filter(key => !keys.includes(key.value));
          }
        }),
      this.configurationKeys$
        .subscribe(configurationKeys => {
          this.configurationKeys = configurationKeys;
          if (this.configurationSettings && this.configurationKeys) {
            const keys = this.configurationSettings.map(x => x.key);
            this.unSetConfigurationKeys = this.configurationKeys.filter(key => !keys.includes(key.value));
          }
        }),
      this.currentCompany$
        .subscribe(company => {
          if (company) {
            this.currentCompany = JSON.parse(JSON.stringify(company));
            this.databaseForm.setValue({
              connectionString: this.currentCompany.connectionString
            });
          }
        }),
      this.databaseForm.valueChanges
        .subscribe(() => {
          this.dbButtons[0].disabled = !this.databaseForm.dirty || !this.databaseForm.valid;
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  //#region Database Section

  onConnectionStringChanged(value: string) {
    this.dbButtons[0].disabled = this.currentCompany.connectionString === this.connectionString;
    this.connectionString = value;
  }

  saveConnectionString() {
    this.databaseForm.reset(this.databaseForm.value);
    const company: Company = { ...this.currentCompany, ...this.databaseForm.value };
    this.store.dispatch(new UpdateCompany(company.id, company)).toPromise()
      .then(() => {
        this.store.dispatch(new GetCompany(this.currentCompany.id));
      });
  }

  cancelConnectionString() {
    this.databaseForm.reset();
    this.databaseForm.setValue({
      connectionString: this.currentCompany.connectionString
    });
  }

  //#endregion

  //#region Configuration Setting

  onCreate() {
    this.isEdit = false;
    this.configurationSettingDialog.show();
  }

  saveConfiguration() {
    if (this.configurationForm.valid && this.configurationForm.dirty) {
      const configurationSetting: ConfigurationSetting = { ...this.selectedConfigurationSetting, ...this.configurationForm.value };
      if (this.isEdit) {
        this.store.dispatch(new UpdateConfigurationSetting(this.currentCompanyId, configurationSetting));
      } else {
        this.store.dispatch(new CreateConfigurationSetting(this.currentCompanyId, configurationSetting));
      }
      this.closeDialog();
    }
  }

  onEdit(configurationSetting: ConfigurationSetting) {
    this.isEdit = true;
    this.selectedConfigurationSetting = configurationSetting;
    if (configurationSetting) {
      console.log("sdfas: ", configurationSetting.key);

      this.configurationForm.setValue({
        key: configurationSetting.key,
        value: configurationSetting.value
      });
    }
    this.configurationSettingDialog.show();
  }

  onDelete(configurationSetting: ConfigurationSetting) {
    this.selectedConfigurationSetting = configurationSetting;
    this.deleteMessage = `Are you sure you want to delete "${configurationSetting.key}"?`;
    this.confirmModal.show();
  }

  removeConfiguration() {
    this.store.dispatch(new DeleteConfigurationSetting(this.currentCompany.id, this.selectedConfigurationSetting.key));
  }

  closeDialog() {
    this.configurationForm.reset();
    this.configurationSettingDialog.hide();
  }

  //#endregion

  // database migration
  migrateDB() {
    this.store.dispatch(new MigrateDatabase(this.currentCompanyId));
  }
}
