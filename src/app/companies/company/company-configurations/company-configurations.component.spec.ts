import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyConfigurationsComponent } from './company-configurations.component';

describe('CompanyConfigurationsComponent', () => {
  let component: CompanyConfigurationsComponent;
  let fixture: ComponentFixture<CompanyConfigurationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyConfigurationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyConfigurationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
