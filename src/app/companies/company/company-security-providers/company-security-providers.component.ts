import { SecurityProvider } from '../../../core/models/entity/security-provider';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import { SubSink } from 'subsink/dist/subsink';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { CompaniesState } from '../../state/companies.state';
import { GetSecurityProviders, CreateSecurityProvider, UpdateSecurityProvider, ClearSecurityProviders } from '../../state/companies.action';
import { Button } from 'src/app/core/models/entity/button';
import { AuthType } from 'src/app/core/enum/auth-type';

@Component({
  selector: 'app-company-security-providers',
  templateUrl: './company-security-providers.component.html',
  styleUrls: ['./company-security-providers.component.css']
})
export class CompanySecurityProvidersComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(CompaniesState.getWebSecurityProvider) webSecurityProvider$: Observable<SecurityProvider>;
  @Select(CompaniesState.getDesktopSecurityProvider) desktopSecurityProvider$: Observable<SecurityProvider>;
  @Select(CompaniesState.getCurrentCompanyId) currentCompanyId$: Observable<number>;

  @ViewChild('fieldDialog', {static: false}) fieldDialogList: DialogComponent;
  @ViewChild('confirmModal', {static: false}) confirmModal: ModalComponent;

  private subs = new SubSink();
  companyId: number;
  selectedAuth: number;
  webButtons: Button[] = [
    { text: 'SAVE CHANGES', type: 'submit', cssClass: 'button-round modal-save-changes-qa', disabled: true, action: this.saveWebSettings.bind(this) },
    { text: 'Cancel', type: 'button', cssClass: 'button-no-outline modal-cancel-changes-qa', action: this.cancelWebSettings.bind(this) },
  ];
  desktopButtons: Button[] = [
    { text: 'SAVE CHANGES', type: 'submit', cssClass: 'button-round modal-save-changes-qa', disabled: true, action: this.saveDesktopSettings.bind(this) },
    { text: 'Cancel', type: 'button', cssClass: 'button-no-outline modal-cancel-changes-qa', action: this.cancelDesktopSettings.bind(this) },
  ];
  deleteMessage: string;
  isEdit: boolean;
  webForm: FormGroup;
  desktopForm: FormGroup;
  webSecurityProvider: SecurityProvider;
  desktopSecurityProvider: SecurityProvider;

  constructor(protected store: Store, private fb: FormBuilder) {
    super(store);
    this.webForm = this.fb.group({
      issuerUrl: ['', [Validators.required]],
      clientId: ['', [Validators.required]],
      authServerId: ['', [Validators.required]]
    });
    this.desktopForm = this.fb.group({
      issuerUrl: ['', [Validators.required]],
      clientId: ['', [Validators.required]],
      authServerId: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.subs.add(
      this.currentCompanyId$
        .subscribe(id => {
          this.companyId = id;
          if (this.companyId)
            this.store.dispatch(new GetSecurityProviders(this.companyId));
        }),
      this.webSecurityProvider$
        .subscribe(provider => {
          this.webForm.setValue({
            issuerUrl: provider ? provider.issuerUrl : '',
            clientId: provider ? provider.clientId : '',
            authServerId: provider ? provider.authServerId : ''
          });
          this.webSecurityProvider = provider;
        }),
      this.desktopSecurityProvider$
        .subscribe(provider => {
          this.desktopForm.setValue({
            issuerUrl: provider ? provider.issuerUrl : '',
            clientId: provider ? provider.clientId : '',
            authServerId: provider ? provider.authServerId : ''
          });
          this.desktopSecurityProvider = provider;
        }),
      this.webForm.valueChanges
        .subscribe(() => {
          this.webButtons[0].disabled = !this.webForm.dirty || !this.webForm.valid;
        }),
      this.desktopForm.valueChanges
        .subscribe(() => {
          this.desktopButtons[0].disabled = !this.desktopForm.dirty || !this.desktopForm.valid;
        }),
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
    this.store.dispatch(new ClearSecurityProviders());
  }

  //#region Web Settings

  saveWebSettings() {
    this.webForm.reset(this.webForm.value);
    const provider: SecurityProvider = { ...this.webSecurityProvider, ...this.webForm.value };
    if (!this.webSecurityProvider) {
      provider.authType = AuthType.Web;
      this.store.dispatch(new CreateSecurityProvider(this.companyId, provider));
    } else {
      this.store.dispatch(new UpdateSecurityProvider(this.companyId, provider));
    }
  }

  cancelWebSettings() {
    this.webForm.reset();
    this.webForm.setValue({
      issuerUrl: this.webSecurityProvider ? this.webSecurityProvider.issuerUrl : '',
      clientId: this.webSecurityProvider ? this.webSecurityProvider.clientId : '',
      authServerId: this.webSecurityProvider ? this.webSecurityProvider.authServerId : ''
    });
  }

  //#endregion

  //#region Desktop Settings

  saveDesktopSettings() {
    this.desktopForm.reset(this.desktopForm.value);
    const provider: SecurityProvider = { ...this.desktopSecurityProvider, ...this.desktopForm.value };
    if (!this.desktopSecurityProvider) {
      provider.authType = AuthType.Desktop;
      this.store.dispatch(new CreateSecurityProvider(this.companyId, provider));
    } else {
      this.store.dispatch(new UpdateSecurityProvider(this.companyId, provider));
    }
  }

  cancelDesktopSettings() {
    this.desktopForm.reset();
    this.desktopForm.setValue({
      issuerUrl: this.desktopSecurityProvider ? this.desktopSecurityProvider.issuerUrl : '',
      clientId: this.desktopSecurityProvider ? this.desktopSecurityProvider.clientId : '',
      authServerId: this.desktopSecurityProvider ? this.desktopSecurityProvider.authServerId : ''
    });
  }

  //#endregion
}
