import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanySecurityProvidersComponent } from './company-security-providers.component';

describe('CompanySecurityProvidersComponent', () => {
  let component: CompanySecurityProvidersComponent;
  let fixture: ComponentFixture<CompanySecurityProvidersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanySecurityProvidersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanySecurityProvidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
