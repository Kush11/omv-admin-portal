import { Injectable } from "@angular/core";
import { Store } from '@ngxs/store';
import { BlobService, UploadParams } from 'angular-azure-blob-service';
import { Company } from 'src/app/core/models/entity/company';
import { ShowErrorMessage } from 'src/app/state/app.actions';
import { UpdateCompany, GetCompany, UpdateCompanyLogo } from '../state/companies.action';

@Injectable({
  providedIn: "root"
})
export class CompanyService {

  config: any;
  percent: any;
  sasToken: any;
  storageAccount: any;
  containerName: any;

  constructor(private store: Store, private blob: BlobService) { }

  upload(file: File, containerName: string, sasToken: string, storageAccount: string) {
    const Config: UploadParams = {
      sas: sasToken,
      storageAccount: storageAccount,
      containerName: containerName
    };
    if (file) {
      const blobName = `logos/${file.name}`;
      const baseUrl = this.blob.generateBlobUrl(Config, blobName);
      this.config = {
        baseUrl: baseUrl,
        sasToken: sasToken,
        blockSize: 1024 * 64,
        file: file,
        complete: () => {
          this.store.dispatch(new UpdateCompanyLogo(baseUrl));
        },
        error: (err: { statusText: any; }) => {
          this.store.dispatch(new ShowErrorMessage(err.statusText));
        },
        progress: (percent: any) => {
          this.percent = percent;
        }
      };
      this.blob.upload(this.config);
    }
  }
}