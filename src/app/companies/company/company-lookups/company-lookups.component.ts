import { Component, OnInit, EventEmitter, Output, OnDestroy, ViewChild } from '@angular/core';
import { GridColumn, GridColumnButton } from 'src/app/core/models/entity/grid.column';
import { SubSink } from 'subsink/dist/subsink';
import { Select, Store } from '@ngxs/store';
import { CompaniesState } from '../../state/companies.state';
import { Observable } from 'rxjs/internal/Observable';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { GetLookups, GetLookupTypes, UpdateLookup, CreateLookup, DeleteLookup } from '../../state/companies.action';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChangeEventArgs } from '@syncfusion/ej2-dropdowns/src/drop-down-list';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';

@Component({
  selector: 'app-company-lookups',
  templateUrl: './company-lookups.component.html',
  styleUrls: ['./company-lookups.component.css']
})
export class CompanyLookupsComponent extends BaseComponent implements OnInit, OnDestroy {

  @Select(CompaniesState.getLookupTypes) lookupTypes$: Observable<string[]>;
  @Select(CompaniesState.getLookups) lookups$: Observable<Lookup[]>;
  @Select(CompaniesState.getCurrentCompanyId) currentCompanyId$: Observable<number>;

  @ViewChild('lookupModal', {static: false}) lookupModal: DialogComponent;
  @ViewChild('confirmModal', {static: false}) confirmModal: ModalComponent;

  private subs = new SubSink();
  actions: GridColumnButton[] = [
    { type: 'button', text: 'Delete', visible: true, action: this.showDeleteModal.bind(this) }
  ];
  columns: GridColumn[] = [
    { headerText: 'Description', field: 'description', ignoreFiltering: true, width: 200 },
    { headerText: 'Value', field: 'value', filterType: 'checkbox', width: 200, },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 120 }
  ];
  lookupForm: FormGroup;
  lookups: Lookup[];
  filteredLookups: Lookup[];
  selectedLookupType: string;
  lookupTypes: string[];
  canManageLookups: boolean;
  deleteMessage: string;
  currentLookup: Lookup;
  isEdit: boolean;
  currentCompanyId: number;
  lookup: Lookup;

  constructor(protected store: Store, private fb: FormBuilder) {
    super(store);
    this.lookupForm = this.fb.group({
      description: ['', [Validators.required]],
      value: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.subs.add(
      this.currentCompanyId$
        .subscribe(companyId => {
          if (companyId) {
            this.currentCompanyId = companyId;
            // this.canManageLookups = true;
            this.store.dispatch(new GetLookups(companyId));
            this.store.dispatch(new GetLookupTypes(companyId));
          }
        }),
      this.lookupTypes$
        .subscribe(types => {
          this.lookupTypes = types;
          console.log('lookups', this.lookupTypes)
          if (this.lookupTypes) {
            this.selectedLookupType = this.lookupTypes[0];
            this.getLookupsByType(this.selectedLookupType);
          }
        }),
      this.lookups$
        .subscribe(lookups => {
          this.lookups = lookups;
          this.getLookupsByType(this.selectedLookupType);
        })
    );
  }

  ngOnDestroy() {

  }

  addLookup() {
    this.lookupForm.reset();
    this.isEdit = false;
    this.currentLookup = null;
    this.lookupModal.show();
  }

  onLookupTypeChanged(args: ChangeEventArgs) {
    if (!args) return;
    const { value } = args;
    if (value) {
      this.selectedLookupType = value.toString();
      this.getLookupsByType(this.selectedLookupType);
    }
  }

  showDeleteModal(args: Lookup) {
    this.lookup = args;
    this.selectedLookupType = args.type;
    this.deleteMessage = `Are you sure you want to delete "${args.description}"?`;
    this.confirmModal.show();
  }

  showLookupModal(lookup: Lookup) {
    this.isEdit = true;
    this.lookupForm.reset();
    if (lookup) {
      this.currentLookup = lookup;
      this.lookupForm.setValue({
       description: lookup.description,
        value: lookup.value,
      });
      this.lookupModal.show();
    }
  }

  saveLookup() {
    if (this.lookupForm.valid) {
      const type = {type: this.selectedLookupType};
      const lookup: Lookup = { ...type, ...this.lookupForm.value };
      if (this.isEdit) {
        this.store.dispatch(new UpdateLookup(this.currentCompanyId, lookup)).toPromise()
          .then(() => {
            this.lookupForm.reset();
          });
      } else {
        this.store.dispatch(new CreateLookup(this.currentCompanyId, lookup)).toPromise()
          .then(() => {
            this.lookupForm.reset();
          });
          console.log(this.currentCompanyId, 'companyId')
      }
      this.lookupModal.hide();
    }
  }

  removeLookup() {
    console.log('DELETED LOOKUP', this.lookup);
    this.store.dispatch(new DeleteLookup(this.currentCompanyId, this.lookup));
  }

  closeDialog() {
    this.lookupModal.hide();
    this.lookupForm.reset();
  }

  private getLookupsByType(type: string) {
    if (this.lookups) {
      this.filteredLookups = this.lookups.filter(x => x.type === type);
    }
  }
}
