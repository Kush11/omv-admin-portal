import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyLookupsComponent } from './company-lookups.component';

describe('CompanyLookupsComponent', () => {
  let component: CompanyLookupsComponent;
  let fixture: ComponentFixture<CompanyLookupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyLookupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyLookupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
