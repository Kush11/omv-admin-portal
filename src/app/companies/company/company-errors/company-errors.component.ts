import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ErrorMessage } from 'src/app/core/models/entity/error-message';
import { Select, Store } from '@ngxs/store';
import { CompaniesState } from '../../state/companies.state';
import { GridColumn } from 'src/app/core/models/entity/grid.column';
import { GetErrorMessages } from '../../state/companies.action';

@Component({
  selector: 'app-company-errors',
  templateUrl: './company-errors.component.html',
  styleUrls: ['./company-errors.component.css']
})
export class CompanyErrorsComponent implements OnInit {

  @Select(CompaniesState.getErrorMessages) errorMessages$: Observable<ErrorMessage[]>;

  columns: GridColumn[] = [
    { headerText: 'Timestamp', field: 'timestamp', width: 200 },
    { headerText: 'Message', field: 'message', width: 200 },
  ];
  
  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new GetErrorMessages());
  }

}
