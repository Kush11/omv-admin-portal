import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyErrorsComponent } from './company-errors.component';

describe('CompanyErrorsComponent', () => {
  let component: CompanyErrorsComponent;
  let fixture: ComponentFixture<CompanyErrorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyErrorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyErrorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
