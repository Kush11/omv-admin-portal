import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyProfilesListViewComponent } from './company-profiles-list-view.component';

describe('CompanyProfilesListViewComponent', () => {
  let component: CompanyProfilesListViewComponent;
  let fixture: ComponentFixture<CompanyProfilesListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyProfilesListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyProfilesListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
