import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Company } from 'src/app/core/models/entity/company';
import { Observable } from 'rxjs/internal/Observable';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { GridColumnButton, GridColumn } from 'src/app/core/models/entity/grid.column';
import { ShowWarningMessage } from 'src/app/state/app.actions';
import { ConfigurationSetting } from 'src/app/core/models/entity/configuration';
import { CompaniesState } from '../../state/companies.state';
import { GetCompanies, CreateCompany, DeleteCompany, UpdateCompany } from '../../state/companies.action';

@Component({
  selector: 'app-company-profiles-list-view',
  templateUrl: './company-profiles-list-view.component.html',
  styleUrls: ['./company-profiles-list-view.component.css']
})
export class CompanyProfilesListViewComponent implements OnInit {

  @Select(CompaniesState.getCompanies) companies$: Observable<Company[]>;
  @Select(CompaniesState.getTotalCompanies) totalCompanies$: Observable<number>;
  @Select(CompaniesState.getConfigurationSettings) customerConfiguration$: Observable<ConfigurationSetting[]>;


  @Output() edit = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  @Output() create = new EventEmitter<any>();
  @ViewChild('fieldDialog', {static: false}) companyModal: DialogComponent;
  @ViewChild('confirmModal' , {static: false}) confirmModal: ModalComponent;
  @ViewChild('file' , {static: false}) file: any;
  modalPosition: Object = { X: 'center', Y: 'center' };

  companyForm: FormGroup;
  companyId: number;
  company: Company;
  deleteMessage: string;
  selectedCustomerFieldId: number;
  pointOfContract = [];
  currentCompany: Company;
  isEdit: boolean;
  customerId: any;
  items: any;
  containerName: string;
  storageAccount: string;
  sasToken: string;
  view: any;
  actions: GridColumnButton[] = [
    { type: 'icon', icon: 'edit', width: 18, visible: true, action: this.onEdit.bind(this) },
    { type: 'icon', icon: 'delete', width: 16, visible: true, action: this.onDelete.bind(this) },
    { type: 'icon', icon: 'impersonate', width: 20, visible: true, action: this.onImpersonate.bind(this) }
  ];
  columns: GridColumn[] = [
    { headerText: 'Name', field: 'name', width: 110, useAsLink: true },
    { headerText: 'Type', field: 'companyType', ignoreFiltering: true, width: 70 },
    { headerText: 'Contract Type', field: 'contractType', ignoreFiltering: true, width: 100 },
    { headerText: 'Point Of Contact', field: 'pointOfContact', ignoreFiltering: true, width: 120 },
    { headerText: 'Actions', type: 'buttons', buttons: this.actions, width: 70 }
  ];
  get pointsOfContact() {
    return this.companyForm.get('pointsOfContact') as FormArray;
  }
  get hostHeader() {
    return this.companyForm.get('hostHeader');
  }

  constructor(private store: Store, private router: Router, private fb: FormBuilder) {
    this.companyForm = this.fb.group({
      name: ['', [Validators.required]],
      companyType: ['', [Validators.required]],
      contractType: ['', [Validators.required]],
      hostHeader: ['', [Validators.required]],
      pointsOfContact: this.fb.array([])
    });
  }

  ngOnInit() {
    this.store.dispatch(new GetCompanies());
  }

  ngOnDestroy() {

  }

  onView(company: Company) {
    this.router.navigate(['companies', company.id]);
  }

  saveChanges() {
    if (this.companyForm.valid) {
      this.companyForm.reset(this.companyForm.value);
      const company: Company = { ...this.currentCompany, ...this.companyForm.value };
      if (this.isEdit) {
        this.store.dispatch(new UpdateCompany(company.id, company)).toPromise()
          .then(() => {
            this.store.dispatch(new GetCompanies());
          });
      } else {
        this.store.dispatch(new CreateCompany(company)).toPromise()
          .then(() => {
            this.store.dispatch(new GetCompanies());
          });
      }
      this.closeDialog();
    }
  }

  removeCompany() {
    this.store.dispatch(new DeleteCompany(this.currentCompany)).toPromise()
      .then(() => {
        this.store.dispatch(new GetCompanies());
      });
  }

  onCreate() {
    this.isEdit = false;
    this.addPointOfContact();
    this.currentCompany = null;
    this.companyModal.show();
  }

  onEdit(company: Company) {
    this.isEdit = true;
    if (company) {
      this.currentCompany = company;
      this.companyForm.patchValue({
        name: company.name,
        companyType: company.companyType,
        contractType: company.contractType,
        hostHeader: company.hostHeader
      });
      this.setPointsOfContact();
    }
    this.companyModal.show();
  }

  private setPointsOfContact() {
    this.currentCompany.pointsOfContact.map(p => {
      const control = new FormControl(p, [Validators.required, Validators.email]);
      const pointsGroup = this.companyForm as FormGroup;
      (pointsGroup.controls.pointsOfContact as FormArray).push(control);
    });
  }

  onDelete(company: Company) {
    this.currentCompany = company;
    this.deleteMessage = `Are you sure you want to deactivate "${company.name}"?`;
    this.confirmModal.show();
  }

  onImpersonate(company: Company) {
    const { hostHeader } = company;
    if (!hostHeader) {
      return this.store.dispatch(new ShowWarningMessage("Host Header has not been set."));
    }
    if (this.isUrlValid(hostHeader)) {
      window.open(hostHeader, "_blank");
    } else {
      window.open(`https://${hostHeader}`, "_blank");
    }
  }

  closeDialog() {
    this.companyForm.reset();
    this.pointsOfContact.controls.splice(0);
    this.companyModal.hide();
  }

  addPointOfContact() {
    this.pointsOfContact.push(this.fb.control('', [Validators.required, Validators.email]));
  }

  removePointOfContact(index: number) {
    this.companyForm.get('pointsOfContact').markAsDirty();
    this.pointsOfContact.removeAt(index);
  }

  private isUrlValid(url: string): boolean {
    return /^(http|https|ftp):\/\/.*$/.test(url);
  }
}
