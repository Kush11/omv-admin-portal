import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { ViewType } from 'src/app/core/enum/view-type';
import { SubSink } from 'subsink/dist/subsink';

@Component({
  selector: 'app-company-profiles',
  templateUrl: './company-profiles.component.html',
  styleUrls: ['./company-profiles.component.css']
})
export class CompanyProfilesComponent extends BaseComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  viewType: string;

  constructor(protected store: Store, private route: ActivatedRoute) {
    super(store);
    this.setPageTitle('Company Profiles');
  }

  ngOnInit() {
    this.subs.sink =
      this.route.queryParams
        .subscribe(params => {
          this.viewType = params.view ? params.view : ViewType.TILE;
        });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
