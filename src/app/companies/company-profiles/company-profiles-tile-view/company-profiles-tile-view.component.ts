import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Company } from 'src/app/core/models/entity/company';
import { Observable } from 'rxjs/internal/Observable';
import { SubSink } from 'subsink/dist/subsink';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { PagerComponent } from '@syncfusion/ej2-angular-grids';
import { CompaniesService } from '../../../core/services/business/companies.service';
import { ConfigurationSetting } from 'src/app/core/models/entity/configuration';
import { ConfigurationSettingKey } from 'src/app/core/enum/configuration-setting-key';
import { ShowSpinner, ShowWarningMessage } from 'src/app/state/app.actions';
import { CompaniesState } from '../../state/companies.state';
import { GetCompanies, SetCurrentCompanyId, CreateCompany, ClearConfigurationSettings, DeleteCompany, UpdateCompany, GetConfigurationSettings } from '../../state/companies.action';

@Component({
  selector: 'app-company-profiles-tile-view',
  templateUrl: './company-profiles-tile-view.component.html',
  styleUrls: ['./company-profiles-tile-view.component.css']
})
export class CompanyProfilesTileViewComponent implements OnInit {

  @Select(CompaniesState.getCompanies) companies$: Observable<Company[]>;
  @Select(CompaniesState.getTotalCompanies) totalCompanies$: Observable<number>;
  @Select(CompaniesState.getConfigurationSettings) customerConfiguration$: Observable<ConfigurationSetting[]>;

  @ViewChild('fieldDialog', {static: false}) companyModal: DialogComponent;
  @ViewChild('confirmModal', {static: false}) confirmModal: ModalComponent;
  @ViewChild('pager', {static: false}) pager: PagerComponent;
  @ViewChild('file', {static: false}) file: any;

  private subs = new SubSink();
  width = 700;
  modalPosition: Object = { X: 'center', Y: 100 };
  pageSize = 8;
  currentPage = 1;
  currentPagination: number;
  companies: Company[];
  allCompanies: Company[];
  company: Company;
  companyId: number;
  currentCompany: Company;
  isEdit: boolean;
  companyForm: FormGroup;
  deleteMessage: string;
  isDelete: boolean;
  temporaryCompanyForm: FormGroup;
  items: FormArray;
  customerId: any;
  totalCompanies: number;
  view: any;

  get pointsOfContact() {
    return this.companyForm.get('pointsOfContact') as FormArray;
  }
  get hostHeader() {
    return this.companyForm.get('hostHeader');
  }

  constructor(protected store: Store, private router: Router, private fb: FormBuilder, private route: ActivatedRoute) {
    this.companyForm = this.fb.group({
      name: ['', [Validators.required]],
      companyType: ['', [Validators.required]],
      contractType: ['', [Validators.required]],
      hostHeader: ['', [Validators.required]],
      pointsOfContact: this.fb.array([])
    });
  }

  ngOnInit() {
    this.store.dispatch(new GetCompanies());
    this.subs.add(
      this.companies$
        .subscribe(companies => {
          if (typeof (this.currentPagination) == 'undefined') {
            this.allCompanies = companies.slice(0, this.pageSize);
          } else {
            let start = (this.currentPagination - 1) * this.pageSize;
            this.allCompanies = companies.slice(start, start + this.pageSize);
            if (this.isDelete) {
              this.allCompanies = this.allCompanies.filter(x => x.id !== this.customerId);
              this.isDelete = false;
            }
          }
          this.companies = companies;
        }),
      this.totalCompanies$
        .subscribe(total => {
          this.totalCompanies = total;
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  addPointOfContact() {
    this.pointsOfContact.push(this.fb.control('', [Validators.required, Validators.email]));
  }

  removePointOfContact(index: number) {
    this.companyForm.get('pointsOfContact').markAsDirty();
    this.pointsOfContact.removeAt(index);
  }

  onView(company: Company) {
    const { id } = company;
    this.store.dispatch(new SetCurrentCompanyId(id));
    this.router.navigate(['companies', id]);
  }

  saveChanges() {
    if (this.companyForm.valid) {
      this.companyForm.reset(this.companyForm.value);
      const company: Company = { ...this.currentCompany, ...this.companyForm.value };
      if (this.isEdit) {
        this.store.dispatch(new UpdateCompany(company.id, company)).toPromise()
          .then(() => {
            this.store.dispatch(new GetCompanies());
          });
      } else {
        this.store.dispatch(new CreateCompany(company)).toPromise()
          .then(() => {
            this.store.dispatch(new GetCompanies());
          });
      }
      this.closeDialog();
    }
  }

  onCreate() {
    this.isEdit = false;
    this.addPointOfContact();
    this.currentCompany = null;
    this.companyModal.show();
  }

  onEdit(company: Company) {
    this.isEdit = true;
    if (company) {
      this.currentCompany = company;
      this.companyForm.patchValue({
        name: company.name,
        companyType: company.companyType,
        contractType: company.contractType,
        hostHeader: company.hostHeader
      });
      this.setPointsOfContact();
    }
    this.companyModal.show();
  }

  removeCompany() {
    this.isDelete = true;
    this.store.dispatch(new DeleteCompany(this.currentCompany)).toPromise()
      .then(() => {
        this.store.dispatch(new GetCompanies());
      });
  }

  onDelete(customer: Company) {
    this.currentCompany = customer;
    this.customerId = customer.id;
    this.deleteMessage = `Are you sure you want to deactivate "${customer.name}"?`;
    this.confirmModal.show();
  }

  onImpersonate(customer: Company) {
    if (!customer.hostHeader) {
      return this.store.dispatch(new ShowWarningMessage("Host Header has not been set."));
    }
    if (this.isUrlValid(customer.hostHeader)) {
      window.open(customer.hostHeader, "_blank");
    } else {
      window.open(`https://${customer.hostHeader}`, "_blank");
    }
  }

  closeDialog() {
    this.companyForm.reset();
    this.pointsOfContact.controls.splice(0);
    this.companyModal.hide();
  }

  paginationEvent(args: any) {
    if (args.currentPage) {
      this.currentPagination = args.currentPage;
      let start = (args.currentPage - 1) * this.pageSize;
      this.allCompanies = this.companies.slice(start, start + this.pageSize);
    }
    window.scrollTo(0, 0);
  }

  private setPointsOfContact() {
    this.currentCompany.pointsOfContact.map(p => {
      const control = new FormControl(p, [Validators.required, Validators.email]);
      const pointsGroup = this.companyForm as FormGroup;
      (pointsGroup.controls.pointsOfContact as FormArray).push(control);
    });
  }

  private isUrlValid(url: string): boolean {
    return /^(http|https|ftp):\/\/.*$/.test(url);
  }
}
