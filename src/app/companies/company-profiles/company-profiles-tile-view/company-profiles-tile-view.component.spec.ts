import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyProfilesTileViewComponent } from './company-profiles-tile-view.component';

describe('CompanyProfilesTileViewComponent', () => {
  let component: CompanyProfilesTileViewComponent;
  let fixture: ComponentFixture<CompanyProfilesTileViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyProfilesTileViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyProfilesTileViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
