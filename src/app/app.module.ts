import { NotificationsWebDataService } from './core/services/data/notifications/notifications.web.data.service';
import { NotificationsMockDataService } from './core/services/data/notifications/notifications.mock.data.service';
import { BrowserModule } from '@angular/platform-browser';
import { AppState } from './state/app.state';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { environment } from 'src/environments/environment';
import { NgxsModule } from '@ngxs/store';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CustomersDataService } from './core/services/data/customers/customers.data.service';
import { UsersDataService } from './core/services/data/users/users.data.service';
import { OktaDataService } from './core/services/data/okta/okta.data.service';
import { CustomersMockDataService } from './core/services/data/customers/customers.mock.data.service';
import { UsersMockDataService } from './core/services/data/users/users.mock.data.service';
import { OktaWebDataService } from './core/services/data/okta/okta.web.data.service';
import { UsersWebDataService } from './core/services/data/users/users.web.data.service';
import { AppHeaderComponent } from './shared/app-header/app-header.component';
import { AuthorizationCheckComponent } from './authorization-check/authorization-check.component';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { SideMenuComponent } from './shared/side-menu/side-menu.component';
import { UnAuthorizedComponent } from './unauthorized/unauthorized.component';
import { SharedModule } from './shared/shared.module';
import { NotificationsDataService } from './core/services/data/notifications/notifications.data.service';
import { NotificationsState } from './notifications/state/notifications.state';
import { GridModule, PagerModule } from '@syncfusion/ej2-angular-grids';
import { NotificationDetailComponent } from './notifications/notification-details/notification-detail.component';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { OktaMockDataService } from './core/services/data/okta/okta.mock.data.service';
import { ProfileCardComponent } from './shared/profile-card/profile-card.component';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { ListViewModule } from '@syncfusion/ej2-angular-lists';
import { HttpInterceptorService } from './core/services/httpinterceptor.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlobModule } from 'angular-azure-blob-service';
import { LookupDataService } from './core/services/data/lookup/lookup.data.service';
import { LookupMockDataService } from './core/services/data/lookup/lookup.mock.data.service';
import { LookupWebDataService } from './core/services/data/lookup/lookup.web.data.service';
import { CustomersWebDataService } from './core/services/data/customers/customers.web.data.service';
import { CompanyProfilesComponent } from './companies/company-profiles/company-profiles.component';
import { CompanyComponent } from './companies/company/company.component';
import { CompanyProfilesTileViewComponent } from './companies/company-profiles/company-profiles-tile-view/company-profiles-tile-view.component';
import { CompanyProfilesListViewComponent } from './companies/company-profiles/company-profiles-list-view/company-profiles-list-view.component';
import { CompanyConfigurationsComponent } from './companies/company/company-configurations/company-configurations.component';
import { CompanyLiveStreamComponent } from './companies/company/company-live-stream/company-live-stream.component';
import { CompaniesState } from './companies/state/companies.state';
import { CompanyPermissionsComponent } from './companies/company/company-configurations/company-permissions/company-permissions.component';
import { NotificationsListComponent } from './notifications/notifications-list/notifications-list.component';
import { NotificationsNewComponent } from './notifications/notifications-list/notifications-new/notifications-new.component';
import { NotificationsHistoryComponent } from './notifications/notifications-list/notifications-history/notifications-history.component';
import { CompanySecurityProvidersComponent } from './companies/company/company-security-providers/company-security-providers.component';
import { CompanyLookupsComponent } from './companies/company/company-lookups/company-lookups.component';
import { CompanyErrorsComponent } from './companies/company/company-errors/company-errors.component';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { LoginComponent } from './login/login.component';
import { DirectoryWebDataService } from './core/services/data/directory/directory.web.data.service';
import { DirectoryMockDataService } from './core/services/data/directory/directory.mock.data.service';
import { DirectoryDataService } from './core/services/data/directory/directory.data.service';
import { TreeGridModule } from '@syncfusion/ej2-angular-treegrid';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AuthorizationCheckComponent,
    ProfileCardComponent,
    AuthCallbackComponent,
    LoginComponent,
    SideMenuComponent,
    UnAuthorizedComponent,
    NotificationDetailComponent,
    CompanyProfilesComponent,
    CompanyComponent,
    CompanyProfilesTileViewComponent,
    CompanyProfilesListViewComponent,
    CompanyConfigurationsComponent,
    CompanyLiveStreamComponent,
    CompanyPermissionsComponent,
    CompanySecurityProvidersComponent,
    NotificationsListComponent,
    NotificationsNewComponent,
    NotificationsHistoryComponent,
    CompanyLookupsComponent,
    CompanyErrorsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    GridModule,
    BlobModule.forRoot(),
    NgxsModule.forRoot([AppState, CompaniesState, NotificationsState], { developmentMode: !environment.production }),
    NgxsStoragePluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot(),
    CheckBoxModule,
    DropDownListModule,
    ListViewModule,
    FormsModule,
    ReactiveFormsModule,
    PagerModule,
    DialogModule,
    ToastModule,
    TreeGridModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
    { provide: CustomersDataService, useClass: environment.useMocks ? CustomersMockDataService : CustomersWebDataService },
    { provide: NotificationsDataService, useClass: environment.useMocks ? NotificationsMockDataService : NotificationsWebDataService },
    { provide: UsersDataService, useClass: environment.useMocks ? UsersMockDataService : UsersWebDataService },
    { provide: OktaDataService, useClass: environment.useMocks ? OktaMockDataService : OktaWebDataService },
    { provide: LookupDataService, useClass: environment.useMocks ? LookupMockDataService : LookupWebDataService },
    { provide: DirectoryDataService, useClass: environment.useMocks ? DirectoryMockDataService : DirectoryWebDataService }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule { }
