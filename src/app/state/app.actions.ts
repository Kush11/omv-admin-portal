//#region Window

export class ShowLeftNav {
  static readonly type = '[App] ShowLeftNav';

  constructor(public payload: boolean) { }
}

export class SetPageTitle {
  static readonly type = '[App] SetPageTitle';

  constructor(public title: string) { }
}

export class SetPreviousRoute {
  static readonly type = '[App] SetPreviousRoute';

  constructor(public route: string) { }
}

//#endregion

//#region Spinner

export class ShowSpinner {
  static readonly type = '[App] ShowSpinner';
}

export class HideSpinner {
  static readonly type = '[App] HideSpinner';
}

//#endregion

//#region Toast Message

export class ShowSuccessMessage {
  static readonly type = '[App] ShowSuccessMessage';

  constructor(public message: string) { }
}

export class ShowWarningMessage {
  static readonly type = '[App] ShowWarningMessage';

  constructor(public message: string) { }
}

export class ShowErrorMessage {
  static readonly type = '[App] ShowErrorMessage';

  constructor(public error: any) { }
}

export class GetUserPermissions {
  static readonly type = '[App] GetUserPermissions';

  constructor(public userId: number) { }
}

//#endregion

//#region Authentication

export class GetLoggedInUser {
  static readonly type = '[App] GetLoggedInUser';
}

export class LogOut {
  static readonly type = '[App] LogOut';
}

//#endregion
