import { Permission } from './../core/enum/permission';
import { State, Selector, Action, StateContext } from '@ngxs/store';
import {
  GetLoggedInUser, SetPageTitle, ShowSpinner, HideSpinner, ShowSuccessMessage, ShowWarningMessage,
  LogOut, ShowErrorMessage, ShowLeftNav, SetPreviousRoute,
} from './app.actions';
import { User } from '../core/models/entity/user';
import { AuthService } from '../core/services/business/auth.service';
import { UsersDataService } from '../core/services/data/users/users.data.service';
import { Toast, ToastType } from '../core/enum/toast';
import { tap } from 'rxjs/internal/operators/tap';
import { CustomersDataService } from '../core/services/data/customers/customers.data.service';

export class AppStateModel {
  currentUser: User;
  isAuthorized: boolean;
  pageTitle: string;
  toastMessage: Toast;
  showSpinner: boolean;
  showLeftNav: boolean;
  permissions: Permission[];
  currentUserId: number;
  previousRoute: string;
  azureSASToken: string;
  azureContainer: string;
  azureStorageAccount: string;
}

@State<AppStateModel>({
  name: 'app',
  defaults: {
    currentUser: null,
    isAuthorized: false,
    pageTitle: 'OMV Admin Portal',
    toastMessage: null,
    showSpinner: false,
    showLeftNav: false,
    permissions: [],
    currentUserId: 1,
    previousRoute: '',
    azureSASToken: '',
    azureContainer: '',
    azureStorageAccount: ''
  }
})
export class AppState {

  //#region S E L E C T O R S

  //#region Window

  @Selector()
  static getPreviousRoute(state: AppStateModel) {
    return state.previousRoute;
  }

  @Selector()
  static getPageTitle(state: AppStateModel) {
    return state.pageTitle;
  }

  //#endregion

  //#region Authentication

  @Selector()
  static getLoggedInUser(state: AppStateModel) {
    return state.currentUser;
  }

  @Selector()
  static getCurrentUserId(state: AppStateModel) {
    return state.currentUserId;
  }

  @Selector()
  static getLeftNavVisibility(state: AppStateModel) {
    return state.showLeftNav;
  }

  @Selector()
  static getUserPermissions(state: AppStateModel) {
    return state.permissions;
  }

  @Selector()
  static getIsAuthorized(state: AppStateModel) {
    return state.isAuthorized;
  }

  //#endregion

  //#region Spinner

  @Selector()
  static getSpinnerVisibility(state: AppStateModel) {
    return state.showSpinner;
  }

  //#endregion

  //#region Taost

  @Selector()
  static getToastMessage(state: AppStateModel) {
    return state.toastMessage;
  }

  //#endregion

  //#region BlobConfig

  @Selector()
  static getAzureContainer(state: AppStateModel) {
    return state.azureContainer;
  }

  @Selector()
  static getAzureStorageAccount(state: AppStateModel) {
    return state.azureStorageAccount;
  }

  @Selector()
  static getAzureSASToken(state: AppStateModel) {
    return state.azureSASToken;
  }

  //#endregion

  //#endregion S E L E C T O R S

  constructor(private auth: AuthService, private usersDataService: UsersDataService, private customersDataService: CustomersDataService) { }

  //#region ACTIONS

  //#region Window

  @Action(SetPreviousRoute)
  setPreviousRoute(ctx: StateContext<AppStateModel>, { route }: SetPreviousRoute) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      previousRoute: route
    });
  }

  @Action(SetPageTitle)
  setPageTitle({ getState, setState }: StateContext<AppStateModel>, { title }: SetPageTitle) {
    const state = getState();
    setState({
      ...state,
      pageTitle: title
    });
  }

  //#endregion

  //#region Authentication

  @Action(GetLoggedInUser)
  getLoggedinUser(ctx: StateContext<AppStateModel>) {
    return this.usersDataService.getLoggedInUser()
      .pipe(
        tap(user => {

          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentUser: user,
            isAuthorized: true
          });
        }, err => {
          console.log('App State getLoggedinUser', err);
          const state = ctx.getState();
          ctx.setState({
            ...state,
            currentUser: null,
            isAuthorized: false
          });
          if (err.status === 404) {
            this.auth.logout();
          }
        })
      );
  }

  @Action(LogOut)
  async logOut({ getState, setState }: StateContext<AppStateModel>) {
    await this.auth.logout().then(() => {
      const state = getState();
      setState({
        ...state,
        currentUser: null,
        // isUserAuthenticated: false
      });
    });
  }


  //#endregion

  //#region Spinner

  @Action(ShowSpinner)
  showSpinner({ getState, setState }: StateContext<AppStateModel>) {
    const state = getState();
    setState({
      ...state,
      showSpinner: true
    });
  }

  @Action(HideSpinner)
  hideSpinner({ getState, setState }: StateContext<AppStateModel>) {
    const state = getState();
    setState({
      ...state,
      showSpinner: false
    });
  }

  //#endregion

  //#region Toaster

  @Action(ShowSuccessMessage)
  showSuccessMessage(ctx: StateContext<AppStateModel>, { message }: ShowSuccessMessage) {
    const state = ctx.getState();
    const toast: Toast = { message: message, type: ToastType.Success };
    ctx.setState({
      ...state,
      toastMessage: toast
    });
    ctx.setState({ ...state, toastMessage: null });
  }

  @Action(ShowWarningMessage)
  showWarningMessage(ctx: StateContext<AppStateModel>, { message }: ShowWarningMessage) {
    const state = ctx.getState();
    const toast: Toast = { message: message, type: ToastType.Warning };
    ctx.setState({
      ...state,
      toastMessage: toast
    });
    ctx.setState({ ...state, toastMessage: null });
  }

  @Action(ShowErrorMessage)
  showErrorMessage(ctx: StateContext<AppStateModel>, { error }: ShowErrorMessage) {
    ctx.dispatch(new HideSpinner());
    const state = ctx.getState();
    if (!error) return;
    const message = error.message ? error.message : error;
    const toast: Toast = { message: message, type: ToastType.Error };
    ctx.setState({
      ...state,
      toastMessage: toast
    });
    ctx.setState({ ...state, toastMessage: null });
  }

  @Action(ShowLeftNav)
  setLeftNavToggle({ getState, setState }: StateContext<AppStateModel>, { payload }: ShowLeftNav) {
    const state = getState();
    setState({
      ...state,
      showLeftNav: payload
    });
  }

  //#endregion

  //#endregion
}
