export enum ToastType {
  Success = 0,
  Error = 1,
  Info = 2,
  Warning = 3
}

export enum ToastAction {
  close = 0
}

export class Toast {
  message?: string;
  type?: ToastType;
  action?: ToastAction;
}