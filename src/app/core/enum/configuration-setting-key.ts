export enum ConfigurationSettingKey {
    SasToken = 'SASToken',
    StorageAccount = 'StorageAccount',
    MediaContainer = 'MediaContainer'
}
