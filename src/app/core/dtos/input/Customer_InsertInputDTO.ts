export class Customer_InsertInputDTO {
    name: string;
    companyTitle: string;
    companyType: string;
    contractType: string;
    pointOfContact: string[];
    hostHeader: string;
    imageUrl: string;
    status: number;
    createdOn: Date;
    createdBy: string;
    modifiedOn: Date;
    modifiedBy: string;
    hostheader: string;
}
