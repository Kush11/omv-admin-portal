export class Lookup_DeleteInputDTO {
    lookupType: string;
    lookupValue: string;
    lookupDescription: string;
}