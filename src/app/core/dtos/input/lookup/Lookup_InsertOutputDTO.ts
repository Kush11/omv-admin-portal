export class Lookup_InsertOutputDTO {
  lookupType: string;
  lookupValue: string;
  lookupDescription: string;
}