export class Camera_InsertInputDTO {
    name: string;
    livestreamURL: string;
    asset: number;
    group: number;
}
