
export class Camera_UpdateInputDTO
    {
        LivestreamURL: string;
        Name: string;
        ThumbnailUrl: string;
        Asset: number | null;
        Group: number | null;
    }