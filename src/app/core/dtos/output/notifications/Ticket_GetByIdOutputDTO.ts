import { Document_GetAllOutputDTO } from './Document_GetAllOutputDTO';
import { BaseDTO } from './../../BaseDTO';

export class Ticket_GetByIdOutputDTO extends BaseDTO{
    subject: string;
    description: string;
    isFollowUp: boolean;
    companyName: string;
    email : string;
    reportedBy: string;
    status: number;
    region: string;
    createdOn: Date;
    availabilities : string[];
    startTime : Date;
    endTime: Date;
    comment: string;
    isResolved: boolean;
    dateResolved: Date;
    resolvedBy: string;
    documents: Document_GetAllOutputDTO[];

}