import { BaseDTO } from '../../BaseDTO';

export class Ticket_GetAllOutputDTO extends BaseDTO {
    TicketId: number;
    Subject: string;
    Description: string;
    IsFollowUp: boolean;
    CompanyName: string;
    ReportedBy: string;
    Status: number;
    CreatedOn: string;
    DocumentCount: number;
}
