export class Ticket_PostPayloadDTO {
    comment: string;
    ticketId: string;
    isResolved: boolean;
}
