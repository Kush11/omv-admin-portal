import { BaseDTO } from './../../BaseDTO';

export class Document_GetAllOutputDTO extends BaseDTO {
    DocumentId: string;
    StorageType: string;
    EntityType: string;
    EntityId: string;
    DocumentTypeCode: string;
    MediaType: number;
    DocumentName: string;
    DocumentUrl: string;
    Metadata: string;
    ContentType: string;
    Size: number;
    ContainerId: string;
    ThumbnailContainerUrl: string;
    IsDeleted: boolean;
    Status: number;
    CreatedOn: Date;
    CreatedBy: string;
    ModifiedOn: Date;
    ModifiedBy: string;
}
