export class CustomerSetting_InputDTO {
    settingKey: string;
    value: string;
    type: string;
}
