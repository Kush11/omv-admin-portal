import { BaseDTO } from '../../BaseDTO';

export class CustomerGetAllOutputDTO extends BaseDTO {
    name: string;
    companyTitle: string;
    companyType: string;
    contractType: string;
    pointsOfContact: [];
    imageURL: string;
    impersonate: string;
    connectionString: string;
}
