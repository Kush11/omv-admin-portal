import { BaseDTO } from '../../BaseDTO';

export class CustomerSetting_GetByCustomerIdOutputDTO extends BaseDTO  {
    customerId: number;
    settingKey: string;
    value: string;
    type: string;
    isSystem: boolean;
}
