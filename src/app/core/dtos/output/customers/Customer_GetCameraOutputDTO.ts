export class Camera_GetAllOutputDTO {
  cameraId: number;
  livestreamURL: string;
  name: string;
  thumbnailUrl: string;
  asset: number;
  assetName: string;
  group: number;
  groupName: string;
}
