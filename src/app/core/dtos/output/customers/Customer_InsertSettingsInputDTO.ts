export class Customer_GetSettingsOutputDTO {
  menuName: string;
  isPermitted: boolean;
}
