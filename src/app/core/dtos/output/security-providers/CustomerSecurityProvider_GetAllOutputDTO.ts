export class CustomerSecurityProvider_GetAllOutputDTO {
  securityProviderId: number;
  customerId: number;
  clientId: string;
  issuerUrl: string;
  authServerId: string;
  authType: number;
}