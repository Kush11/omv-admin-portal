
export class Lookup_GetByLookupTypeOutputDTO
   {
       LookupType: string;
       LookupValue: string;
       LookupDescription: string;
       LookupSort: string;
       Status: number;
   }