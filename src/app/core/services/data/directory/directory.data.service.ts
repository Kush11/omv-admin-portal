import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Directory } from 'src/app/core/models/entity/directory';

@Injectable({
  providedIn: 'root'
})
export abstract class DirectoryDataService {

  constructor() { }

  abstract getDirectories(parentId?: number): Observable<Directory[]>;
}