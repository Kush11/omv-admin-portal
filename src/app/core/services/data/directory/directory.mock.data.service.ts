import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Directory } from 'src/app/core/models/entity/directory';
import { DirectoryDataService } from './directory.data.service';

@Injectable({
  providedIn: 'root'
})
export class DirectoryMockDataService implements DirectoryDataService {

  constructor() { }
  
  getDirectories(parentId?: number): Observable<Directory[]> {
    throw new Error("Method not implemented.");
  }

}