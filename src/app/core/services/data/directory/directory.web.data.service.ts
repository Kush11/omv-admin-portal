import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Directory } from 'src/app/core/models/entity/directory';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Directory_GetAllOutputDTO } from 'src/app/core/dtos/output/directories/Directory_GetAllOutputDTO';
import { map } from 'rxjs/internal/operators/map';
import * as automapper from 'automapper-ts';
import { DirectoryDataService } from './directory.data.service';

@Injectable({
  providedIn: 'root'
})
export class DirectoryWebDataService implements DirectoryDataService {

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient) { }
  
  getDirectories(parentId?: number): Observable<Directory[]> {
    let requestUri = this.baseUrl + `/v1/directories`;

    const options = {
      params: new HttpParams()
    };
    if (parentId) options.params = options.params.set('parentId', parentId.toString());

    return this.httpClient.get<Directory_GetAllOutputDTO[]>(requestUri, options)
      .pipe(
        map(response => {
          automapper
            .createMap(Directory_GetAllOutputDTO, Directory)
            .forMember('id', function (opts) { opts.mapFrom('directoryId'); })
            .forMember('name', function (opts) { opts.mapFrom('directoryName'); })
            .forMember('parentId', function (opts) { opts.mapFrom('directoryParentId'); })
            .forMember('hasChild', function (opts) { opts.mapFrom('hasChild'); });

          let folders: Directory[] = automapper.map(Directory_GetAllOutputDTO, Directory, response);
          folders.forEach(item => item.parentId = item.parentId !== 0 ? item.parentId : null);
          return folders;
        })
      );
  }

}