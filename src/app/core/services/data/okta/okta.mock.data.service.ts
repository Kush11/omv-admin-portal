import { Injectable } from '@angular/core';
import { OktaDataService } from './okta.data.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OktaMockDataService implements OktaDataService {

  constructor(private httpClient: HttpClient) { }

  logout() {
    const adminhost = window.location.host.toLowerCase();
    let oktaSecurityConfig = JSON.parse(sessionStorage.getItem(adminhost));
    if (sessionStorage.getItem(adminhost)) {
      sessionStorage.removeItem(adminhost);
    }
    const idToken = sessionStorage.getItem('id_Token');
    // tslint:disable-next-line: max-line-length
    let url = `${oktaSecurityConfig.issuerUrl}/oauth2/${oktaSecurityConfig.authServerId}/v1/logout?id_token_hint=${idToken}&post_logout_redirect_uri=${window.location.protocol}//${window.location.host.toLowerCase()}/implicit/callback`;
    window.location.href = url;
  }
}
