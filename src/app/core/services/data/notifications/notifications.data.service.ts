import { Ticket } from './../../../models/entity/notification';
import { Observable } from 'rxjs/internal/Observable';
import { Ticket_PostPayloadDTO } from 'src/app/core/dtos/output/notifications/Ticket_PostPayloadDTO';
import { HttpEvent } from '@angular/common/http';

export abstract class NotificationsDataService {

  constructor() { }

  abstract getNewNotifications(): Observable<Ticket[]>;
  abstract getNotification(id: number): Observable<Ticket>;
  abstract getNotificationsHistory(): Observable<Ticket[]>;
  abstract getAzureReadSASToken(): Observable<string>;
  abstract markIssueAsResolved(payload: Ticket_PostPayloadDTO): Observable<Ticket>;
  abstract download(url: string): Observable<HttpEvent<Blob>>;

  // abstract deleteCustomer(id: number);
  // abstract impersonateCustomer(id: number);
}

