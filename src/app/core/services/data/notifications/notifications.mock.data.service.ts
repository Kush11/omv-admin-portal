import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpHeaders, HttpEvent } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as automapper from 'automapper-ts';
import { Ticket } from 'src/app/core/models/entity/notification';
import { map } from 'rxjs/internal/operators/map';
import { NotificationsDataService } from './notifications.data.service';
import { Ticket_PostPayloadDTO } from 'src/app/core/dtos/output/notifications/Ticket_PostPayloadDTO';
import { DateService } from '../../business/dates/date.service';
import { Ticket_GetAllOutputDTO } from 'src/app/core/dtos/output/notifications/Ticket_GetAllOutputDTO';
import { Ticket_GetByIdOutputDTO } from 'src/app/core/dtos/output/notifications/Ticket_GetByIdOutputDTO';

@Injectable({
  providedIn: 'root'
})
export class NotificationsMockDataService implements NotificationsDataService {


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private httpClient: HttpClient, private dateService: DateService) { }

  getNewNotifications(): Observable<Ticket[]> {
    var requestUri = environment.api.baseUrl + '/v1/tickets?status=false';
    return this.httpClient.get<Ticket_GetAllOutputDTO[]>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(Ticket_GetAllOutputDTO, Ticket)
          .forMember('id', function (opts) { opts.mapFrom('ticketId'); })
          .forMember('companyName', function (opts) { opts.mapFrom('companyName'); })
          .forMember('reportedBy', function (opts) { opts.mapFrom('reportedBy'); })
          .forMember('subject', function (opts) { opts.mapFrom('subject'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('attachementCount', function (opts) { opts.mapFrom('documentCount'); });
        let notifications: Ticket[] = automapper.map(Ticket_GetAllOutputDTO, Ticket, response);
        return notifications;
      })
    );
  }

  getNotificationsHistory(): Observable<Ticket[]> {
    var requestUri = environment.api.baseUrl + '/v1/tickets?status=true';
    return this.httpClient.get<Ticket_GetAllOutputDTO[]>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(Ticket_GetAllOutputDTO, Ticket)
          .forMember('id', function (opts) { opts.mapFrom('ticketId'); })
          .forMember('companyName', function (opts) { opts.mapFrom('companyName'); })
          .forMember('reportedBy', function (opts) { opts.mapFrom('reportedBy'); })
          .forMember('subject', function (opts) { opts.mapFrom('subject'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('attachementCount', function (opts) { opts.mapFrom('documentCount'); });
        let notifications: Ticket[] = automapper.map(Ticket_GetAllOutputDTO, Ticket, response);
        return notifications;
      })
    );
  }

  getNotification(id: number): Observable<Ticket> {
    var requestUri = environment.api.baseUrl + `/v1/tickets/${id}`;
    return this.httpClient.get<Ticket_GetByIdOutputDTO>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(Ticket_GetByIdOutputDTO, Ticket)
          .forMember('id', function (opts) { opts.mapFrom('ticketId'); })
          .forMember('companyName', function (opts) { opts.mapFrom('companyName'); })
          .forMember('reportedBy', function (opts) { opts.mapFrom('reportedBy'); })
          .forMember('subject', function (opts) { opts.mapFrom('subject'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('region', function (opts) { opts.mapFrom('region'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('email', function (opts) { opts.mapFrom('email'); })
          .forMember('description', function (opts) { opts.mapFrom('description'); })
          .forMember('comment', function (opts) { opts.mapFrom('comment'); })
          .forMember('attachmentUrls', function (opts) { opts.mapFrom('documents'); })
          .forMember('availabilities', function (opts) { opts.mapFrom('availabilities'); })
          .forMember('isResolved', function (opts) { opts.mapFrom('isResolved'); })
          .forMember('startTime', function (opts) { opts.mapFrom('startTime'); })
          .forMember('resolvedBy', function (opts) { opts.mapFrom('resolvedBy'); })
          .forMember('dateResolved', function (opts) { opts.mapFrom('dateResolved'); })
          .forMember('endTime', function (opts) { opts.mapFrom('endTime'); });
        let notifications: Ticket = automapper.map(Ticket_GetByIdOutputDTO, Ticket, response);
        return notifications;
      })
    );
  }
  getAzureReadSASToken(): Observable<string> {
    throw new Error('Method not implemented.');
  }

  markIssueAsResolved(payload: Ticket_PostPayloadDTO): Observable<Ticket> {
    var requestUri = environment.api.baseUrl + `/v1/tickets`;
    return this.httpClient.put(requestUri, payload).pipe(
      map(response => {
        automapper
          .createMap(Ticket_GetByIdOutputDTO, Ticket)
          .forMember('id', function (opts) { opts.mapFrom('ticketId'); })
          .forMember('companyName', function (opts) { opts.mapFrom('companyName'); })
          .forMember('reportedBy', function (opts) { opts.mapFrom('reportedBy'); })
          .forMember('subject', function (opts) { opts.mapFrom('subject'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('region', function (opts) { opts.mapFrom('region'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('email', function (opts) { opts.mapFrom('email'); })
          .forMember('description', function (opts) { opts.mapFrom('description'); })
          .forMember('comment', function (opts) { opts.mapFrom('comment'); })
          .forMember('attachmentUrls', function (opts) { opts.mapFrom('documents'); })
          .forMember('isResolved', function (opts) { opts.mapFrom('isResolved'); })
          .forMember('IsFollowUp', function (opts) { opts.mapFrom('IsFollowUp'); });
        let notifications: Ticket = automapper.map(Ticket_GetByIdOutputDTO, Ticket, response);
        return notifications;
      }
      ));
  }

  download(url: string): Observable<HttpEvent<Blob>> {
    throw new Error('Method not implemented.');
  }
}

