import { Ticket_GetAllOutputDTO } from './../../../dtos/output/notifications/Ticket_GetAllOutputDTO';
import { map } from 'rxjs/internal/operators/map';
import { DateService } from './../../business/dates/date.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Ticket } from 'src/app/core/models/entity/notification';
import { NotificationsDataService } from './notifications.data.service';
import { environment } from 'src/environments/environment';
import { Ticket_GetByIdOutputDTO } from 'src/app/core/dtos/output/notifications/Ticket_GetByIdOutputDTO';
import { Ticket_PostPayloadDTO } from 'src/app/core/dtos/output/notifications/Ticket_PostPayloadDTO';
import { tap, last, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationsWebDataService implements NotificationsDataService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private httpClient: HttpClient, private dateService: DateService) { }

  getNewNotifications(): Observable<Ticket[]> {
    var requestUri = environment.api.baseUrl + '/v1/tickets?status=false';
    return this.httpClient.get<Ticket_GetAllOutputDTO[]>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(Ticket_GetAllOutputDTO, Ticket)
          .forMember('id', function (opts) { opts.mapFrom('ticketId'); })
          .forMember('companyName', function (opts) { opts.mapFrom('companyName'); })
          .forMember('reportedBy', function (opts) { opts.mapFrom('reportedBy'); })
          .forMember('subject', function (opts) { opts.mapFrom('subject'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('attachmentCount', function (opts) { opts.mapFrom('documentCount'); });
        let notifications: Ticket[] = automapper.map(Ticket_GetAllOutputDTO, Ticket, response);
        return notifications;
      })
    );
  }


  getNotificationsHistory(): Observable<Ticket[]> {
    var requestUri = environment.api.baseUrl + '/v1/tickets?status=true';
    return this.httpClient.get<Ticket_GetAllOutputDTO[]>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(Ticket_GetAllOutputDTO, Ticket)
          .forMember('id', function (opts) { opts.mapFrom('ticketId'); })
          .forMember('companyName', function (opts) { opts.mapFrom('companyName'); })
          .forMember('reportedBy', function (opts) { opts.mapFrom('reportedBy'); })
          .forMember('subject', function (opts) { opts.mapFrom('subject'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('attachmentCount', function (opts) { opts.mapFrom('documentCount'); });
        let notifications: Ticket[] = automapper.map(Ticket_GetAllOutputDTO, Ticket, response);
        return notifications;
      })
    );
  }

  getNotification(id: number): Observable<Ticket> {
    var requestUri = environment.api.baseUrl + `/v1/tickets/${id}`;
    return this.httpClient.get<Ticket_GetByIdOutputDTO>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(Ticket_GetByIdOutputDTO, Ticket)
          .forMember('id', function (opts) { opts.mapFrom('ticketId'); })
          .forMember('companyName', function (opts) { opts.mapFrom('companyName'); })
          .forMember('reportedBy', function (opts) { opts.mapFrom('reportedBy'); })
          .forMember('subject', function (opts) { opts.mapFrom('subject'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('region', function (opts) { opts.mapFrom('region'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('email', function (opts) { opts.mapFrom('email'); })
          .forMember('isFollowUp', function (opts) { opts.mapFrom('isFollowUp'); })
          .forMember('description', function (opts) { opts.mapFrom('description'); })
          .forMember('comment', function (opts) { opts.mapFrom('comment'); })
          .forMember('attachmentUrls', function (opts) { opts.mapFrom('documents'); })
          .forMember('availabilities', function (opts) { opts.mapFrom('availabilities'); })
          .forMember('isResolved', function (opts) { opts.mapFrom('isResolved'); })
          .forMember('startTime', function (opts) { opts.mapFrom('startTime'); })
          .forMember('resolvedBy', function (opts) { opts.mapFrom('resolvedBy'); })
          .forMember('dateResolved', function (opts) { opts.mapFrom('dateResolved'); })
          .forMember('endTime', function (opts) { opts.mapFrom('endTime'); });
        let notifications: Ticket = automapper.map(Ticket_GetByIdOutputDTO, Ticket, response);
        return notifications;
      })
    );
  }

  getAzureReadSASToken(): Observable<string> {
    const requestUri = environment.api.baseUrl + `/v1/documents/sastoken?containername=${environment.issuesContainer}&permission=read`;
    return this.httpClient.get<string>(requestUri);
  }

  markIssueAsResolved(payload: Ticket_PostPayloadDTO): Observable<Ticket> {
    var requestUri = environment.api.baseUrl + `/v1/tickets?isresolved=${payload.isResolved}`;
    return this.httpClient.put(requestUri, payload).pipe(
      map(response => {
        automapper
          .createMap(Ticket_GetByIdOutputDTO, Ticket)
          .forMember('id', function (opts) { opts.mapFrom('ticketId'); })
          .forMember('companyName', function (opts) { opts.mapFrom('companyName'); })
          .forMember('reportedBy', function (opts) { opts.mapFrom('reportedBy'); })
          .forMember('subject', function (opts) { opts.mapFrom('subject'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('region', function (opts) { opts.mapFrom('region'); })
          .forMember('date', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('email', function (opts) { opts.mapFrom('email'); })
          .forMember('description', function (opts) { opts.mapFrom('description'); })
          .forMember('comment', function (opts) { opts.mapFrom('comment'); })
          .forMember('attachmentUrls', function (opts) { opts.mapFrom('documents'); })
          .forMember('isResolved', function (opts) { opts.mapFrom('isResolved'); })
          .forMember('IsFollowUp', function (opts) { opts.mapFrom('IsFollowUp'); });
        let notifications: Ticket = automapper.map(Ticket_GetByIdOutputDTO, Ticket, response);
        return notifications;
      }
      ));
  }
  download(url: string): Observable<HttpEvent<Blob>> {
    const req = new HttpRequest('GET', url, null, {
      reportProgress: true,
      responseType: 'blob'
    });
    return this.httpClient.request(req).pipe(
      map(event => this.getEventMessage(event)),
      tap(message => console.log('MediaComponent Service tap: ', (message))),
      last(), // return last (completed) message to caller
      catchError(err => this.handleError(err))
    );
  }

  private getEventMessage(event: HttpEvent<any>) {
    switch (event.type) {
      case HttpEventType.DownloadProgress:
        // Compute and show the % done:
        const percentDone = Math.round(100 * event.loaded / event.total);
        console.log('MediaComponent Service UploadProgress: ', percentDone);
        return event as HttpEvent<Blob>;

      case HttpEventType.Response:
        console.log('MediaComponent Service Response: ', event);
        return event as HttpEvent<Blob>;
      default:
        console.log('MediaComponent Service default: ', event);
        return event as HttpEvent<Blob>;
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}
