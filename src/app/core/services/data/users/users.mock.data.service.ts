import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsersDataService } from './users.data.service';
import { Observable } from 'rxjs/internal/Observable';
import { User } from 'src/app/core/models/entity/user';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { UserGetByIdOutputDTO } from 'src/app/core/dtos/output/users/UserGetByIdOutputDTO';

@Injectable({
  providedIn: 'root'
})
export class UsersMockDataService implements UsersDataService {

  constructor(private httpClient: HttpClient) { }

  //#region Authentication

  getLoggedInUser(): Observable<User> {
    const requestUri = environment.api.baseUrl + `/v1/users/username`;
    return this.httpClient.get<UserGetByIdOutputDTO>(requestUri).pipe(
      map(response => {
        automapper
          .createMap(UserGetByIdOutputDTO, User)
          .forMember('userId', function (opts) { opts.mapFrom('userId'); })
          .forMember('userName', function (opts) { opts.mapFrom('userName'); })
          .forMember('emailAddress', function (opts) { opts.mapFrom('emailAddress'); })
          .forMember('firstName', function (opts) { opts.mapFrom('firstName'); })
          .forMember('lastName', function (opts) { opts.mapFrom('lastName'); })
          .forMember('displayName', function (opts) { opts.mapFrom('displayName'); })
          .forMember('roleNames', function (opts) { opts.mapFrom('roleNames'); })
          .forMember('status', function (opts) { opts.mapFrom('status'); })
          .forMember('statusName', function (opts) { opts.mapFrom('statusName'); })
          .forMember('createdOn', function (opts) { opts.mapFrom('createdOn'); })
          .forMember('createdBy', function (opts) { opts.mapFrom('createdBy'); })
          .forMember('modifiedOn', function (opts) { opts.mapFrom('modifiedOn'); })
          .forMember('modifiedBy', function (opts) { opts.mapFrom('modifiedBy'); })
        const user: User = automapper.map(UserGetByIdOutputDTO, User, response);
        return user;
      })
    );
  }

  //#endregion
}
