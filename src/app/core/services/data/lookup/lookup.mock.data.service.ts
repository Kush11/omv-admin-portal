import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { LookupDataService } from './lookup.data.service';

@Injectable({
  providedIn: 'root'
})
export class LookupMockDataService implements LookupDataService {
  getConfigurationKeys(): Observable<Lookup[]> {
    throw new Error("Method not implemented.");
  }

  constructor() { }
}