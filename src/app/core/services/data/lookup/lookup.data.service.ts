import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lookup } from 'src/app/core/models/entity/lookup';

@Injectable({
  providedIn: 'root'
})
export abstract class LookupDataService {

  abstract getConfigurationKeys(): Observable<Lookup[]>;
}