import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import * as automapper from 'automapper-ts';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { LookupDataService } from './lookup.data.service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Lookup_GetByLookupTypeOutputDTO } from 'src/app/core/dtos/output/lookup/Lookup_GetByLookupTypeOutputDTO';

@Injectable({
  providedIn: 'root'
})
export class LookupWebDataService implements LookupDataService {

  baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient) { }

  getConfigurationKeys(): Observable<Lookup[]> {
    const requestUri = `${this.baseUrl}/v1/lookups`;
    const param = { lookupType: "CUSTOMER_SETTING_KEY" };
    return this.httpClient.get<Lookup_GetByLookupTypeOutputDTO[]>(requestUri, { params: param })
      .pipe(
        map(response => {
          let types = this.mapResponse(response);
          console.log("getConfigurationKeys", types)
          return types;
        })
      );
  }

  private mapResponse(apiResponse: Lookup_GetByLookupTypeOutputDTO[]): Lookup[] {
    automapper
      .createMap(Lookup_GetByLookupTypeOutputDTO, Lookup)
      .forMember('type', function (opts) { opts.mapFrom('lookupType'); })
      .forMember('value', function (opts) { opts.mapFrom('lookupValue'); })
      .forMember('description', function (opts) { opts.mapFrom('lookupDescription'); })
      .forMember('sort', function (opts) { opts.mapFrom('lookupSort'); })
      .forMember('status', function (opts) { opts.mapFrom('status'); });

    const lookups: Lookup[] = automapper.map(Lookup_GetByLookupTypeOutputDTO, Lookup, apiResponse);
    return lookups;
  }
}