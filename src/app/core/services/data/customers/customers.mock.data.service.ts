import { LiveStreamCamera } from 'src/app/core/models/entity/camera';
import { Customer_InsertInputDTO } from '../../../dtos/input/Customer_InsertInputDTO';
import { ConfigurationSetting } from './../../../models/entity/configuration';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CustomersDataService } from './customers.data.service';
import * as automapper from 'automapper-ts';
import { Company } from 'src/app/core/models/entity/company';
import { map } from 'rxjs/internal/operators/map';
import { of } from 'rxjs/internal/observable/of';
import { CustomerGetAllOutputDTO } from 'src/app/core/dtos/output/customers/Customer_GetAllOutputDTO';
import { CustomerSetting_GetByCustomerIdOutputDTO } from 'src/app/core/dtos/output/customers/Customer_GetConfigurationOutputDTO';
import { BlobService } from 'angular-azure-blob-service';
import { Document } from '../../../models/entity/document';
import { Permission } from 'src/app/core/models/entity/permissions';
import { SecurityProvider } from 'src/app/core/models/entity/security-provider';
import { Camera_InsertInputDTO } from 'src/app/core/dtos/input/Customer_CameraInsertInputDTO';
import { Camera_GetAllOutputDTO } from 'src/app/core/dtos/output/customers/Customer_GetCameraOutputDTO';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { ErrorMessage } from 'src/app/core/models/entity/error-message';
import { Directory } from 'src/app/core/models/entity/directory';

@Injectable({
  providedIn: 'root'
})
export class CustomersMockDataService implements CustomersDataService {
  getDirectories(id: number, parentId?: number): Observable<Directory[]> {
    throw new Error("Method not implemented.");
  }
  getTypes(id: number): Observable<string[]> {
    throw new Error("Method not implemented.");
  }
  getAzureReadSASToken(id: number): Observable<string> {
    throw new Error('Method not implemented.');
  }

  createLookup(id: number, lookup: Lookup) {
    throw new Error('Method not implemented.');
  }
  updateLookup(id: number, lookup: Lookup) {
    throw new Error('Method not implemented.');
  }
  deleteLookup(id: number, lookup: Lookup) {
    throw new Error('Method not implemented.');
  }

  getAzureWriteSASToken(id: number): Observable<string> {
    throw new Error('Method not implemented.');
  }


  constructor(private httpClient: HttpClient, private blob: BlobService) { }

  getSetting(setting: string): Observable<any> {
    const requestUri = environment.api.baseUrl + `/v1/customers/setting`;
    const options = { params: new HttpParams() };

    options.params = options.params.set('settingkey', setting);

    return this.httpClient.get<any>(requestUri, options);
  }

  getSecurityProviders(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/securityprovider`;
    return this.httpClient.get<SecurityProvider[]>(requestUri);
  }

  getCustomers(): Observable<Company[]> {
    const requestUri = environment.api.baseUrl + `/v1/customers`;
    return this.httpClient.get<Company[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(CustomerGetAllOutputDTO, Company)
            .forMember('id', function (opts) { opts.mapFrom('customerId'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('companyTitle', function (opts) { opts.mapFrom('companyTitle'); })
            .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
            .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
            .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
            .forMember('imageURL', function (opts) { opts.mapFrom('imageUrl'); })
            .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); })
            .forMember('pointsOfContact', function (opts) { opts.mapFrom('pointOfContact'); });
          const companies: Company[] = automapper.map(CustomerGetAllOutputDTO, Company, response);
          console.log('CustomersMockDataService - getCustomers: ', companies);
          return companies;
        })
      );
  }

  getCustomer(id: number): Observable<Company> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}`;
    return this.httpClient.get<Company>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(CustomerGetAllOutputDTO, Company)
            .forMember('id', function (opts) { opts.mapFrom('customerId'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('companyTitle', function (opts) { opts.mapFrom('companyTitle'); })
            .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
            .forMember('contractType', function (opts) { opts.mapFrom('contractType'); })
            .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
            .forMember('imageURL', function (opts) { opts.mapFrom('imageUrl'); })
            .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); })
            .forMember('pointsOfContact', function (opts) { opts.mapFrom('pointOfContact'); });
          const customer: Company = automapper.map(CustomerGetAllOutputDTO, Company, response);
          console.log('CustomersMockDataService - getCustomer: ', customer);
          return customer;
        })
      );
  }

  getConfigurationSettings(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/setting`;
    return this.httpClient.get<ConfigurationSetting[]>(requestUri)
      .pipe(
        map(
          config => {
            automapper
              .createMap(CustomerSetting_GetByCustomerIdOutputDTO, ConfigurationSetting)
              .forMember('customerId', function (opts) { opts.mapFrom('customerId'); })
              .forMember('settingKey', function (opts) { opts.mapFrom('name'); })
              .forMember('value', function (opts) { opts.mapFrom('value'); })
              .forMember('type', function (opts) { opts.mapFrom('section'); })
              .forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); });
            const company: ConfigurationSetting[] = automapper.map(CustomerSetting_GetByCustomerIdOutputDTO, ConfigurationSetting, config);
            console.log('CustomersMockDataService - getCustomersConfigurations: ', company);
            return company;
          })
      );
  }


  getPermissions(id: number): Observable<Permission> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/permissions`;
    return this.httpClient.get<Permission>(requestUri)
      .pipe(
        map(response => {
          // automapper
          // .createMap(CustomerGetAllOutputDTO, Settings)
          // .forMember('menuName', function(opts) { opts.mapFrom('menuName'); })
          // .forMember('isPermitted', function(opts) { opts.mapFrom('isPermitted'); });
          // const settings: Settings = automapper.map(Customer_InsertSettingsInputDTO , response);
          // console.log('CustomersMockDataService - getSettings: ', settings);
          return response;
        })
      );
  }

  getLiveStreamCameras(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/camera`;
    return this.httpClient.get<LiveStreamCamera[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(Camera_GetAllOutputDTO, LiveStreamCamera)
            .forMember('cameraId', function (opts) { opts.mapFrom('cameraId'); })
            .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('name'))
            .forMember('livestreamURL', function (opts) { opts.mapFrom('livestreamURL'); })
            .forMember('asset', function (opts) { opts.mapFrom('asset'); })
            .forMember('group', function (opts) { opts.mapFrom('group'); });
          const camera: LiveStreamCamera[] = automapper.map(Camera_GetAllOutputDTO, LiveStreamCamera, response);
          return camera;
        })
      );
  }


  


  createCustomer(customer: Company) {
    const requestUri = environment.api.baseUrl + `/v1/customers`;
    automapper
      .createMap(Company, Customer_InsertInputDTO)
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
      .forMember('contractType', function (opts) { opts.mapFrom('contractType'); })
      .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
      .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); })
      .forMember('pointOfContact', function (opts) { opts.mapFrom('pointsOfContact'); });

    const request = automapper.map(Company, Customer_InsertInputDTO, customer);

    return this.httpClient.post(requestUri, request)
      .pipe(map(
        response => {
          automapper
            .createMap(Customer_InsertInputDTO, Company)
            .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('name'))
            .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
            .forMember('contractType', function (opts) { opts.mapFrom('contractType'); })
            .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
            .forMember('imageURL', function (opts) { opts.mapFrom('imageURL'); })
            .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); })
            .forMember('pointsOfContact', function (opts) { opts.mapFrom('pointOfContact'); });

          const customers: Company = automapper.map(Company, Customer_InsertInputDTO, response);
          return customers;
        },
        err => console.log(err)
      ));
  }


  createLiveStreamCamera(companyId: number, camera: LiveStreamCamera) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/camera`;
    automapper
      .createMap(LiveStreamCamera, Camera_InsertInputDTO)
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('liveStreamURl', function (opts) { opts.mapFrom('liveStreamURl'); })
      .forMember('asset', function (opts) { opts.mapFrom('asset'); })
      .forMember('group', function (opts) { opts.mapFrom('group'); });

    const request = automapper.map(LiveStreamCamera, Camera_InsertInputDTO, camera);

    return this.httpClient.post(requestUri, request)
      .pipe(map(
        response => {
          automapper
            .createMap(Camera_InsertInputDTO, LiveStreamCamera)
            .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('name'))
            .forMember('liveStreamURl', function (opts) { opts.mapFrom('liveStreamURl'); })
            .forMember('asset', function (opts) { opts.mapFrom('asset'); })
            .forMember('group', function (opts) { opts.mapFrom('group'); });
          const cameras: LiveStreamCamera = automapper.map(LiveStreamCamera, Camera_InsertInputDTO, response);
          return cameras;
        },
        err => console.log(err)
      ));
  }

  createSecurityProvider(companyId: number, setting: SecurityProvider) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/securityprovider`;
    return this.httpClient.post(requestUri, setting)
      .pipe(map(
        response => {
          return response;
        }
      ));
  }

  createConfigurationSetting(companyId: number, customer: ConfigurationSetting) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/setting`;

    automapper
      .createMap(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO)
      .forMember('settingKey', function (opts) { opts.mapFrom('settingKey'); })
      .forMember('type', function (opts) { opts.mapFrom('type'); })
      .forMember('value', function (opts) { opts.mapFrom('value'); });

    const request = automapper.map(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO, customer);

    return this.httpClient.post(requestUri, request)
      .pipe(
        map(
          response => {
            automapper
              .createMap(CustomerSetting_GetByCustomerIdOutputDTO, ConfigurationSetting)
              .forMember('settingKey', function (opts) { opts.mapFrom('settingKey') })
              .forMember('type', function (opts) { opts.mapFrom('type'); })
              .forMember('value', function (opts) { opts.mapFrom('value'); });
            const configuration: ConfigurationSetting = automapper.map(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO, response);
            return configuration;
          }
        ));
  }

  createPermission(companyId: number, permission: Permission[]) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/permissions`;
    return this.httpClient.post(requestUri, permission)
      .pipe(map(
        response => {
          return response;
        }
      ));
  }

  updatePermissions(companyId: number, permission: Permission[]) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/permissions`;
    return this.httpClient.put(requestUri, permission)
      .pipe(map(
        response => {
          return response;
        }
      ));
  }

  updateSecurityProvider(companyId: number, setting: SecurityProvider) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/securityprovider`;
    return this.httpClient.put(requestUri, setting)
      .pipe(map(
        response => {
          return response;
        }
      ));
  }

  updateCustomer(id: number, customer: Company): Observable<any> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}`;

    automapper
      .createMap(Company, Customer_InsertInputDTO)
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
      .forMember('contractType', function (opts) { opts.mapFrom('contractType'); })
      .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
      .forMember('imageURL', function (opts) { opts.mapFrom('imageUrl'); })
      .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); })
      .forMember('pointOfContact', function (opts) { opts.mapFrom('pointsOfContact'); });

    const request = automapper.map(Company, Customer_InsertInputDTO, customer);
    return this.httpClient.put(requestUri, request)
      .pipe(map(
        response => {
          automapper
            .createMap(Customer_InsertInputDTO, Company)
            .forMember('name', (opts: AutoMapperJs.IMemberConfigurationOptions) => opts.mapFrom('name'))
            .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
            .forMember('contractType', function (opts) { opts.mapFrom('contractType'); })
            .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
            .forMember('imageURL', function (opts) { opts.mapFrom('imageURL'); })
            .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); })
            .forMember('pointsOfContact', function (opts) { opts.mapFrom('pointOfContact'); });

          const customers: Company = automapper.map(Company, Customer_InsertInputDTO, response);
          console.log('CustomersMockDataService - UpdateCustomer', customers);
          return customers;
        }
      ));
  }

  updateConfigurationSetting(companyId: number, customer: ConfigurationSetting) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/setting`;

    automapper
      .createMap(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO)
      .forMember('settingKey', function (opts) { opts.mapFrom('settingKey'); })
      .forMember('type', function (opts) { opts.mapFrom('type'); })
      .forMember('value', function (opts) { opts.mapFrom('value'); });

    const request = automapper.map(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO, customer);

    return this.httpClient.put(requestUri, request)
      .pipe(
        map(
          response => {
            automapper
              .createMap(CustomerSetting_GetByCustomerIdOutputDTO, ConfigurationSetting)
              .forMember('settingKey', function (opts) { opts.mapFrom('settingKey'); })
              .forMember('type', function (opts) { opts.mapFrom('type'); })
              .forMember('value', function (opts) { opts.mapFrom('value'); });
            const configuration: ConfigurationSetting = automapper.map(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO, response);
            return configuration;
          }
        ));
  }

  deleteCustomer(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}`;
    return this.httpClient.delete(requestUri);
  }

  deleteCompanySettings(id: number, clientId: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/securityprovider?clientId=${clientId}`;
    return this.httpClient.delete(requestUri);
  }

  deleteConfigurationSetting(id: number, settingKey: string) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/setting`;
    const param = { key: settingKey };
    return this.httpClient.delete(requestUri, { params: param });
  }

  impersonateCustomer(id: number) {
    return of({});
  }

  migrateDatabase(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/initialize/${id}`;
    return this.httpClient.post(requestUri, null);
  }

  updateLiveStreamCamera(id: number, camera: LiveStreamCamera) {
    return of({});
  }

  deleteLiveStreamCamera(companyId: number, cameraId: number) {
    return of({});
  }

  getLiveStreamGroups(id: number): Observable<Lookup[]> {
    throw new Error('Method not implemented.');
  }

  getLiveStreamAssets(id: number): Observable<Lookup[]> {
    throw new Error('Method not implemented.');
  }

  getErrorMessages(): Observable<ErrorMessage[]> {
    throw new Error('Method not implemented.');
  }
  
  getLookups(id: number): Observable<Lookup[]> {
    throw new Error('Method not implemented.');
}

}

