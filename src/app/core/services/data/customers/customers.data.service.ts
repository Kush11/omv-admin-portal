import { ErrorMessage } from './../../../models/entity/error-message';
import { permission } from './../../../enum/permission';
import { Permission } from './../../../models/entity/permissions';
import { ConfigurationSetting } from 'src/app/core/models/entity/configuration';
import { Observable } from 'rxjs/internal/Observable';
import { Company } from 'src/app/core/models/entity/company';
import { Document } from '../../../models/entity/document';
import { SecurityProvider } from 'src/app/core/models/entity/security-provider';
import { LiveStreamCamera } from 'src/app/core/models/entity/camera';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { Directory } from 'src/app/core/models/entity/directory';


export abstract class CustomersDataService {

  constructor() { }

  //#region Customers  
  abstract getCustomers(): Observable<Company[]>;
  abstract getCustomer(id: number): Observable<Company>;
  abstract createCustomer(company: Company);
  abstract updateCustomer(id: number, customer: Company);
  abstract deleteCustomer(id: number);
  abstract getAzureReadSASToken(id: number): Observable<string>;
  abstract getAzureWriteSASToken(id: number): Observable<string>;
  //#endregion

  //#region Configuration Settings
  abstract getConfigurationSettings(id: number): Observable<ConfigurationSetting[]>;
  abstract createConfigurationSetting(id: number, configuration: ConfigurationSetting);
  abstract deleteConfigurationSetting(id: number, key: string);
  abstract updateConfigurationSetting(id: number, configuration: ConfigurationSetting);
  //#endregion

  //#region Security Providers
  abstract getSecurityProviders(id: number): Observable<SecurityProvider[]>;
  abstract createSecurityProvider(companyId: number, setting: SecurityProvider);
  abstract updateSecurityProvider(companyId: number, setting: SecurityProvider);
  //#endregion

  //#region Permissions
  abstract getPermissions(companyId: number): Observable<Permission>;
  abstract createPermission(companyId: number, permission: Permission[]);
  abstract updatePermissions(companyId: number, permission: Permission[]);
  //#endregion

  //#region LiveStream Cameras
  abstract getLiveStreamCameras(companyId: number): Observable<LiveStreamCamera[]>;
  abstract createLiveStreamCamera(companyId: number, camera: LiveStreamCamera);
  abstract updateLiveStreamCamera(companyId: number, camera: LiveStreamCamera);
  abstract deleteLiveStreamCamera(companyId: number, cameraId: number);
  //#endregion

  //#region Lookups
  abstract getTypes(id: number): Observable<string[]>;
  abstract getLiveStreamAssets(id: number): Observable<Lookup[]>;
  abstract getLiveStreamGroups(id: number): Observable<Lookup[]>;
  abstract getLookups(id: number): Observable<Lookup[]>;
  abstract createLookup(id: number, lookup: Lookup);
  abstract updateLookup(id: number, lookup: Lookup);
  abstract deleteLookup(id: number, lookup: Lookup);
  //#endregion

  //#region Directories
  abstract getDirectories(id: number, parentId?: number): Observable<Directory[]>;
  //#endregion

  abstract migrateDatabase(id: number);
  abstract getErrorMessages(): Observable<ErrorMessage[]>;
}

