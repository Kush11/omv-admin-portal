import { LiveStreamCamera } from 'src/app/core/models/entity/camera';
import { Customer_InsertInputDTO } from '../../../dtos/input/Customer_InsertInputDTO';
import { ConfigurationSetting } from './../../../models/entity/configuration';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CustomersDataService } from './customers.data.service';
import * as automapper from 'automapper-ts';
import { Company } from 'src/app/core/models/entity/company';
import { map } from 'rxjs/internal/operators/map';
import { CustomerGetAllOutputDTO } from 'src/app/core/dtos/output/customers/Customer_GetAllOutputDTO';
import { CustomerSetting_GetByCustomerIdOutputDTO } from 'src/app/core/dtos/output/customers/Customer_GetConfigurationOutputDTO';
import { Document } from '../../../models/entity/document';
import { Permission } from 'src/app/core/models/entity/permissions';
import { Camera_InsertInputDTO } from 'src/app/core/dtos/input/Customer_CameraInsertInputDTO';
import { Camera_GetAllOutputDTO } from 'src/app/core/dtos/output/customers/Customer_GetCameraOutputDTO';
import { Camera_UpdateInputDTO } from 'src/app/core/dtos/input/live-stream/Camera_UpdateInputDTO';
import { SecurityProvider } from 'src/app/core/models/entity/security-provider';
import { Lookup } from 'src/app/core/models/entity/lookup';
import { Lookup_GetByLookupTypeOutputDTO } from 'src/app/core/dtos/output/lookup/Lookup_GetByLookupTypeOutputDTO';
import { CustomerSecurityProvider_GetAllOutputDTO } from 'src/app/core/dtos/output/security-providers/CustomerSecurityProvider_GetAllOutputDTO';
import { ErrorMessage } from 'src/app/core/models/entity/error-message';
import { ErrorMessage_GetByCustomerIdOutputDTO } from 'src/app/core/dtos/output/error-messages/ErrorMessage_GetByCustomerIdOutputDTO';
import { Lookup_InsertOutputDTO } from 'src/app/core/dtos/input/lookup/Lookup_InsertOutputDTO';
import { Directory } from 'src/app/core/models/entity/directory';
import { Directory_GetAllOutputDTO } from 'src/app/core/dtos/output/directories/Directory_GetAllOutputDTO';
import { Lookup_DeleteInputDTO } from 'src/app/core/dtos/input/lookup/Lookup_DeleteInputDTO';

@Injectable({
  providedIn: 'root'
})
export class CustomersWebDataService implements CustomersDataService {

  constructor(private httpClient: HttpClient) { }

  //#region Customers

  getCustomers(): Observable<Company[]> {
    const requestUri = environment.api.baseUrl + `/v1/customers`;
    return this.httpClient.get<Company[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(CustomerGetAllOutputDTO, Company)
            .forMember('id', function (opts) { opts.mapFrom('customerId'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('companyTitle', function (opts) { opts.mapFrom('companyTitle'); })
            .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
            .forMember('contractType', function (opts) { opts.mapFrom('contractType'); })
            .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
            .forMember('imageUrl', function (opts) { opts.mapFrom('imageURL'); })
            .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); })
            .forMember('pointsOfContact', function (opts) { opts.mapFrom('pointOfContact'); });
          const companies: Company[] = automapper.map(CustomerGetAllOutputDTO, Company, response);
          return companies;
        })
      );
  }

  getCustomer(id: number): Observable<Company> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}`;
    return this.httpClient.get<Company>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(CustomerGetAllOutputDTO, Company)
            .forMember('id', function (opts) { opts.mapFrom('customerId'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('companyTitle', function (opts) { opts.mapFrom('companyTitle'); })
            .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
            .forMember('contractType', function (opts) { opts.mapFrom('contractType'); })
            .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
            .forMember('imageUrl', function (opts) { opts.mapFrom('imageURL'); })
            .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); })
            .forMember('pointsOfContact', function (opts) { opts.mapFrom('pointOfContact'); });
          const customer: Company = automapper.map(CustomerGetAllOutputDTO, Company, response);
          return customer;
        })
      );
  }

  createCustomer(customer: Company) {
    const requestUri = environment.api.baseUrl + `/v1/customers`;
    automapper
      .createMap(Company, Customer_InsertInputDTO)
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
      .forMember('contractType', function (opts) { opts.mapFrom('contractType'); })
      .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
      .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); })
      .forMember('pointOfContact', function (opts) { opts.mapFrom('pointsOfContact'); });

    const request = automapper.map(Company, Customer_InsertInputDTO, customer);

    return this.httpClient.post(requestUri, request);
  }

  updateCustomer(id: number, company: Company): Observable<any> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}`;

    automapper
      .createMap(Company, Customer_InsertInputDTO)
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('companyType', function (opts) { opts.mapFrom('companyType'); })
      .forMember('contractType', function (opts) { opts.mapFrom('contractType'); })
      .forMember('connectionString', function (opts) { opts.mapFrom('connectionString'); })
      .forMember('imageURL', function (opts) { opts.mapFrom('imageUrl'); })
      .forMember('hostHeader', function (opts) { opts.mapFrom('hostHeader'); });

    const request: Customer_InsertInputDTO = automapper.map(Company, Customer_InsertInputDTO, company);
    request.pointOfContact = company.pointsOfContact;

    return this.httpClient.put(requestUri, request);
  }

  getAzureReadSASToken(id: number): Observable<string> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/sastoken?permission=read`;
    return this.httpClient.get<string>(requestUri);
  }

  getAzureWriteSASToken(id: number): Observable<string> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/sastoken?permission=write`;
    return this.httpClient.get<string>(requestUri);
  }

  deleteCustomer(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}`;
    return this.httpClient.delete(requestUri);
  }

  //#endregion

  //#region Configuration Settings

  getSetting(setting: string): Observable<any> {
    const requestUri = environment.api.baseUrl + `/v1/customers/setting`;
    const options = { params: new HttpParams() };
    options.params = options.params.set('settingkey', setting);

    return this.httpClient.get<any>(requestUri, options);
  }

  getConfigurationSettings(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/setting`;
    return this.httpClient.get<ConfigurationSetting[]>(requestUri)
      .pipe(
        map(
          config => {
            automapper
              .createMap(CustomerSetting_GetByCustomerIdOutputDTO, ConfigurationSetting)
              .forMember('key', function (opts) { opts.mapFrom('settingKey'); })
              .forMember('value', function (opts) { opts.mapFrom('value'); })
              .forMember('type', function (opts) { opts.mapFrom('section'); })
              .forMember('customerId', function (opts) { opts.mapFrom('customerId'); })
              .forMember('isSystem', function (opts) { opts.mapFrom('isSystem'); });
            const company: ConfigurationSetting[] = automapper.map(CustomerSetting_GetByCustomerIdOutputDTO, ConfigurationSetting, config);
            return company;
          })
      );
  }

  createConfigurationSetting(companyId: number, setting: ConfigurationSetting) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/setting`;

    automapper
      .createMap(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO)
      .forMember('settingKey', function (opts) { opts.mapFrom('key'); })
      .forMember('type', function (opts) { opts.mapFrom('type'); })
      .forMember('value', function (opts) { opts.mapFrom('value'); });

    const request = automapper.map(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO, setting);

    return this.httpClient.post(requestUri, request);
  }

  updateConfigurationSetting(companyId: number, customer: ConfigurationSetting) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/setting`;

    automapper
      .createMap(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO)
      .forMember('settingKey', function (opts) { opts.mapFrom('key'); })
      .forMember('type', function (opts) { opts.mapFrom('type'); })
      .forMember('value', function (opts) { opts.mapFrom('value'); });

    const request = automapper.map(ConfigurationSetting, CustomerSetting_GetByCustomerIdOutputDTO, customer);

    return this.httpClient.put(requestUri, request);
  }

  deleteConfigurationSetting(id: number, settingKey: string) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/setting`;
    const param = { key: settingKey };
    return this.httpClient.delete(requestUri, { params: param });
  }

  //#endregion

  //#region Security Providers

  getSecurityProviders(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/securityproviders`;
    return this.httpClient.get<SecurityProvider[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(CustomerSecurityProvider_GetAllOutputDTO, SecurityProvider)
            .forMember('id', function (opts) { opts.mapFrom('securityProviderId'); })
            .forMember('customerId', function (opts) { opts.mapFrom('customerId'); })
            .forMember('clientId', function (opts) { opts.mapFrom('clientId'); })
            .forMember('issuerUrl', function (opts) { opts.mapFrom('issuerUrl'); })
            .forMember('authServerId', function (opts) { opts.mapFrom('authServerId'); })
            .forMember('authType', function (opts) { opts.mapFrom('authType'); });
          const camera: SecurityProvider[] = automapper.map(CustomerSecurityProvider_GetAllOutputDTO, SecurityProvider, response);
          return camera;
        })
      );
  }

  createSecurityProvider(companyId: number, setting: SecurityProvider) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/securityproviders`;
    return this.httpClient.post(requestUri, setting);
  }

  updateSecurityProvider(companyId: number, setting: SecurityProvider) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/securityproviders/${setting.id}`;
    return this.httpClient.put(requestUri, setting);
  }

  //#endregion

  //#region Permissions

  getPermissions(id: number): Observable<Permission> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/permissions`;
    return this.httpClient.get<Permission>(requestUri);
  }

  createPermission(companyId: number, permission: Permission[]) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/permissions`;
    return this.httpClient.post(requestUri, permission);
  }

  updatePermissions(companyId: number, permission: Permission[]) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/permissions`;
    return this.httpClient.put(requestUri, permission);
  }

  //#endregion

  //#region Cameras

  getLiveStreamCameras(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/cameras`;
    return this.httpClient.get<LiveStreamCamera[]>(requestUri)
      .pipe(
        map(response => {
          automapper
            .createMap(Camera_GetAllOutputDTO, LiveStreamCamera)
            .forMember('id', function (opts) { opts.mapFrom('cameraId'); })
            .forMember('name', function (opts) { opts.mapFrom('name'); })
            .forMember('source', function (opts) { opts.mapFrom('livestreamURL'); })
            .forMember('asset', function (opts) { opts.mapFrom('asset'); })
            .forMember('assetName', function (opts) { opts.mapFrom('assetName'); })
            .forMember('group', function (opts) { opts.mapFrom('group'); })
            .forMember('groupName', function (opts) { opts.mapFrom('groupName'); })
            .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); });

          const camera: LiveStreamCamera[] = automapper.map(Camera_GetAllOutputDTO, LiveStreamCamera, response);
          return camera;
        })
      );
  }

  createLiveStreamCamera(companyId: number, camera: LiveStreamCamera) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/cameras`;
    automapper
      .createMap(LiveStreamCamera, Camera_InsertInputDTO)
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('liveStreamURl', function (opts) { opts.mapFrom('source'); })
      .forMember('asset', function (opts) { opts.mapFrom('asset'); })
      .forMember('group', function (opts) { opts.mapFrom('group'); })
      .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); });

    const request = automapper.map(LiveStreamCamera, Camera_InsertInputDTO, camera);

    return this.httpClient.post(requestUri, request);
  }

  updateLiveStreamCamera(companyId: number, camera: LiveStreamCamera) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/cameras/${camera.id}`;

    automapper
      .createMap(LiveStreamCamera, Camera_UpdateInputDTO)
      .forMember('name', function (opts) { opts.mapFrom('name'); })
      .forMember('liveStreamURl', function (opts) { opts.mapFrom('source'); })
      .forMember('asset', function (opts) { opts.mapFrom('asset'); })
      .forMember('group', function (opts) { opts.mapFrom('group'); })
      .forMember('directoryId', function (opts) { opts.mapFrom('directoryId'); });

    const request = automapper.map(LiveStreamCamera, Camera_UpdateInputDTO, camera);
    return this.httpClient.put(requestUri, request);
  }

  deleteLiveStreamCamera(companyId: number, cameraId: number) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${companyId}/cameras/${cameraId}`;
    return this.httpClient.delete(requestUri);
  }

  //#endregion

  //#region Lookups

  getTypes(id: number): Observable<string[]> {
    const requestUri = `${environment.api.baseUrl}/v1/customers/${id}/lookuptypes`;
    return this.httpClient.get<string[]>(requestUri);
  }

  getLiveStreamAssets(id: number): Observable<Lookup[]> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/lookups`;
    const options = { params: new HttpParams() };
    options.params = options.params.set('lookupType', "LIVE_STREAM_ASSETS");

    return this.httpClient.get<Lookup_GetByLookupTypeOutputDTO[]>(requestUri, options)
      .pipe(
        map(response => {
          let lookups = this.mapResponse(response);
          lookups.forEach(lookup => lookup.value = Number(lookup.value));
          return lookups;
        })
      );
  }

  getLiveStreamGroups(id: number): Observable<Lookup[]> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/lookups`;
    const options = { params: new HttpParams() };
    options.params = options.params.set('LookupType', "LIVE_STREAM_GROUPS");

    return this.httpClient.get<Lookup_GetByLookupTypeOutputDTO[]>(requestUri, options)
      .pipe(
        map(response => {
          let lookups = this.mapResponse(response);
          lookups.forEach(lookup => lookup.value = Number(lookup.value));
          return lookups;
        })
      )
  }

  getLookups(id: number): Observable<Lookup[]> {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/lookups`;

    return this.httpClient.get<Lookup_GetByLookupTypeOutputDTO[]>(requestUri)
      .pipe(
        map(response => {
          let lookups = this.mapResponse(response);
          lookups.forEach(lookup => lookup.value = Number(lookup.value));
          return lookups;
        })
      )
  }

  createLookup(id: number, lookup: Lookup) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/lookups`;

    automapper
      .createMap(Lookup, Lookup_InsertOutputDTO)
      .forMember('lookupType', function (opts) { opts.mapFrom('type'); })
      .forMember('lookupValue', function (opts) { opts.mapFrom('value'); })
      .forMember('lookupDescription', function (opts) { opts.mapFrom('description'); });
    const request = automapper.map(Lookup, Lookup_InsertOutputDTO, lookup);

    return this.httpClient.post(requestUri, request);
  }

  updateLookup(id: number, lookup: Lookup) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/lookups`;

    automapper
      .createMap(Lookup, Lookup_InsertOutputDTO)
      .forMember('lookupType', function (opts) { opts.mapFrom('type'); })
      .forMember('lookupValue', function (opts) { opts.mapFrom('value'); })
      .forMember('lookupDescription', function (opts) { opts.mapFrom('description'); });
    const request = automapper.map(Lookup, Lookup_InsertOutputDTO, lookup);

    return this.httpClient.put(requestUri, request);
  }

  deleteLookup(id: number, lookup: Lookup) {
    const requestUri = environment.api.baseUrl + `/v1/customers/${id}/lookups/${lookup.type}/${lookup.value}`;

    // const requestUri = environment.api.baseUrl + `/v1/customers/${id}/lookups/`;
    // throw new Error("Method not implemented.");

    automapper
    .createMap(Lookup, Lookup_DeleteInputDTO)
    .forMember('lookupType', function (opts) { opts.mapFrom('type'); })
    .forMember('lookupValue', function (opts) { opts.mapFrom('value'); })
    .forMember('lookupDescription', function (opts) { opts.mapFrom('description'); });
  const request = automapper.map(Lookup, Lookup_DeleteInputDTO, lookup);

  console.log("DELETED LOOKUP", requestUri, request)

  return this.httpClient.delete(requestUri, request);
  }

  private mapResponse(apiResponse: Lookup_GetByLookupTypeOutputDTO[]): Lookup[] {
    automapper
      .createMap(Lookup_GetByLookupTypeOutputDTO, Lookup)
      .forMember('type', function (opts) { opts.mapFrom('lookupType'); })
      .forMember('value', function (opts) { opts.mapFrom('lookupValue'); })
      .forMember('description', function (opts) { opts.mapFrom('lookupDescription'); })
      .forMember('sort', function (opts) { opts.mapFrom('lookupSort'); })
      .forMember('status', function (opts) { opts.mapFrom('status'); });

    const lookups: Lookup[] = automapper.map(Lookup_GetByLookupTypeOutputDTO, Lookup, apiResponse);
    return lookups;
  }

  //#endregion

  //#region Directories

  getDirectories(id: number, parentId?: number): Observable<Directory[]> {
    let requestUri = environment.api.baseUrl + `/v1/customers/${id}/directories`;

    const options = {
      params: new HttpParams()
    };
    if (parentId) options.params = options.params.set('parentId', parentId.toString());

    return this.httpClient.get<Directory_GetAllOutputDTO[]>(requestUri, options)
      .pipe(
        map(response => {
          automapper
            .createMap(Directory_GetAllOutputDTO, Directory)
            .forMember('id', function (opts) { opts.mapFrom('directoryId'); })
            .forMember('name', function (opts) { opts.mapFrom('directoryName'); })
            .forMember('parentId', function (opts) { opts.mapFrom('directoryParentId'); })
            .forMember('hasChild', function (opts) { opts.mapFrom('hasChild'); });

          let folders: Directory[] = automapper.map(Directory_GetAllOutputDTO, Directory, response);
          folders.forEach(item => item.parentId = item.parentId !== 0 ? item.parentId : null);
          return folders;
        })
      );
  }

  //#endregion

  migrateDatabase(id: number) {
    const requestUri = environment.api.baseUrl + `/v1/initialize/${id}`;
    return this.httpClient.post(requestUri, null);
  }

  getErrorMessages() {
    // const requestUri = environment.api.baseUrl + `/v1/customers/${id}/setting`;
    const requestUri = `./assets/mock/error-messages.json`;
    return this.httpClient.get<ErrorMessage[]>(requestUri)
      .pipe(
        map(
          message => {
            automapper
              .createMap(ErrorMessage_GetByCustomerIdOutputDTO, ErrorMessage)
              .forMember('timestamp', function (opts) { opts.mapFrom('time'); })
              .forMember('message', function (opts) { opts.mapFrom('message'); })
            const errorMessages: ErrorMessage[] = automapper.map(ErrorMessage_GetByCustomerIdOutputDTO, ErrorMessage, message);
            return errorMessages;
          })
      );
  }
}

