import { Injectable } from '@angular/core';
import { BlobUploadParams } from '../../models/blob-upload-params';
import { BlobService } from 'angular-azure-blob-service';
import { Store } from '@ngxs/store';
import { Document } from '../../models/entity/document';
import { ShowErrorMessage } from '../../../state/app.actions';
import { Company } from '../../models/entity/company';
import { CreateCompany, GetCompanies, UpdateCompany } from 'src/app/companies/state/companies.action';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  constructor(private blob: BlobService, private store: Store) { }

  uploadCompanyLogo(containerName: string, storageAccount: string, sasToken: string,
    file: File, company: Company, isEdit: boolean, isDetail: boolean, customerId?: number) {
    const Config: BlobUploadParams = {
      sas: sasToken,
      storageAccount: storageAccount,
      containerName: containerName
    };
    if (file) {
      const baseUrl = this.blob.generateBlobUrl(Config, file.name);
      const config = {
        baseUrl: baseUrl,
        sasToken: Config.sas,
        file: file,
        complete: () => {
          let thumbnail = `https://${Config.storageAccount}.blob.core.windows.net/thumbs/${file.name}`;

          const document = new Document();
          document.documentName = file.name;
          document.size = file.size;
          document.thumbnailContainerUrl = thumbnail;
          document.documentTypeCode = file.type;
          if (!isDetail && !isEdit) {
            this.store.dispatch(new CreateCompany(company)).toPromise()
              .then(
                (res) => this.store.dispatch(new GetCompanies())
              );
          } else if (isDetail || isEdit) {
            this.store.dispatch(new UpdateCompany(customerId, company)).toPromise()
              .then(
                (res) => this.store.dispatch(new GetCompanies())
              );
          }
          console.log('Transfer completed !');
        },
        error: (err) => {
          console.log('Error:', err);
        },
        progress: (percent) => {
          console.log('upload progress =>', percent);
        }
      };
      this.blob.upload(config);
    } else {
      this.store.dispatch(new ShowErrorMessage('No file found'));
    }
  }

}
