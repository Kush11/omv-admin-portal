import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { tap } from 'rxjs/internal/operators/tap';
import { Directory } from 'src/app/core/models/entity/directory';
import { DirectoryDataService } from '../../data/directory/directory.data.service';

@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  constructor(private directoriesDataService: DirectoryDataService) { }

  getDirectories(parentId?: number): Observable<Directory[]> {
    return this.directoriesDataService.getDirectories(parentId)
      .pipe(
        tap(folders => {
          folders.forEach(folder => {
            if (parentId) {
              folder.hasChildRecords = folder.hasChild;
            }
          })
        })
      );
  }
}
