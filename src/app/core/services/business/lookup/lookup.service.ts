import { Injectable } from '@angular/core';
import { LookupDataService } from '../../data/lookup/lookup.data.service';
import { Observable } from 'rxjs';
import { Lookup } from 'src/app/core/models/entity/lookup';

@Injectable({
  providedIn: 'root'
})
export class LookupService {

  constructor(private lookupDataService: LookupDataService) { }

  getConfigurationKeys(): Observable<Lookup[]> {
    return this.lookupDataService.getConfigurationKeys();
  }
}
