import { Injectable } from '@angular/core';
import * as OktaAuth from '@okta/okta-auth-js';
import { Router } from '@angular/router';
import { OktaDataService } from '../data/okta/okta.data.service';
import { environment } from 'src/environments/environment';
import { Store } from '@ngxs/store';
import { GetLoggedInUser, ShowErrorMessage } from 'src/app/state/app.actions';

const OktaTokenStorageKey = 'okta-token-storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private auth: OktaAuth = null;

  constructor(private router: Router, private oktaDataService: OktaDataService, private store: Store) { }

  async isAuthenticated() {
    if (!this.auth) {
      await this.load();
    }

    // Checks if there is a current accessToken in the TokenManger.
    let retVal = !!(await this.auth.tokenManager.get('accessToken'));
    return retVal;
  }

  async getAccessToken() {
    if (!this.auth) { return false; }
    // Checks if there is a current accessToken in the TokenManger.
    let retVal = await this.auth.tokenManager.get('accessToken');

    if (retVal) {
      return retVal.accessToken;
    } else {
      return null;
    }
  }

  async login() {
    if (!this.auth) {
      await this.load();
    }
    if (sessionStorage.getItem(OktaTokenStorageKey)) {
      sessionStorage.removeItem(OktaTokenStorageKey);
    }
    // Launches the login redirect.
    this.auth.token.getWithRedirect({
      responseType: ['id_token', 'token'],
      scopes: ['openid', 'email', 'profile']
    });
  }

  async handleAuthentication() {
    // ensure auth initialized
    if (!this.auth) {
      await this.load();
    }

    await this.auth.token.parseFromUrl()
      .then(tokens => {
        tokens.forEach(token => {
          if (token.idToken) {
            sessionStorage.setItem('id_Token', token.idToken);
            this.auth.tokenManager.add('idToken', token);
          }
          if (token.accessToken) {
            this.auth.tokenManager.add('accessToken', token);
          }
        });
      }, error => {
        console.log(error);
      });

    // redirect to page after login
    await this.isAuthenticated()
      .then(isAuthenticated => {
        if (isAuthenticated) {
          this.store.dispatch(new GetLoggedInUser()).toPromise()
            .then(() => {
              this.router.navigateByUrl('/authorize-check');
            });
        }
      });
  }

  async logout() {
    // ensure auth initialized
    if (!this.auth) {
      await this.load();
    }

    if (sessionStorage.getItem('okta-token-storage')) {
      sessionStorage.removeItem('okta-token-storage');
    }
    const idToken = sessionStorage.getItem('id_Token');
    if (idToken) {
      console.log("Gotten user's Id Token.");
      // remove okta config setting from session storage
      const host = window.location.host.toLowerCase();
      let oktaSecurityConfig = JSON.parse(sessionStorage.getItem(host));
      if (sessionStorage.getItem(host)) {
        sessionStorage.removeItem(host);
      }
      let url = `${oktaSecurityConfig.issuerUrl}/oauth2/${oktaSecurityConfig.authServerId}/v1/logout?id_token_hint=${idToken}&post_logout_redirect_uri=${window.location.protocol}//${window.location.host.toLowerCase()}/login`;
      window.location.href = url;
    } else {      
      console.log("Unable to get user's Id Token.");
    }
  }

  async load(): Promise<OktaAuth> {
    const host = window.location.host.toLowerCase(); // this includes host and port - eg localhost:4200 or bp.omv.com
    let okta_security_config: any;

    if (!sessionStorage.getItem(host)) {
      okta_security_config = {
        issuerUrl: environment.customer.issuerUrl,
        clientId: environment.customer.clientId,
        authServerId: environment.customer.authServerId
      }
      sessionStorage.setItem(host, JSON.stringify(okta_security_config));
    } else {
      okta_security_config = JSON.parse(sessionStorage.getItem(host));
    }

    this.auth = new OktaAuth({
      url: okta_security_config.issuerUrl,
      clientId: okta_security_config.clientId,
      issuer: `${okta_security_config.issuerUrl}/oauth2/${okta_security_config.authServerId}`,
      redirectUri: `${window.location.protocol}//${window.location.host.toLowerCase()}/implicit/callback`,
      tokenManager: { storage: 'sessionStorage' }
    });
  }
}
