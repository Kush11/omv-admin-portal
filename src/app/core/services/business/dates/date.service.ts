import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() { }

  toString(dateString: string): Date {
    var retVal: Date;
    try {
      retVal = moment(dateString, 'MM/DD/YYYY').toDate();
    }
    catch (ex) {
      console.log(`Unable to format date from string ${dateString} `);
    }
    return retVal;
  }

  formatToString(value: Date, format = 'MM/DD/YYYY'): string {
    var retVal: string;
    try {
      retVal = moment(value).format(format);
    }
    catch (ex) {
      console.log('Unable to format date to string ', value);
    }
    return retVal;
  }

  formatToShortdate(value: Date): string {
    var retVal: string;
    try {
      retVal = moment(value).format('MMM DD, YYYY');
    }
    catch (ex) {
      console.log('Unable to format to full date ', value);
    }
    return retVal;
  }

  formatToFullDate(value: Date): string {
    var retVal: string;
    try {
      retVal = moment(value).format('MMM DD, YYYY hh:mm a');
    }
    catch (ex) {
      console.log('Unable to format to full date ', value);
    }
    return retVal;
  }

  toUTC(date: Date) {
    return moment.utc(date).format();
  }

  isValid(date: any) {
    return moment(date, moment.ISO_8601, true).isValid();
  }

  isValidFormat(date: any, format: string) {
    return moment(date, format, true).isValid();
  }
}
