import { Injectable } from '@angular/core';

export const Monday = 'Monday';
export const Tuesday = 'Tuesday';
export const Wednesday = 'Wednesday';
export const Thursday = 'Thursday';
export const Friday = 'Friday';
export const Saturday = 'Saturday';
export const Sunday = 'Sunday';

export const Mon = 'Mon';
export const Tue = 'Tue';
export const Wed = 'Wed';
export const Thur = 'Thur';
export const Fri = 'Fri';
export const Sat = 'Sat';
export const Sun = 'Sun';

const scenarios: Scenario[] = [
  // 3 days
  { text: `${Mon} - ${Wed}`, value: [Mon, Tue, Wed] },
  { text: `${Tue} - ${Thur}`, value: [Tue, Wed, Thur] },
  { text: `${Wed} - ${Fri}`, value: [Wed, Thur, Fri] },
  { text: `${Thur} - ${Sat}`, value: [Thur, Fri, Sat] },
  { text: `${Fri} - ${Sun}`, value: [Fri, Sat, Sun] },
  { text: `${Sat} - ${Mon}`, value: [Sat, Sun, Mon] },
  { text: `${Sun} - ${Tue}`, value: [Sun, Mon, Tue] },
  // 4 days
  { text: `${Mon} - ${Thur}`, value: [Mon, Tue, Wed, Thur] },
  { text: `${Tue} - ${Fri}`, value: [Tue, Wed, Thur, Fri] },
  { text: `${Wed} - ${Sat}`, value: [Wed, Thur, Fri, Sat] },
  { text: `${Thur} - ${Sun}`, value: [Thur, Fri, Sat, Sun] },
  { text: `${Fri} - ${Mon}`, value: [Fri, Sat, Sun, Mon] },
  { text: `${Sat} - ${Tue}`, value: [Sat, Sun, Mon, Tue] },
  { text: `${Sun} - ${Wed}`, value: [Sun, Mon, Tue, Wed] },
  // 5 days
  { text: `${Mon} - ${Fri}`, value: [Mon, Tue, Wed, Thur, Fri] },
  { text: `${Tue} - ${Sat}`, value: [Tue, Wed, Thur, Fri, Sat] },
  { text: `${Wed} - ${Sun}`, value: [Wed, Thur, Fri, Sat, Sun] },
  { text: `${Thur} - ${Mon}`, value: [Thur, Fri, Sat, Sun, Mon] },
  { text: `${Fri} - ${Tue}`, value: [Fri, Sat, Sun, Mon, Tue] },
  { text: `${Sat} - ${Wed}`, value: [Sat, Sun, Mon, Tue, Wed] },
  { text: `${Sun} - ${Thur}`, value: [Sun, Mon, Tue, Wed, Thur] },
  // 6 days
  { text: `${Mon} - ${Sat}`, value: [Mon, Tue, Wed, Thur, Fri, Sat] },
  { text: `${Tue} - ${Sun}`, value: [Tue, Wed, Thur, Fri, Sat, Sun] },
  { text: `${Wed} - ${Mon}`, value: [Wed, Thur, Fri, Sat, Sun, Mon] },
  { text: `${Thur} - ${Tue}`, value: [Thur, Fri, Sat, Sun, Mon, Tue] },
  { text: `${Fri} - ${Wed}`, value: [Fri, Sat, Sun, Mon, Tue, Wed] },
  { text: `${Sat} - ${Thur}`, value: [Sat, Sun, Mon, Tue, Wed, Thur] },
  { text: `${Sun} - ${Fri}`, value: [Sun, Mon, Tue, Wed, Thur, Fri] },
  // 7 days
  { text: `${Mon} - ${Sun}`, value: [Mon, Tue, Wed, Thur, Fri, Sat, Sun] }
];

@Injectable({
  providedIn: 'root'
})
export class DaysConverter {

  constructor() { }

  toString(payload: string[]): string {
    if (!payload) return '';
    let retVal = '';
    if (payload.length > 0) {
      let days = this.shorten(payload);
      // get all scenarios that correspond to the number of days
      let daysScenarios = scenarios.filter(s => s.value.length === days.length);
      daysScenarios.forEach(scenario => {
        const match = this.compare(scenario.value, days);
        if (match) {
          retVal = scenario.text;
        }
      });
      if (!retVal) {
        days = this.shorten(payload);
        if (days.length === 2) {
          retVal = days.join(' & ');
        } else {
          retVal = days.join(', ');
        }
      }
    }
    return retVal;
  }

  shorten(payload: string[]): string[] {
    let days: string[] = [];
    payload.forEach(day => {
      switch (day.toLowerCase().trim()) {
        case Monday.toLowerCase():
          day = Mon;
          break;
        case Tuesday.toLowerCase():
          day = Tue;
          break;
        case Wednesday.toLowerCase():
          day = Wed;
          break;
        case Thursday.toLowerCase():
          day = Thur;
          break;
        case Friday.toLowerCase():
          day = Fri;
          break;
        case Saturday.toLowerCase():
          day = Sat;
          break;
        case Sunday.toLowerCase():
          day = Sun;
          break;
        default:
          return;
      }
      days = [...days, day];
    });
    return days;
  }

  private compare(firstArray: string[], secondArray: string[]): boolean {
    if (firstArray.length !== secondArray.length)
      return false;
    firstArray.sort((a, b) => (a.localeCompare(b)));
    secondArray.sort((a, b) => (a.localeCompare(b)));
    for (var i = firstArray.length; i--;) {
      if (firstArray[i] !== secondArray[i])
        return false;
    }
    return true;
  }
}

export class Scenario {
  text: string;
  value: string[]
}