import { Ticket } from 'src/app/core/models/entity/notification';
import { Observable } from 'rxjs/internal/Observable';
import { Injectable } from '@angular/core';
import { NotificationsDataService } from '../../data/notifications/notifications.data.service';
import { Ticket_PostPayloadDTO } from 'src/app/core/dtos/output/notifications/Ticket_PostPayloadDTO';
import { tap } from 'rxjs/internal/operators/tap';
import { DaysConverter } from './days-converter';
import { DateService } from '../dates/date.service';
import { map } from 'rxjs/internal/operators/map';
import { HttpEventType } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(private notificationDataService: NotificationsDataService, private daysConverter: DaysConverter,
    private dateService: DateService) { }

  getNewNotifications(): Observable<Ticket[]> {
    return this.notificationDataService.getNewNotifications();
  }

  getNotificationsHistory(): Observable<Ticket[]> {
    return this.notificationDataService.getNotificationsHistory();
  }

  getNotification(id: number): Observable<Ticket> {
    return this.notificationDataService.getNotification(id)
      .pipe(
        tap(ticket => {
          const { availabilities, startTime, endTime } = ticket;
          const days = this.daysConverter.toString(availabilities);
          let availabilityText = days;
          availabilityText += days && (startTime || endTime) ? ' / ' : '';
          availabilityText += startTime ? this.dateService.formatToString(startTime, "hh:mma") : '';
          availabilityText += startTime && endTime ? ' - ' : '';
          availabilityText += endTime ? this.dateService.formatToString(endTime, "hh:mma") : '';
          ticket.availabilityText = availabilityText;
        })
      );
  }

  getAzureReadSASToken(): Observable<string> {
    return this.notificationDataService.getAzureReadSASToken();
  }

  markIssueAsResolved(payload: Ticket_PostPayloadDTO) {
    return this.notificationDataService.markIssueAsResolved(payload);
  }

  download(name: string, url: string): Promise<boolean> {
    return this.notificationDataService.download(url)
      .pipe(
        map(data => {
          switch (data.type) {
            case HttpEventType.Response:
              const downloadedFile = new Blob([data.body], { type: data.body.type });
              const a = document.createElement('a');
              a.setAttribute('style', 'display:none;');
              document.body.appendChild(a);
              a.download = name;
              a.href = URL.createObjectURL(downloadedFile);
              a.target = '_blank';
              a.click();
              document.body.removeChild(a);
              break;
          }
          return true;
        })
      ).toPromise();
  }
}
