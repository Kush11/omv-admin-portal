import { ErrorMessage } from './../../../models/entity/error-message';

import { ConfigurationSetting } from './../../../models/entity/configuration';
import { Injectable } from '@angular/core';
import { CustomersDataService } from '../../data/customers/customers.data.service';
import { Company } from 'src/app/core/models/entity/company';
import { Observable } from 'rxjs/internal/Observable';
import { tap } from 'rxjs/internal/operators/tap';
import { Document } from '../../../models/entity/document';
import { Permission } from 'src/app/core/models/entity/permissions';
import { SecurityProvider } from 'src/app/core/models/entity/security-provider';
import { LiveStreamCamera } from 'src/app/core/models/entity/camera';
import { Lookup } from 'src/app/core/models/entity/lookup';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private customersDataService: CustomersDataService) { }

  //#region Customers

  getCustomers(): Observable<Company[]> {
    return this.customersDataService.getCustomers()
      .pipe(
        tap(customers => {
          customers.forEach(customer => {
            customer.routeLink = `/companies/${customer.id}`;
          })
        })
      );
  }

  getCustomer(id: number): Observable<Company> {
    return this.customersDataService.getCustomer(id);
  }

  createCustomer(customer: Company) {
    return this.customersDataService.createCustomer(customer);
  }

  updateCustomer(id: number, customer: Company) {
    return this.customersDataService.updateCustomer(id, customer);
  }

  getAzureWriteSASToken(companyId: number): Observable<string> {
    return this.customersDataService.getAzureWriteSASToken(companyId);
  }

  getAzureReadSASToken(companyId: number): Observable<string> {
    return this.customersDataService.getAzureReadSASToken(companyId);
  }

  deleteCustomer(id: number) {
    return this.customersDataService.deleteCustomer(id);
  }

  //#endregion

  //#region Configuration Settings

  getConfigurationSettings(id: number): Observable<ConfigurationSetting[]> {
    return this.customersDataService.getConfigurationSettings(id);
  }

  createConfigurationSetting(customerId: number, configuration: ConfigurationSetting) {
    return this.customersDataService.createConfigurationSetting(customerId, configuration);
  }

  updateConfigurationSetting(customerId: number, configuration: ConfigurationSetting) {
    return this.customersDataService.updateConfigurationSetting(customerId, configuration);
  }

  deleteConfigurationSetting(customerId: number, key: string) {
    return this.customersDataService.deleteConfigurationSetting(customerId, key);
  }

  //#endregion

  //#region Security Providers

  getCompanySettings(id: number): Observable<SecurityProvider[]> {
    return this.customersDataService.getSecurityProviders(id);
  }

  createCompanySettings(customerId: number, settings: SecurityProvider) {
    return this.customersDataService.createSecurityProvider(customerId, settings);
  }

  updateCompanySettings(id: number, securityProvider: SecurityProvider) {
    return this.customersDataService.updateSecurityProvider(id, securityProvider);
  }

  //#endregion

  //#region Permissions

  getPermissions(customerId: number): Observable<Permission> {
    return this.customersDataService.getPermissions(customerId);
  }

  createPermissions(customerId: number, permissions: Permission[]) {
    return this.customersDataService.createPermission(customerId, permissions);
  }

  updatePermissions(customerId: number, permission: Permission[]) {
    return this.customersDataService.updatePermissions(customerId, permission);
  }

  //#endregion

  //#region Live Stream Cameras

  getLiveStreamCameras(customerId: number): Observable<LiveStreamCamera[]> {
    return this.customersDataService.getLiveStreamCameras(customerId);
  }

  createLiveStreamCamera(companyId: number, camera: LiveStreamCamera) {
    return this.customersDataService.createLiveStreamCamera(companyId, camera);
  }

  updateLiveStreamCamera(companyId: number, camera: LiveStreamCamera) {
    return this.customersDataService.updateLiveStreamCamera(companyId, camera);
  }

  deleteLiveStreamCamera(companyId: number, cameraId: number) {
    return this.customersDataService.deleteLiveStreamCamera(companyId, cameraId);
  }

  //#endregion

  //#region Lookups

  getTypes(id: number): Observable<string[]> {
    return this.customersDataService.getTypes(id);
  }

  getLiveStreamAssets(id: number): Observable<Lookup[]> {
    return this.customersDataService.getLiveStreamAssets(id);
  }

  getLiveStreamGroups(id: number): Observable<Lookup[]> {
    return this.customersDataService.getLiveStreamGroups(id);
  }

  getLookups(id: number): Observable<Lookup[]> {
    return this.customersDataService.getLookups(id);
  }

  createLookup(id: number, lookup: Lookup) {
    return this.customersDataService.createLookup(id, lookup);
  }

  updateLookup(id: number, lookup: Lookup) {
    return this.customersDataService.updateLookup(id, lookup);
  }

  deleteLookup(id: number, lookup: Lookup) {
    return this.customersDataService.deleteLookup(id, lookup);
  }

  //#endregion

  //#region Directories

  getDirectories(id?: number, parentId?: number) {
    return this.customersDataService.getDirectories(id, parentId)
      .pipe(
        tap(folders => {
          folders.forEach(folder => {
            if (parentId) {
              folder.hasChildRecords = folder.hasChild;
            }
          })
        })
      );
  }

  //#endregion

  migrateDatabase(id: number) {
    return this.customersDataService.migrateDatabase(id);
  }

  getErrorMessages(): Observable<ErrorMessage[]> {
    return this.customersDataService.getErrorMessages();
  }
}
