export class BlobUploadParams {
    sas: string;
    storageAccount: string;
    containerName: string;
  }