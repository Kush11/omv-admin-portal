
export class Lookup {
    type: string;
    value: any;
    description: string;
    sort: string;
    status: number;
}