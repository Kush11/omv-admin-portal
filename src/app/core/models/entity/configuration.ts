
export class ConfigurationSetting {
  key: string;
  value: string;
  type: string;
  customerId: number;
  isSystem: boolean;
}
