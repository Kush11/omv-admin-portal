export class Tab {
    link: any;
    query?: string;
    name: string;
    isActive?: boolean;
    disabled?: boolean;
  }
