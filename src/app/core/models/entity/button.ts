export class Button {  
  type? = 'button';
  cssClass? = 'button-default';
  disabled?: boolean;
  hidden?: boolean;
  text: string;
  override?: boolean;
  left?: number;
  right?: number;
  query?: any;
  action?: EventListener;
}