import { BaseModel } from '../base-model';

export class Media extends BaseModel {
  id: number;
  logo: string;
  name: string;
  destination: string;
  date: string;
  size: string;
}
