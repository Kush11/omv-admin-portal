import { BaseModel } from '../base-model';

export class Ticket extends BaseModel {
  id: number;
  companyName: string;
  reportedBy: string;
  email: string;
  isFollowUp: boolean;
  subject: string;
  date: string;
  startTime: Date;
  endTime: Date;
  attachmentCount: number;
  region: string;
  availabilities: string[];
  availabilityText: string;
  description: string;
  comment: string;
  isResolved: boolean;
  dateResolved: Date;
  resolvedBy: string;
  attachmentUrls: Attachment[];
}

export class Attachment {
  documentId: string;
  storageType: string;
  entityType: string;
  entityId: string;
  documentTypeCode: string;
  mediaType: number;
  documentName: string;
  documentUrl: string;
  metadata: string;
  contentType: string;
  size: number;
  containerId: string;
  thumbnailContainerUrl: string;
  isDeleted: boolean;
  status: number;
  createdOn: Date;
  createdBy: string;
  modifiedOn: Date;
  modifiedBy: string;
}
