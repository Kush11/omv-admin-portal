export class Permission {
  menuName: string;
  isPermitted: boolean;
  customerId?: number;
}
