export class ErrorMessage {
    timestamp: string;
    message: string;
}
