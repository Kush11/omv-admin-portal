import { BaseModel } from '../base-model';

export class Company extends BaseModel {
  id?: number;
  name?: string;
  companyType?: string;
  contractType?: string;
  companyTitle?: string;
  pointsOfContact?: string[];
  imageUrl?: string;
  text?: string;
  files?: number;
  activeHours?: number;
  activeUsers?: number;
  hostHeader?: string;
  connectionString?: string;
  get pointOfContact(): string {
    return this.pointsOfContact ? this.pointsOfContact[0] : 'N/A';
  }
}
