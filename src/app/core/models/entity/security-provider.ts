export class SecurityProvider {
  id: number;
  companyId: number;
  issuerUrl: string;
  clientId: string;
  authServerId: string;
  authType: number;
}
