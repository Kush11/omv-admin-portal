export class ToolBar {
  name?: string;
  text: string;
  disabled?: boolean;
  cssClass?: string;
  icon?: string;
  iconCssClass?: string;
  toggle?: boolean;
  visible?: boolean;
  action?: EventListener;
}
