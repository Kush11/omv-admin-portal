export class User {
    userId: number;
    userName: string;
    emailAddress: string;
    firstName: string;
    lastName: string;
    displayName: string;
    roleNames: string;
    groups?: string;
    groupsDisplay?: string;
    status: number;
    statusName?: string;
    createdOn?: Date;
    createdBy?: string;
    modifiedOn?: Date;
    modifiedOnString?: string;
    modifiedBy?: string;
}
