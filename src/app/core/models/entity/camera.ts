export class LiveStreamCamera {
  id: number;
  name: string;
  source: string;
  thumbNailUrl: string;
  asset: number;
  assetName: string;
  group: number;
  groupName: string;
  directoryId: number;
  directoryPath: string;
}
