export class Directory {
  id: number;
  name: string;
  parentId: number | null;
  hasChild?: boolean;
  hasChildRecords?: boolean;
  icon?: string;
  isParent?: boolean;
}