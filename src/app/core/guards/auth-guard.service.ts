import { AuthService } from './../services/business/auth.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  authenticated: Promise<boolean>;

  constructor(private auth: AuthService, private router: Router) {
    this.authenticated = auth.isAuthenticated();
  }

  async canActivate() {
    const authenticated = await this.authenticated;
    if (authenticated) {
      return true;
    }
    // Redirect to login flow.
    const route = this.router.url;
    this.saveReturnUrl(route);
    this.auth.login();
    return false;
  }

  private saveReturnUrl(route) {
    const ignored_routes = ['/startup', '/dashboard', '/authorize-check', '/implicit/callback', 'login'];
    if (ignored_routes.includes(route)) { return; }
    const key = 'return_url';
    if (sessionStorage.getItem(key)) { sessionStorage.removeItem(key); }
    sessionStorage.setItem(key, route);
  }
}
