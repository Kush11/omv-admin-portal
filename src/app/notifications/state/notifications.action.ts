import { Ticket_PostPayloadDTO } from 'src/app/core/dtos/output/notifications/Ticket_PostPayloadDTO';

export class GetNewNotifications {
  static readonly type = '[Notifications] GetNewNotifications';
}


export class GetNotificationsHistory {
  static readonly type = '[Notifications] GetNotificationsHistory';
}

export class GetNotification {
  static readonly type = '[Notifications] GetNotification'

  constructor(public id: number) { }
}

export class ClearCurrentNotification {
  static readonly type = '[Notifications] ClearCurrentNotification'
}

export class MarkIssueAsResolved {
  static readonly type = '[Notifications] MarkIssueAsResolved';

  constructor(public payload: Ticket_PostPayloadDTO) { }
}

export class GetAzureReadSASToken {
  static readonly type = '[Notifications] GetAzureReadSASToken';

  constructor() { }
}
export class DownloadItem {
  static readonly type = '[Notifications] DownloadItem';

  constructor(public name: string, public url: string) { }
}
