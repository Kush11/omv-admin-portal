import { Ticket } from './../../core/models/entity/notification';
import { GetNewNotifications, GetNotification, GetNotificationsHistory, MarkIssueAsResolved, ClearCurrentNotification, GetAzureReadSASToken, DownloadItem } from './notifications.action';
import { State, Selector, Action, StateContext } from '@ngxs/store';
import { NotificationsService } from 'src/app/core/services/business/notifications/notifications.service';
import { tap } from 'rxjs/internal/operators/tap';
import { HideSpinner, ShowSpinner, ShowSuccessMessage, ShowErrorMessage } from 'src/app/state/app.actions';

export class NotificationsStateModel {
  newNotifications: Ticket[];
  currentNotification: Ticket;
  currentNotificationId: number;
  notificationsHistory: Ticket[];
  azureReadSASToken: string;
}
@State<NotificationsStateModel>({
  name: 'notifications',
  defaults: {
    newNotifications: [],
    currentNotification: null,
    currentNotificationId: null,
    notificationsHistory: [],
    azureReadSASToken: null,
  }
})
export class NotificationsState {

  @Selector()
  static getNewNotifications(state: NotificationsStateModel) {
    return state.newNotifications;
  }



  @Selector()
  static getNotificationsHistory(state: NotificationsStateModel) {
    return state.notificationsHistory;
  }

  @Selector()
  static getCurrentNotification(state: NotificationsStateModel) {
    return state.currentNotification;
  }

  @Selector()
  static getAzureReadSASToken(state: NotificationsStateModel) {
    return state.azureReadSASToken;
  }

  @Selector()
  static getCurrentNotificationId(state: NotificationsStateModel) {
    return state.currentNotificationId;
  }

  constructor(private notificationService: NotificationsService) { }

  @Action(GetNewNotifications)
  getNotifications(ctx: StateContext<NotificationsStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.notificationService.getNewNotifications()
      .pipe(
        tap(notifications => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            newNotifications: notifications
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }



  @Action(GetNotificationsHistory)
  getNotificationsHistory(ctx: StateContext<NotificationsStateModel>) {
    ctx.dispatch(new ShowSpinner());
    return this.notificationService.getNotificationsHistory()
      .pipe(
        tap(notificationsHist => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            notificationsHistory: notificationsHist
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(GetNotification)
  getCurrentNotification(ctx: StateContext<NotificationsStateModel>, { id }: GetNotification) {
    ctx.dispatch(new ShowSpinner());
    return this.notificationService.getNotification(id)
      .pipe(
        tap(notification => {
          const state = ctx.getState();
          notification.attachmentUrls.forEach(imageUrl => {
            imageUrl.documentUrl += state.azureReadSASToken;
          });
          ctx.setState({
            ...state,
            currentNotification: notification
          });
          ctx.dispatch(new HideSpinner());
        }, err => ctx.dispatch(new ShowErrorMessage(err.error)))
      );
  }

  @Action(GetAzureReadSASToken)
  getAzureReadSASToken(ctx: StateContext<NotificationsStateModel>) {
    return this.notificationService.getAzureReadSASToken()
      .pipe(
        tap(response => {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            azureReadSASToken: response
          });
        }, error => ctx.dispatch(new ShowErrorMessage(error.error)))
      );
  }

  @Action(ClearCurrentNotification)
  clearCurrentNotification(ctx: StateContext<NotificationsStateModel>) {
    const state = ctx.getState();
    const notification = { ...state.currentNotification };
    ctx.setState({
      ...state,
      currentNotification: null
    });
  }

  @Action(MarkIssueAsResolved)
  markIssueAsResolved(ctx: StateContext<NotificationsStateModel>, { payload }: MarkIssueAsResolved) {
    ctx.dispatch(new ShowSpinner());
    return this.notificationService.markIssueAsResolved(payload)
      .pipe(
        tap(() => {
          if (payload.isResolved) {
          ctx.dispatch(new ShowSuccessMessage(`Notification has been successfully marked as resolved.`));
          ctx.dispatch(new GetNewNotifications());
          } else {
          ctx.dispatch(new ShowSuccessMessage(`Notification comment saved successfully.`));
          }
          ctx.dispatch(new HideSpinner());
        }, error => ctx.dispatch(new ShowErrorMessage(error.error)))
      );
  }

  @Action(DownloadItem)
  downloadItem(ctx: StateContext<NotificationsStateModel>, { name, url }: DownloadItem) {
    ctx.dispatch(new ShowSuccessMessage('Item is Downloading'));
    this.notificationService.download(name, url)
      .then(() => {
        ctx.dispatch(new HideSpinner());
      }, error => ctx.dispatch( new ShowErrorMessage(error.error)));
  }
}
