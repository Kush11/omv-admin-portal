import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { NotificationsState } from '../../state/notifications.state';
import { Select, Store } from '@ngxs/store';
import { GridColumn } from 'src/app/core/models/entity/grid.column';
import { GetNewNotifications } from '../../state/notifications.action';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Router } from '@angular/router';
import { SetPreviousRoute } from 'src/app/state/app.actions';
import { Ticket } from 'src/app/core/models/entity/notification';
import { SubSink } from 'subsink/dist/subsink';

@Component({
  selector: 'app-notifications-new',
  templateUrl: './notifications-new.component.html',
  styleUrls: ['./notifications-new.component.css']
})
export class NotificationsNewComponent extends BaseComponent implements OnInit {

  @Select(NotificationsState.getNewNotifications) notifications$: Observable<Ticket[]>;

  columns: GridColumn[] = [
    { headerText: 'Company', field: 'companyName', useAsLink: true, filterType: 'checkbox', action: this.viewNotification.bind(this), width: 150 },
    { headerText: 'Reported By', field: 'reportedBy', filterType: 'checkbox', width: 150, useAsLink: true, action: this.viewNotification.bind(this), },
    { headerText: 'Subject', field: 'subject', filterType: 'checkbox', width: 150, useAsLink: true, action: this.viewNotification.bind(this), },
    { headerText: 'Date', field: 'date', ignoreFiltering: true, type: 'date', format: 'MMM dd, yyyy  hh:mm aa', width: 150, useAsLink: true, action: this.viewNotification.bind(this), },
    { headerText: '#', field: 'attachmentCount', ignoreFiltering: true, width: 60, useAsLink: true, action: this.viewNotification.bind(this) },
  ];
  newNotification: boolean;
  private subs = new SubSink();


  constructor(protected store: Store, private router: Router) {
    super(store);
    this.setPageTitle('Notifications - New Issues');
  }

  ngOnInit() {
    this.store.dispatch(new GetNewNotifications());
  }

  viewNotification(notification: Ticket) {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    this.router.navigate([`notifications/${notification.id}/details`]);
  }
}
