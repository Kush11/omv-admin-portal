import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Select, Store } from '@ngxs/store';
import { NotificationsState } from '../../state/notifications.state';
import { GridColumn } from 'src/app/core/models/entity/grid.column';
import { Ticket } from 'src/app/core/models/entity/notification';
import { BaseComponent } from 'src/app/shared/base/base.component';
import { Router } from '@angular/router';
import { SetPreviousRoute } from 'src/app/state/app.actions';
import { GetNotificationsHistory } from '../../state/notifications.action';

@Component({
  selector: 'app-notifications-history',
  templateUrl: './notifications-history.component.html',
  styleUrls: ['./notifications-history.component.css']
})
export class NotificationsHistoryComponent extends BaseComponent implements OnInit {

  @Select(NotificationsState.getNotificationsHistory) notifications$: Observable<Ticket[]>;

  columns: GridColumn[] = [
    { headerText: 'Company', field: 'companyName', useAsLink: true, filterType: 'checkbox', action: this.viewNotification.bind(this), width: 150 },
    { headerText: 'Reported By', field: 'reportedBy', filterType: 'checkbox', width: 150, useAsLink: true, action: this.viewNotification.bind(this), },
    { headerText: 'Subject', field: 'subject', filterType: 'checkbox', width: 150, useAsLink: true, action: this.viewNotification.bind(this), },
    { headerText: 'Date', field: 'date', ignoreFiltering: true, type: 'date', format: 'MMM dd, yyyy  hh:mm aa', width: 150, useAsLink: true, action: this.viewNotification.bind(this), },
    { headerText: '#', field: 'attachmentCount', ignoreFiltering: true, width: 60, useAsLink: true, action: this.viewNotification.bind(this) },
  ];

  constructor(protected store: Store, private router: Router) {
    super(store);
    this.setPageTitle('Notifications - Issues History');
  }

  ngOnInit() {
    this.store.dispatch(new GetNotificationsHistory());
  }

  viewNotification(notification: Ticket) {
    this.store.dispatch(new SetPreviousRoute(this.router.url));
    this.router.navigate([`notifications/${notification.id}/details`]);
  }
}
