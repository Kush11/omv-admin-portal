import { Component, OnInit } from '@angular/core';
import { Tab } from 'src/app/core/models/entity/tab';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications-list',
  templateUrl: './notifications-list.component.html',
  styleUrls: ['./notifications-list.component.css']
})
export class NotificationsListComponent implements OnInit {

  tabs: Tab[] = [
    { link: '/notifications/new', name: 'New Issues', isActive: true },
    { link: '/notifications/history', name: 'Issues History' }
  ];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  switchTabs(tabLink: string) {
    this.router.navigate([tabLink]);
  }
}
