import { AppState } from '../../state/app.state';
import { NotificationsState } from '../state/notifications.state';
import { Observable } from 'rxjs/internal/Observable';
import { GetNotification, MarkIssueAsResolved, GetAzureReadSASToken, DownloadItem } from '../state/notifications.action';
import { Ticket } from '../../core/models/entity/notification';
import { SubSink } from 'subsink/dist/subsink';
import { Store, Select } from '@ngxs/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Ticket_PostPayloadDTO } from 'src/app/core/dtos/output/notifications/Ticket_PostPayloadDTO';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { Button } from 'src/app/core/models/entity/button';
import { DaysConverter } from 'src/app/core/services/business/notifications/days-converter';
import { CompaniesState } from 'src/app/companies/state/companies.state';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.css']
})
export class NotificationDetailComponent implements OnInit, OnDestroy {

  @ViewChild('confirmModal', {static: false}) confirmModal: ModalComponent;

  private subs = new SubSink();
  verifyModalText: string;
  notification: Ticket;
  notificationId: number;
  previousRoute: string;
  comment = '';
  readSASToken: string;

  modalPosition: { X: 'center', Y: 'center' };
  buttons: Button[] = [
    { text: 'SAVE COMMENT', type: 'submit', cssClass: 'button-round modal-save-changes-qa', action: this.onSaveComment.bind(this) },
    { text: 'Marked as Resolved', type: 'button', cssClass: 'button-check modal-cancel-changes-qa', action: this.onResolved.bind(this) },
  ];

  @Select(NotificationsState.getCurrentNotification) notification$: Observable<Ticket>;
  @Select(AppState.getPreviousRoute) previousRoute$: Observable<string>;


  constructor(private actvatedRoute: ActivatedRoute, private store: Store, private router: Router) {
    this.verifyModalText = '';
  }

  ngOnInit() {
    this.store.dispatch(new GetAzureReadSASToken());
    this.subs.add(
      this.actvatedRoute.paramMap.subscribe(
        params => {
          this.notificationId = Number(params.get('id'));
          if (this.notificationId) {
            this.store.dispatch(new GetNotification(this.notificationId));
            this.notification$.subscribe(notification => {
              this.notification = notification;
            });
          }
        }
      ),
      this.notification$
        .subscribe(notification => {
          this.notification = notification;
        }),
      this.previousRoute$.subscribe(route => {
        const default_route = '/notifications';
        if (route.includes(default_route)) {
          this.previousRoute = route;
        } else {
          this.previousRoute = default_route
        }
      })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onBack(route: string) {
    this.router.navigate([route]);
  }

  onResolved() {
    this.verifyModalText = 'Are you sure you want to mark this item as resolved?';
    this.confirmModal.show();
  }

  addComment(comment: string) {
    this.comment = comment;
  }

  markAsResolved() {
    const payload = new Ticket_PostPayloadDTO();
    payload.comment = this.comment;
    payload.ticketId = this.notificationId.toString();
    payload.isResolved = true;
    this.store.dispatch(new MarkIssueAsResolved(payload));
    this.router.navigate([`notifications`]);
  }

  onSaveComment() {
    const payload = new Ticket_PostPayloadDTO();
    payload.comment = this.comment;
    payload.ticketId = this.notificationId.toString();
    payload.isResolved = false;
    this.store.dispatch(new MarkIssueAsResolved(payload));
    }

    downloadAttachment(attachment) {
      this.store.dispatch(new DownloadItem(attachment.documentName, attachment.documentUrl));
    }
}
