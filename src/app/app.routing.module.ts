import { NotificationDetailComponent } from './notifications/notification-details/notification-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnAuthorizedComponent } from './unauthorized/unauthorized.component';
import { AuthorizationCheckComponent } from './authorization-check/authorization-check.component';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { AuthGuard } from './core/guards/auth-guard.service';
import { AuthService } from './core/services/business/auth.service';
import { CompanyProfilesComponent } from './companies/company-profiles/company-profiles.component';
import { CompanyComponent } from './companies/company/company.component';
import { CompanyConfigurationsComponent } from './companies/company/company-configurations/company-configurations.component';
import { CompanyLiveStreamComponent } from './companies/company/company-live-stream/company-live-stream.component';
import { NotificationsListComponent } from './notifications/notifications-list/notifications-list.component';
import { NotificationsNewComponent } from './notifications/notifications-list/notifications-new/notifications-new.component';
import { NotificationsHistoryComponent } from './notifications/notifications-list/notifications-history/notifications-history.component';
import { CompanySecurityProvidersComponent } from './companies/company/company-security-providers/company-security-providers.component';
import { CompanyLookupsComponent } from './companies/company/company-lookups/company-lookups.component';
import { CompanyErrorsComponent } from './companies/company/company-errors/company-errors.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'companies',
    pathMatch: 'full'
  },
  {
    path: 'unauthorize',
    component: UnAuthorizedComponent
  },
  {
    path: 'authorize-check',
    component: AuthorizationCheckComponent
  },
  {
    path: 'implicit/callback',
    component: AuthCallbackComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'companies',
    component: CompanyProfilesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'companies/:id',
    component: CompanyComponent,
    children: [
      { path: '', redirectTo: 'configurations', pathMatch: 'full' },
      { path: 'configurations', component: CompanyConfigurationsComponent, canActivate: [AuthGuard] },
      { path: 'live-stream', component: CompanyLiveStreamComponent, canActivate: [AuthGuard] },
      { path: 'security-providers', component: CompanySecurityProvidersComponent, canActivate: [AuthGuard] },
      { path: 'lookups', component: CompanyLookupsComponent, canActivate: [AuthGuard] },
      { path: 'errors', component: CompanyErrorsComponent, canActivate: [AuthGuard] }
    ],
  },
  {
    path: 'notifications',
    component: NotificationsListComponent,
    children: [
      { path: '', redirectTo: 'new', pathMatch: 'full' },
      { path: 'new', component: NotificationsNewComponent, canActivate: [AuthGuard] },
      { path: 'history', component: NotificationsHistoryComponent, canActivate: [AuthGuard] }
    ]
  },
  {
    path: 'notifications/:id/details',
    component: NotificationDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'page-not-found',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard, AuthService]
})
export class AppRoutingModule { }
