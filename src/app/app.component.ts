import { Component, ViewChild } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Title } from '@angular/platform-browser';
import { AuthService } from './core/services/business/auth.service';
import { AppState } from './state/app.state';
import { Observable } from 'rxjs/internal/Observable';
import { SubSink } from 'subsink/dist/subsink';
import { Toast, ToastType } from './core/enum/toast';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';
import { createSpinner, showSpinner, hideSpinner, setSpinner } from '@syncfusion/ej2-popups/src/spinner/spinner';
import { NotificationsState } from './notifications/state/notifications.state';
import { Ticket } from './core/models/entity/notification';
import { GetNewNotifications } from './notifications/state/notifications.action';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @Select(AppState.getPageTitle) currentPageTitle$: Observable<string>;
  @Select(AppState.getSpinnerVisibility) showSpinner$: Observable<boolean>;
  @Select(AppState.getToastMessage) toastMessage$: Observable<Toast>;
  @Select(NotificationsState.getNewNotifications) notifications$: Observable<Ticket[]>;



  @ViewChild('toast' , {static: false}) toastObj: ToastComponent;

  toastPosition = { X: 'Right', Y: 'Top' };
  private subs = new SubSink();
  displayWidth: number;

  constructor(public auth: AuthService, private title: Title, private store: Store) {
  }

  ngOnInit() {
    setInterval(() => {
      this.getNewNotifications();
    }, 10000);

    this.subs.add(
      this.showSpinner$
        .subscribe(showSpinner => {
          if (showSpinner) this.showSpinner();
          else this.hideSpinner();
        }),
      this.toastMessage$
        .subscribe(toastResponse => {
          if (!toastResponse) return;
          let toast: any;
          if (typeof (toastResponse.action) != 'undefined') {
            toast = { title: 'Information!', content: toastResponse.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' };
            this.toastObj.hide();
          }
          if (!toastResponse.message) return;
          if (typeof toastResponse.message !== 'string') return;
          switch (toastResponse.type) {
            case ToastType.Success:
              toast = { title: 'Success!', content: toastResponse.message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' };
              this.toastObj.show(toast);
              break;
            case ToastType.Warning:
              toast = { title: 'Warning!', content: toastResponse.message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' };
              this.toastObj.show(toast);
              break;
            case ToastType.Error:
              toast = { title: 'Error!', content: toastResponse.message, cssClass: 'e-toast-danger', icon: 'e-danger toast-icons' };
              this.toastObj.show(toast);
              break;
            case ToastType.Info:
              toast = { title: 'Infomation!', content: toastResponse.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' };
              this.toastObj.timeOut = 0;
              this.toastObj.show(toast);
              break;
          }
        }),
      this.currentPageTitle$
        .subscribe((res) => res === 'OMV Admin Portal' ? this.title.setTitle(res) : this.title.setTitle(res + ' - OMV Admin Portal'))
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  showSpinner() {
    createSpinner({
      // Specify the target for the spinner to show
      target: document.getElementById('spinnerContainer')
    });
    showSpinner(document.getElementById('spinnerContainer'));
    // setSpinner({ type: 'Bootstrap' });
  }

  hideSpinner() {
    createSpinner({
      // Specify the target for the spinner to show
      target: document.getElementById('spinnerContainer')
    });
    hideSpinner(document.getElementById('spinnerContainer'));
  }
  getNewNotifications() {
    this.store.dispatch(new GetNewNotifications());
  }

}
