import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnInit {
  @Input() totalRecordsCount: number;
  @Input() pageCount: number;
  @Input() pageSize: number;
  @Input() currentPage = 1;

  @Output() change = new  EventEmitter<number>();

  constructor() { }

  ngOnInit() { }

  changeEvent(event){
    if (event.currentPage) this.change.emit(event.currentPage); 
    return event;    
  }
}
