import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { Company } from 'src/app/core/models/entity/company';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-detail-card',
  templateUrl: './detail-card.component.html',
  styleUrls: ['./detail-card.component.css']
})
export class DetailCardComponent implements OnInit {

  @ViewChild('uploadModal', {static: false}) uploadModal: ModalComponent;

  @Input() isNew: boolean;
  @Input() company: Company;

  @Output() edit = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  @Output() create = new EventEmitter<any>();
  @Output() view = new EventEmitter<any>();
  @Output() impersonate = new EventEmitter<any>();
  @Output() checkCanUpload = new EventEmitter<any>();
  @Output() changeLogo = new EventEmitter<File>();

  constructor() {
  }

  ngOnInit() { }

  onEdit(company: Company) {
    this.edit.emit(company);
  }

  onDelete(args: Company) {
    this.delete.emit(args);
  }

  onCreate(args: Company) {
    this.create.emit(args);
  }

  onView(args: Company) {
    this.view.emit(args);
  }

  onImpersonate(company: Company) {
    this.impersonate.emit(company);
  }

  onChangeLogo() {
  }

  //#region Upload 

  onCheckCanUplaod() {
    this.checkCanUpload.emit();
  }

  showUploadModal() {
    this.uploadModal.show();
  }

  onLogoSelected(logo: File) {
    this.changeLogo.emit(logo);
  }

  //#endregion
}
