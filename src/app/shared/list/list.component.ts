import { GridColumn } from '../../core/models/entity/grid.column';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import {
  GridComponent, RowSelectEventArgs, SelectionSettingsModel, RowDeselectEventArgs, QueryCellInfoEventArgs, parentsUntil,
  ExcelExportProperties, VirtualScrollService, FilterService, RowDDService, SelectionService, ToolbarItems
} from '@syncfusion/ej2-angular-grids';
import { Store } from '@ngxs/store';
import { Request_Status } from 'src/app/core/enum/request-status';
import { ToolBar } from 'src/app/core/models/entity/toolbar-action';
import { Button } from 'src/app/core/models/entity/button';
import { Router, ActivatedRoute } from '@angular/router';
import { RowDragEventArgs } from '@syncfusion/ej2-grids';
import { Column } from '@syncfusion/ej2-grids/src/grid/models/column';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css', '../../app.component.css'],
  providers: [FilterService, VirtualScrollService, RowDDService, SelectionService]
})
export class ListComponent extends BaseComponent implements OnInit {

  selectedRecords = [];

  @Input() dataSource = [];
  @Input() initialRecords = [];
  @Input() columns: GridColumn[];
  @Input() checkField = 'id';
  @Input() showFavoriteIcon: boolean;
  @Input() showStatusIcons: boolean;
  @Input() statusIconPosition = 0;
  @Input() requestStatusEnum = Request_Status;
  @Input() isfirstButtonDisabled: boolean;
  @Input() allowCheckboxSelection: boolean;
  @Input() allowEditing: boolean;
  @Input() allowDownloading: boolean;
  @Input() allowDeleting: boolean;
  @Input() deleteText = 'Remove';
  @Input() allowFavoriting: boolean;
  @Input() favoriteIconPosition = 1;
  @Input() rowLinkIndex: number;
  @Input() allowActivating: boolean;
  @Input() allowTooltip: boolean;
  @Input() showDetail: boolean;
  @Input() allowFiltering: boolean;
  @Input() allowRowDragAndDrop: boolean;
  @Input() allowSearch: boolean;

  //Toolbar
  @Input() showToolbar: boolean;
  @Input() showTotal: boolean;
  @Input() totalText = 'Total';
  @Input() toolbarSelectionText = 'Selected';
  @Input() toolbar: ToolBar[];
  @Input() exportText: string;
  @Input() allowExport: boolean;
  @Output() toolbarClick = new EventEmitter<any>();

  // Footer
  @Input() footer: Button[];
  @Input() footerPosition = 'right';
  @Output() footerActions = new EventEmitter<any>();

  //pager
  @Input() allowGridPaging: boolean;
  @Input() allowPaging: boolean;
  @Input() autoPagination = true;
  @Input() pageCount = 5;
  @Input() pageSize: number;
  @Input() totalRecordsCount: number;
  @Input() currentPage: number;
  @Output() changePage = new EventEmitter<number>();

  //Modifications
  @Output() edit = new EventEmitter<any>();
  @Output() export = new EventEmitter<any>();
  @Output() download = new EventEmitter<any>();
  @Output() customAction = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  @Output() activate = new EventEmitter<any>();
  @Output() advance = new EventEmitter<any>();
  @Output() toggleFavorite = new EventEmitter<any[]>();
  @Output() rowSelected = new EventEmitter<any[]>();
  @Output() rowDeselected = new EventEmitter<any[]>();
  @Output() allRowsSelected = new EventEmitter<any>();
  @Output() allRowsDeselected = new EventEmitter<any>();
  @Output() rowDrop = new EventEmitter<any>();

  @Output() selectedItemData = new EventEmitter<any>();

  @ViewChild('grid', {static: false}) grid: GridComponent;
  gridData: any[];
  selectionOptions: SelectionSettingsModel;
  selIndex: any[] = [];
  customAttributes: object;
  selectionSettings: Object;
  filterSettings: Object;
  filter: Object;
  pageSettings: Object;
  toolbarOptions: ToolbarItems[];
  isDataChanging: boolean;
  isAllSelected: boolean;

  constructor(protected store: Store, protected router?: Router, protected route?: ActivatedRoute) {
    super(store);
  }

  ngOnInit() {
    this.filterSettings = { type: "Menu" };
    this.filter = { type: "CheckBox" };
    this.pageSettings = { pageSizes: true, pageCount: 5 };
    if (this.allowSearch) this.toolbarOptions = ['Search'];
    this.selectionOptions = { checkboxOnly: true };
    this.selectionSettings = { checkboxOnly: true };
    this.totalRecordsCount = this.totalRecordsCount ? this.totalRecordsCount : (this.dataSource ? this.dataSource.length : 0);
  }

  rowSelectedGridEvent(args: RowSelectEventArgs) {
    this.selectedRecords = this.grid.getSelectedRecords();
    const rowIndexes = args['rowIndexes'];
    const { data } = args;
    // console.log("rowSelectedGridEvent: ", this.selectedRecords);
    if (this.allowGridPaging) {
      const pageSize = this.grid.pageSettings.pageSize;
      const currentPage = this.grid.pageSettings.currentPage;
      const totalRecordsCount = this.grid.pageSettings.totalRecordsCount;
      const currentPageRowsCount = (totalRecordsCount - (pageSize * (currentPage - 1)) >= pageSize) ? pageSize : totalRecordsCount - (pageSize * (currentPage - 1));
      if (this.selectedRecords.length === currentPageRowsCount) {
        this.rowSelected.emit(this.selectedRecords);
      } else {
        this.rowSelected.emit([data]);
      }
      console.log("rowSelectedGridEvent All pageSettings: ", currentPageRowsCount, this.selectedRecords);
    } else {
      if (this.selectedRecords.length === this.dataSource.length) {
        this.isAllSelected = true;
        this.rowSelected.emit(this.selectedRecords);
      } else {
        this.rowSelected.emit([data]);
      }
    }
  }

  rowDeselectedGridEvent(args: RowDeselectEventArgs) {
    console.log("rowDeselectedGridEvent: ", args);
    this.selectedRecords = this.grid.getSelectedRecords();
    const { data } = args;
    if (!this.isDataChanging) {
      if (this.allowGridPaging) {
        const currentPageRowsCount = this.getCurrentPageRowsCount();
        if (this.selectedRecords.length === currentPageRowsCount) {
          this.rowSelected.emit(this.selectedRecords);
        } else {
          const dataArray = data as Array<Object>;
          dataArray.forEach(d => this.rowDeselected.emit([d]));
        }
      } else {
        if (this.selectedRecords.length === this.dataSource.length) {
          this.isAllSelected = false;
          this.rowDeselected.emit(data as any[]);
        } else {
          const dataArray = data as Array<Object>;
          // if (dataArray.length > 1) return;
          dataArray.forEach(d => this.rowDeselected.emit([d]));
        }
      }
    }
  }

  private getCurrentPageRowsCount(): number {
    const pageSize = this.grid.pageSettings.pageSize;
    const currentPage = this.grid.pageSettings.currentPage;
    const totalRecordsCount = this.grid.pageSettings.totalRecordsCount;
    return (totalRecordsCount - (pageSize * (currentPage - 1)) >= pageSize) ? pageSize : totalRecordsCount - (pageSize * (currentPage - 1));
  }

  onActionBegin(args: any) {
    this.isDataChanging = true;
  }

  onActionComplete(args: any) {
    this.isDataChanging = false;
  }

  clearSelection() {
    this.grid.clearSelection();
  }

  onDataBound(args: any): void {
    if (this.selIndex.length) {
      this.grid.selectRows(this.selIndex);
      this.selIndex = [];
    }
    this.selectedRecords = this.grid.getSelectedRecords();
  }

  rowDataBound(args: any) {
    if (this.initialRecords) {
      if (this.initialRecords.includes(args.data[this.checkField])) {
        this.selIndex.push(parseInt(args.row.getAttribute('aria-rowindex')));
      }
    }
  }

  onRowDrop(args: RowDragEventArgs) {
    this.rowDrop.emit(args);
  }

  refresh() {
    this.grid.refresh();
  }

  async excelExport(data: any, UserStatus) {
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    let hours = today.getHours() > 12 ? today.getHours() - 12 : today.getHours();
    let am_pm = today.getHours() >= 12 ? "PM" : "AM";
    let hours12h = hours < 10 ? "0" + hours : hours;
    let minutes = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
    let time = hours12h + "-" + minutes + " " + am_pm;
    let dateTime = date + ' ' + time;
    let Userstatus;
    if (UserStatus === 0) {
      Userstatus = 'Active Users';
    } else if (UserStatus === 1) {
      Userstatus = 'Unassigned Users';
      (this.grid.columns[7] as Column).visible = false;
    } else if (UserStatus === 2) {
      Userstatus = 'Disabled Users';
    }
    const excelExportProperties: ExcelExportProperties = {
      dataSource: data,
      fileName: Userstatus + ' ' + dateTime + '.xlsx'
    };
    (this.grid.columns[0] as Column).visible = false;
    (this.grid.columns[1] as Column).visible = false;
    (this.grid.columns[2] as Column).visible = true;
    (this.grid.columns[3] as Column).visible = true;
    (this.grid.columns[4] as Column).visible = true;
    (this.grid.columns[6] as Column).visible = false;
    if (UserStatus === 1) {
      (this.grid.columns[7] as Column).visible = false;
    }
    (this.grid.columns[8] as Column).visible = true;

    await this.grid.excelExport(excelExportProperties);

    this.excelExportComplete();
  }

  excelExportComplete() {
    (this.grid.columns[0] as Column).visible = true;
    (this.grid.columns[1] as Column).visible = true;
    (this.grid.columns[2] as Column).visible = false;
    (this.grid.columns[3] as Column).visible = false;
    (this.grid.columns[4] as Column).visible = false;
    (this.grid.columns[6] as Column).visible = true;
    (this.grid.columns[7] as Column).visible = true;
    (this.grid.columns[8] as Column).visible = false;
  }

  exportToExcel() {
    this.export.emit();
  }

  private toggleFavoriteGridEvent(data: any) {
    this.toggleFavorite.emit(data);
  }

  // Modifications
  private editGridEvent(item: any) {
    this.edit.emit(item);
  }

  private deleteGridEvent(item: any) {
    this.delete.emit(item);
  }

  private downloadGridEvent(item: any) {
    this.download.emit(item);
  }

  private activateEvent(item: any) {
    this.activate.emit(item);
  }

  //Toolbar
  private toolBarEvent(index: number) {
    this.toolbarClick.emit(index);
  }

  //Pagination
  private changePageEvent(pageNumber: number) {
    if (this.autoPagination) {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { page: pageNumber },
        queryParamsHandling: "merge"
      });
      try {
        window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
      } catch (e) {
        window.scrollTo(0, 0);
      }
    }
    this.changePage.emit(pageNumber);
  }

  // Footer
  footerActionsEvent(index: number) {
    this.footerActions.emit(index);
  }

  // Collapse Button
  collapse(data: any) {
    const tr = parentsUntil(data.target, 'e-row');
    const trIdx = parseInt(tr.getAttribute('aria-rowindex'));
    if (parentsUntil(data.target, 'e-row').children[0].classList.contains('e-detailrowexpand')) {
      this.grid.detailRowModule.collapse(trIdx);
    } else {
      this.grid.detailRowModule.expand(trIdx);
    }
  }

  onAdvance(data: any) {
    this.advance.emit(data);
  }

  closeDetail(data: any) {
    // const tr = parentsUntil(data.target, 'e-detailrow').previousSibling;
    const tr = data.path[3].previousSibling;
    const trIdx = parseInt(tr.getAttribute('aria-rowindex'));
    if ((tr).childNodes[0].classList.contains('e-detailrowexpand')) {
      this.grid.detailRowModule.collapse(trIdx);
    } else {
      this.grid.detailRowModule.expand(trIdx);
    }
  }

  onQueryCellInfo(args: QueryCellInfoEventArgs) {
    // const content = args.data[args.column.field] ? args.data[args.column.field].toString() : null;
    // if (content) {
    //   const tooltip: Tooltip = new Tooltip({ content: content }, args.cell as HTMLTableCellElement);
    // }
  }
}
