import { DetailCardComponent } from './detail-card/detail-card.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ListComponent } from './list/list.component';
import { TabsComponent } from './tabs/tabs.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import {
  PageService as GridPageService, SearchService as GridSearchService, GridModule, SortService as GridSortService,
  PagerModule, DetailRowService, ExcelExportService, ToolbarService as GridToolbarService, Pager, ResizeService
} from '@syncfusion/ej2-angular-grids';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ViewsComponent } from './views/view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PagerComponent } from './pager/pager.component';
import { CompanyCardComponent } from './company-card/company-card.component';
import { RouterModule } from '@angular/router';
import { ModalComponent } from './modal/modal.component';
import { ModalConfirmComponent } from './modal/modal-confirm.component';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { ButtonsComponent } from './buttons/buttons.component';
import { ImagePreloadDirective } from '../core/directives/image-preload/image-preload.directive';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    TabsComponent,
    ListComponent,
    BreadcrumbComponent,
    ToolbarComponent,
    DetailCardComponent,
    ViewsComponent,
    PagerComponent,
    CompanyCardComponent,
    ModalComponent,
    ModalConfirmComponent,
    ButtonsComponent,
    ImagePreloadDirective,
  ],
  imports: [
    CommonModule,
    GridModule,
    PagerModule,
    RouterModule,
    ReactiveFormsModule,
    DialogModule,
  ],
  exports: [
    CommonModule,
    PageNotFoundComponent,
    TabsComponent,
    ListComponent,
    BreadcrumbComponent,
    ToolbarComponent,
    DetailCardComponent,
    ViewsComponent,
    PagerComponent,
    ModalComponent,
    ModalConfirmComponent,
    ButtonsComponent,
    ImagePreloadDirective,

  ],
  providers: [
    GridPageService,
    GridSortService,
    GridToolbarService,
    GridSearchService,
    DetailRowService,
    ExcelExportService,
    ResizeService
  ],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class SharedModule { }
