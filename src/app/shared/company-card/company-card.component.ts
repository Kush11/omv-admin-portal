import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { Company } from 'src/app/core/models/entity/company';

@Component({
  selector: 'app-company-card',
  templateUrl: './company-card.component.html',
  styleUrls: ['./company-card.component.css']
})
export class CompanyCardComponent implements OnInit {

  @Input() isEdit: string;
  @Output() create = new EventEmitter<any>();

  @ViewChild('fieldDialog', {static: false}) fieldDialogList: DialogComponent;
  constructor() { }

  ngOnInit() {
    this.fieldDialogList.show();
  }
}

