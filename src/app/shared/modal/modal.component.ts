import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { Button } from 'src/app/core/models/entity/button';

const BROWSE = 'Browse';
const CHANGE = 'Change';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @ViewChild('modal', {static: false}) modal: DialogComponent;

  @Input() modalPosition: Object = { X: 'center', Y: 100 };
  confirmButtons: Button[] = [
    { text: 'YES', type: 'button', cssClass: 'button-round modal-yes-button-qa' },
    { text: 'No', type: 'button', cssClass: 'button-no-outline modal-no-button-qa' }
  ];

  @Input() title: string;
  @Input() message = 'Are you sure you want to deactivate this item?'; // for confirm modal
  @Input() dataSource = [];
  @Input() saveText = 'Delete';
  @Input() width = 500;
  @Input() type: string;
  @Input() fields: Object; // for default listview
  @Input() selectedItems = []; // for listviews
  details: ModalDetail[] = [];

  @Output() rowSelected = new EventEmitter<any>();
  @Output() save = new EventEmitter<any>();
  @Output() submit = new EventEmitter(); // for form modal
  @Output() close = new EventEmitter();
  @Output() yes = new EventEmitter<any>(); // for confirm modal
  @Output() no = new EventEmitter<any>(); // for confirm modal

  enableSortSave: boolean;
  sortData = [];

  //#region Upload

  @ViewChild('file', {static: false}) file: ElementRef;
  fileName: string;
  isFileTouched: boolean;
  selectedFile: File;
  fileSelectionButtonText = BROWSE;

  //#endregion

  ngOnInit() {

  }

  show() {
    this.modal.show();
  }

  closeDialog() {
    if (this.type === 'upload') {
      this.resetUpload();
    }
    this.close.emit();
    this.modal.hide();
  }

  //#region Confirm Section

  confirmButtonActions(index: number) {
    this.modal.hide();
    if (index === 0) {
      this.yes.emit();
    } else {
      this.no.emit();
    }
  }

  //#endregion

  //#region Upload

  chooseFile() {
    this.file.nativeElement.click();
  }

  onFileChosen() {
    this.isFileTouched = true;
    const files: { [key: string]: File } = this.file.nativeElement.files;
    this.selectedFile = files[0];
    if (this.selectedFile) {
      this.fileSelectionButtonText = CHANGE;
    }
  }

  onUpload() {
    this.save.emit(this.selectedFile);
    this.closeDialog();
  }

  private resetUpload() {
    this.selectedFile = null;
    this.fileSelectionButtonText = BROWSE;
  }

  //#endregion
}

export class ModalDetail {
  name: string;
  value: any;
}
