import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Tab } from 'src/app/core/models/entity/tab';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SubSink } from 'subsink/dist/subsink';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit, OnDestroy {

  private subs = new SubSink();
  currentRoute: string;
  @Input() tabs: Tab[] = [];
  @Output() navigate = new EventEmitter<string>();
  @Output() routeChange = new EventEmitter<any>();

  constructor(private router: Router) { }

  ngOnInit() {
    this.setActiveTab(this.router.url);
    this.subs.sink = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.setActiveTab(event.url);
        this.routeChange.emit(event.url);
      });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  performNavigation(link: string) {
    this.setActiveTab(link);
    this.navigate.emit(link);
  }

  setActiveTab(link: string) {
    if (typeof link !== 'string') { return; }

    const url = link.split('?')[0]; // get the current route without the query params
    const tab = this.tabs.find(x => x.link === url);
    if (tab) {
      this.tabs.map(x => x.isActive = false);
      tab.isActive = true;
    }
  }
}
