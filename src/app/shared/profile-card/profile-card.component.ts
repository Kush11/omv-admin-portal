import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Company } from 'src/app/core/models/entity/company';
import { DialogComponent } from '@syncfusion/ej2-angular-popups';
import { ModalComponent } from '../modal/modal.component';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.css']
})
export class ProfileCardComponent implements OnInit {

  @Input() profile: Company;
  @Input() isNew: boolean;
  @ViewChild('fieldDialog', {static: false}) fieldDialogList: DialogComponent;
  @ViewChild('confirmModal', {static: false}) confirmModal: ModalComponent;

  @Output() edit = new EventEmitter<any>();
  @Output() delete = new EventEmitter<any>();
  @Output() create = new EventEmitter<any>();
  @Output() view = new EventEmitter<any>();
  @Output() impersonate = new EventEmitter<any>();


  companyForm: FormGroup;
  showListControl: boolean;
  showParentField: boolean;
  selectedCompanyFieldId: number;
  deleteMessage: string;
  isEdit: boolean;
  currentCustomerField: Company;


  constructor(private fb: FormBuilder, private store: Store) {

  }

  ngOnInit() {
    // this.profile = this.profile;

  }

  onEdit(customer) {
    this.edit.emit(customer);

  }

  onDelete(args: Company) {
    this.delete.emit(args);
  }

  onCreate(args: Company) {
    this.create.emit(args);
  }


  onView(args: Company) {
    this.view.emit(args);
  }

  onImpersonate(customer) {
    console.log('data', customer);
    this.impersonate.emit(customer);
  }

}
