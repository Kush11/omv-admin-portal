import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/internal/operators/filter';
import { SubSink } from 'subsink/dist/subsink';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit, OnDestroy {

  private subs = new SubSink;
  isSettingsActive: boolean;
  isNotificationsActive: boolean;
  isCompanyProfileActive: boolean;

  companiesLink = 'companies';
  settingsLink = 'settings';
  notificationsLink = 'notifications';

  searchName = '';
  data: Object = [];
  fields: Object = { groupBy: 'category', tooltip: 'text' };

  constructor(private router: Router) {

  }

  ngOnInit() {
    if (this.router.url !== '/') {
      this.setActiveTab(this.router.url);
    }

    this.subs.add(
      this.router.events
        .pipe(
          filter(event => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          this.setActiveTab(event.url);
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  navigate(url: string) {
    this.router.navigate([url]);
  }

  setActiveTab(url: string) {
    this.clearActiveSelections();
    console.log("setActiveTab: ", url);
    if (url.includes(this.notificationsLink)) {
      this.isNotificationsActive = true;
    } else {
      this.isCompanyProfileActive = true;
    }
  }

  clearActiveSelections() {
    this.isCompanyProfileActive = this.isNotificationsActive = this.isSettingsActive = false;
  }
}
