
import { OnInit, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';
import {
  SetPageTitle, ShowSpinner, HideSpinner, ShowSuccessMessage, ShowWarningMessage, ShowErrorMessage
} from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.state';
import { Permission } from 'src/app/core/enum/permission';
import { AuthService } from 'src/app/core/services/business/auth.service';
import { SubSink } from 'subsink/dist/subsink';


export class BaseComponent implements OnInit, OnDestroy {

  public subsArray = new SubSink();
  displayWidth: number;
  _permission: string;
  userHasPermission: boolean;
  currentUserId: number;
  permission: string;

  @Select(AppState.getUserPermissions) userPermissions$: Observable<Permission[]>;
  @Select(AppState.getCurrentUserId) currentUserId$: Observable<number>;

  constructor(protected store: Store, protected auth?: AuthService) {
    this.subsArray.add(
      this.userPermissions$
        .subscribe(permissions => {
          if (this._permission) {
            var permissionNames = permissions.map(p => p.name);
            this.userHasPermission = permissionNames.includes(this._permission);

            if (!this.userHasPermission) {

            }
          }
          this.Permission = '';
        })
    );
  }

  ngOnInit() {
    console.log('BaseComponent - ngOnInit');
  }

  ngOnDestroy() {
    this.subsArray.unsubscribe();
  }

  showSpinner() {
    this.store.dispatch(new ShowSpinner());
  }

  hideSpinner() {
    this.store.dispatch(new HideSpinner());
  }

  protected showSuccessMessage(message: string) {
    this.store.dispatch(new ShowSuccessMessage(message));
  }

  protected showWarningMessage(message: string) {
    this.store.dispatch(new ShowWarningMessage(message));
  }

  protected showErrorMessage(message: string) {
    this.store.dispatch(new ShowErrorMessage(message));
  }

  get Permission(): string {
    return this._permission;
  }
  set Permission(value: string) {
    this._permission = value;
  }

  protected setPageTitle(pageTitle: string) {
    this.store.dispatch(new SetPageTitle(pageTitle));
  }
}
