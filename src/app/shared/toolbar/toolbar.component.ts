import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ToolBar } from 'src/app/core/models/entity/toolbar-action';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {


  @Input() showTotal: boolean;
  @Input() totalText = 'Total';
  @Input() totalValue = 0;  
  @Input() topMargin: number;
  @Input() selectionText = 'Selected';
  @Input() toolbar: ToolBar[];
  @Input() showStatusIcons: boolean;
  @Input() hideSelectedText: boolean;
  


  @Output() click = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  toolBarEvent(index: number) {
    this.click.emit(index);
  }
}
