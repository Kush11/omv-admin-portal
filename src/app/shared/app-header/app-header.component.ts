import { GetNotification, GetNewNotifications } from './../../notifications/state/notifications.action';
import { NotificationsState } from './../../notifications/state/notifications.state';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AppState } from 'src/app/state/app.state';
import { Observable } from 'rxjs/internal/Observable';
import { User } from 'src/app/core/models/entity/user';
import { AuthService } from 'src/app/core/services/business/auth.service';
import { Router } from '@angular/router';
import { LogOut } from 'src/app/state/app.actions';
import { SubSink } from 'subsink/dist/subsink';
import { Ticket } from 'src/app/core/models/entity/notification';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {

  isAuthenticated: boolean;
  @Select(NotificationsState.getNewNotifications) notifications$: Observable<Ticket[]>;
  @Select(AppState.getLoggedInUser) currentUser$: Observable<User>;

  private subs = new SubSink();
  notifyStatus: boolean;


  constructor(
    private store: Store,
    private router: Router,
    private auth: AuthService
  ) { }

  async ngOnInit() {
    // this.notifications$.toPromise()
    // .then(res => {
    //   console.log('res', res);
    // });
    this.isAuthenticated = await this.auth.isAuthenticated();
    this.subs.add(
      this.notifications$.subscribe(res => {
        if (res.length > 0) {
          this.notifyStatus = true;
        } else {
          this.notifyStatus = false;
        }
      })
    );
  }

  login() {
    this.auth.login();
  }

  navigateToNotifications() {
    const companyElem = document.querySelector('.c-leftnav-menu');
    const notificationsElem = document.querySelector('.notifications');
    companyElem.classList.remove('c-leftnav-menu-active');
    notificationsElem.classList.add('c-leftnav-link-active');
    this.router.navigateByUrl('/notifications');
  }

  logout() {
    this.saveReturnUrl(this.router.url);
    this.store.dispatch(new LogOut());
  }

  private saveReturnUrl(route) {
    const ignored_routes = ['/dashboard', '/authorize-check', '/implicit/callback'];
    if (ignored_routes.includes(route)) { return; }
    const key = 'return_url';
    if (sessionStorage.getItem(key)) { sessionStorage.removeItem(key); }
    sessionStorage.setItem(key, route);
  }



}
