import { Component } from '@angular/core';
import { AuthService } from '../core/services/business/auth.service';
import { Store } from '@ngxs/store';

@Component({
  template: `
<img style="margin: auto; display: block; width: 75px;" src="./assets/images/icon-loading-colored.svg">
<label style="text-align: center; font-family: 'Open Sans', sans-serif; font-size: 20px; display: block;"> Please wait... </label>` })
export class AuthCallbackComponent {

  constructor(private auth: AuthService, private store: Store) {
    // Handles the response from Okta and parses tokens
    auth.handleAuthentication();
  }
}
