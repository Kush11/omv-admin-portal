export const environment = {
  production: false,
  useMocks: true,
  issuesContainer: '{env-issuesContainer}',
  api: {
    baseUrl: '{env-baseUrl}'
  },
  customer: {
    issuerUrl: '{env-issuerUrl}',
    clientId: '{env-clientId}',
    authServerId: '{env-authServerId}'
  }
};
