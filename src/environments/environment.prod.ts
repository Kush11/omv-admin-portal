export const environment = {
  production: true,
  useMocks: false,
  issuesContainer: '{env-issuesContainer}',
  api: {
    baseUrl: '{env-baseUrl}'
  },
  customer: {
    issuerUrl: '{env-issuerUrl}',
    clientId: '{env-clientId}',
    authServerId: '{env-authServerId}'
  }
};

