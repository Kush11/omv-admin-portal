export const environment = {
  production: false,
  useMocks: false,
  issuesContainer: 'issues',
  api: {
    baseUrl: 'https://omv2dadsvr.azurewebsites.net/api'
  },
  customer: {
    issuerUrl: 'https://oceaneering.oktapreview.com',
    clientId: '0oanqyn8vbR8aAndH0h7',
    authServerId: 'auski3vw7syJHudft0h7'
  }
};