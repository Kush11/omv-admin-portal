export const environment = {
  production: false,
  useMocks: false,
  issuesContainer: 'issues',
  api: {
    baseUrl: 'https://chai-omv-d-ap-api.azurewebsites.net/api'
  },
  customer: {
    issuerUrl: 'https://dev-104918.okta.com',
    clientId: '0oa1arirmu6auhJND357',
    authServerId: 'default'
  }
};